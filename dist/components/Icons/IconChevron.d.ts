import React from 'react';
import './Icons.scss';
interface IProps {
    color?: string;
    direction?: 'top' | 'left' | 'right' | 'bottom';
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

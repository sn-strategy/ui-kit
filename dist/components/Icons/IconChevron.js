function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import cn from 'classnames';

(function () {
  var styles = ".Icon {\n  transition: transform 0.3s linear;\n}\n.Icon.direction-top {\n  transform: rotateZ(180deg);\n}\n.Icon.direction-left {\n  transform: rotateZ(270deg);\n}\n.Icon.direction-right {\n  transform: rotateZ(90deg);\n}\n.Icon.direction-bottom {\n  transform: rotateZ(0deg);\n}";
  var fileName = "IconChevron_Icons";
  var element = document.querySelector("style[data-sass-component='IconChevron_Icons']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var IconChevron = function IconChevron(props) {
  var _props$color = props.color,
      color = _props$color === void 0 ? 'currentColor' : _props$color,
      _props$direction = props.direction,
      direction = _props$direction === void 0 ? 'left' : _props$direction;
  var className = cn('Icon', _defineProperty({}, "direction-".concat(direction), direction));
  return /*#__PURE__*/React.createElement("svg", {
    className: className,
    width: "24",
    height: "24",
    viewBox: "0 0 24 24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, /*#__PURE__*/React.createElement("path", {
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M16 9.50009C15.744 9.50009 15.488 9.59809 15.293 9.79309L11.988 13.0981L8.69502 9.91809C8.29702 9.53409 7.66502 9.54409 7.28102 9.94309C6.89702 10.3401 6.90802 10.9741 7.30502 11.3571L11.305 15.2191C11.698 15.5981 12.322 15.5931 12.707 15.2071L16.707 11.2071C17.098 10.8161 17.098 10.1841 16.707 9.79309C16.512 9.59809 16.256 9.50009 16 9.50009",
    fill: color
  }), /*#__PURE__*/React.createElement("mask", {
    id: "mask0_922_40709",
    style: {
      maskType: 'alpha'
    },
    maskUnits: "userSpaceOnUse",
    x: "7",
    y: "9",
    width: "11",
    height: "7"
  }, /*#__PURE__*/React.createElement("path", {
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M16 9.50009C15.744 9.50009 15.488 9.59809 15.293 9.79309L11.988 13.0981L8.69502 9.91809C8.29702 9.53409 7.66502 9.54409 7.28102 9.94309C6.89702 10.3401 6.90802 10.9741 7.30502 11.3571L11.305 15.2191C11.698 15.5981 12.322 15.5931 12.707 15.2071L16.707 11.2071C17.098 10.8161 17.098 10.1841 16.707 9.79309C16.512 9.59809 16.256 9.50009 16 9.50009",
    fill: "white"
  })), /*#__PURE__*/React.createElement("g", {
    mask: "url(#mask0_922_40709)"
  }));
};

export default /*#__PURE__*/React.memo(IconChevron);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9JY29ucy9JY29uQ2hldnJvbi50c3giXSwibmFtZXMiOlsiUmVhY3QiLCJjbiIsIkljb25DaGV2cm9uIiwicHJvcHMiLCJjb2xvciIsImRpcmVjdGlvbiIsImNsYXNzTmFtZSIsIm1hc2tUeXBlIiwibWVtbyJdLCJtYXBwaW5ncyI6Ijs7QUFBQSxPQUFPQSxLQUFQLE1BQTBCLE9BQTFCO0FBQ0EsT0FBT0MsRUFBUCxNQUFlLFlBQWY7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBU0EsSUFBTUMsV0FBdUIsR0FBRyxTQUExQkEsV0FBMEIsQ0FBQ0MsS0FBRCxFQUFXO0FBQ3pDLHFCQUF1REEsS0FBdkQsQ0FBUUMsS0FBUjtBQUFBLE1BQVFBLEtBQVIsNkJBQWdCLGNBQWhCO0FBQUEseUJBQXVERCxLQUF2RCxDQUFnQ0UsU0FBaEM7QUFBQSxNQUFnQ0EsU0FBaEMsaUNBQTRDLE1BQTVDO0FBRUEsTUFBTUMsU0FBUyxHQUFHTCxFQUFFLENBQUMsTUFBRCwwQ0FDSkksU0FESSxHQUNVQSxTQURWLEVBQXBCO0FBSUEsc0JBQ0U7QUFBSyxJQUFBLFNBQVMsRUFBRUMsU0FBaEI7QUFBMkIsSUFBQSxLQUFLLEVBQUMsSUFBakM7QUFBc0MsSUFBQSxNQUFNLEVBQUMsSUFBN0M7QUFBa0QsSUFBQSxPQUFPLEVBQUMsV0FBMUQ7QUFBc0UsSUFBQSxJQUFJLEVBQUMsTUFBM0U7QUFBa0YsSUFBQSxLQUFLLEVBQUM7QUFBeEYsa0JBQ0U7QUFBTSxJQUFBLFFBQVEsRUFBQyxTQUFmO0FBQXlCLElBQUEsUUFBUSxFQUFDLFNBQWxDO0FBQTRDLElBQUEsQ0FBQyxFQUFDLDBWQUE5QztBQUF5WSxJQUFBLElBQUksRUFBRUY7QUFBL1ksSUFERixlQUVFO0FBQU0sSUFBQSxFQUFFLEVBQUMsaUJBQVQ7QUFBMkIsSUFBQSxLQUFLLEVBQUU7QUFBRUcsTUFBQUEsUUFBUSxFQUFFO0FBQVosS0FBbEM7QUFBeUQsSUFBQSxTQUFTLEVBQUMsZ0JBQW5FO0FBQW9GLElBQUEsQ0FBQyxFQUFDLEdBQXRGO0FBQTBGLElBQUEsQ0FBQyxFQUFDLEdBQTVGO0FBQWdHLElBQUEsS0FBSyxFQUFDLElBQXRHO0FBQTJHLElBQUEsTUFBTSxFQUFDO0FBQWxILGtCQUNFO0FBQU0sSUFBQSxRQUFRLEVBQUMsU0FBZjtBQUF5QixJQUFBLFFBQVEsRUFBQyxTQUFsQztBQUE0QyxJQUFBLENBQUMsRUFBQywwVkFBOUM7QUFBeVksSUFBQSxJQUFJLEVBQUM7QUFBOVksSUFERixDQUZGLGVBS0U7QUFBRyxJQUFBLElBQUksRUFBQztBQUFSLElBTEYsQ0FERjtBQVdELENBbEJEOztBQW9CQSw0QkFBZVAsS0FBSyxDQUFDUSxJQUFOLENBQVdOLFdBQVgsQ0FBZiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBGQyB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IGNuIGZyb20gJ2NsYXNzbmFtZXMnXG5cbmltcG9ydCAnLi9JY29ucy5zY3NzJ1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgY29sb3I/OiBzdHJpbmdcbiAgZGlyZWN0aW9uPzogJ3RvcCcgfCAnbGVmdCcgfCAncmlnaHQnIHwgJ2JvdHRvbSdcbn1cblxuY29uc3QgSWNvbkNoZXZyb246IEZDPElQcm9wcz4gPSAocHJvcHMpID0+IHtcbiAgY29uc3QgeyBjb2xvciA9ICdjdXJyZW50Q29sb3InLCBkaXJlY3Rpb24gPSAnbGVmdCcgfSA9IHByb3BzXG5cbiAgY29uc3QgY2xhc3NOYW1lID0gY24oJ0ljb24nLCB7XG4gICAgW2BkaXJlY3Rpb24tJHtkaXJlY3Rpb259YF06IGRpcmVjdGlvbixcbiAgfSlcblxuICByZXR1cm4gKFxuICAgIDxzdmcgY2xhc3NOYW1lPXtjbGFzc05hbWV9IHdpZHRoPScyNCcgaGVpZ2h0PScyNCcgdmlld0JveD0nMCAwIDI0IDI0JyBmaWxsPSdub25lJyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPlxuICAgICAgPHBhdGggZmlsbFJ1bGU9J2V2ZW5vZGQnIGNsaXBSdWxlPSdldmVub2RkJyBkPSdNMTYgOS41MDAwOUMxNS43NDQgOS41MDAwOSAxNS40ODggOS41OTgwOSAxNS4yOTMgOS43OTMwOUwxMS45ODggMTMuMDk4MUw4LjY5NTAyIDkuOTE4MDlDOC4yOTcwMiA5LjUzNDA5IDcuNjY1MDIgOS41NDQwOSA3LjI4MTAyIDkuOTQzMDlDNi44OTcwMiAxMC4zNDAxIDYuOTA4MDIgMTAuOTc0MSA3LjMwNTAyIDExLjM1NzFMMTEuMzA1IDE1LjIxOTFDMTEuNjk4IDE1LjU5ODEgMTIuMzIyIDE1LjU5MzEgMTIuNzA3IDE1LjIwNzFMMTYuNzA3IDExLjIwNzFDMTcuMDk4IDEwLjgxNjEgMTcuMDk4IDEwLjE4NDEgMTYuNzA3IDkuNzkzMDlDMTYuNTEyIDkuNTk4MDkgMTYuMjU2IDkuNTAwMDkgMTYgOS41MDAwOScgZmlsbD17Y29sb3J9Lz5cbiAgICAgIDxtYXNrIGlkPSdtYXNrMF85MjJfNDA3MDknIHN0eWxlPXt7IG1hc2tUeXBlOiAnYWxwaGEnIH19IG1hc2tVbml0cz0ndXNlclNwYWNlT25Vc2UnIHg9JzcnIHk9JzknIHdpZHRoPScxMScgaGVpZ2h0PSc3Jz5cbiAgICAgICAgPHBhdGggZmlsbFJ1bGU9J2V2ZW5vZGQnIGNsaXBSdWxlPSdldmVub2RkJyBkPSdNMTYgOS41MDAwOUMxNS43NDQgOS41MDAwOSAxNS40ODggOS41OTgwOSAxNS4yOTMgOS43OTMwOUwxMS45ODggMTMuMDk4MUw4LjY5NTAyIDkuOTE4MDlDOC4yOTcwMiA5LjUzNDA5IDcuNjY1MDIgOS41NDQwOSA3LjI4MTAyIDkuOTQzMDlDNi44OTcwMiAxMC4zNDAxIDYuOTA4MDIgMTAuOTc0MSA3LjMwNTAyIDExLjM1NzFMMTEuMzA1IDE1LjIxOTFDMTEuNjk4IDE1LjU5ODEgMTIuMzIyIDE1LjU5MzEgMTIuNzA3IDE1LjIwNzFMMTYuNzA3IDExLjIwNzFDMTcuMDk4IDEwLjgxNjEgMTcuMDk4IDEwLjE4NDEgMTYuNzA3IDkuNzkzMDlDMTYuNTEyIDkuNTk4MDkgMTYuMjU2IDkuNTAwMDkgMTYgOS41MDAwOScgZmlsbD0nd2hpdGUnLz5cbiAgICAgIDwvbWFzaz5cbiAgICAgIDxnIG1hc2s9J3VybCgjbWFzazBfOTIyXzQwNzA5KSc+XG4gICAgICA8L2c+XG4gICAgPC9zdmc+XG5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBSZWFjdC5tZW1vKEljb25DaGV2cm9uKVxuIl19
import React from 'react';
import type { IMenuItem } from '../../models/menu';
import './AppSidebar.scss';
interface IProps {
    logo: string;
    menuItems: IMenuItem[];
    className?: string;
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

import React, { useState, useCallback, useRef } from 'react';
import { Layout } from 'antd';
import cn from 'classnames';
import AppSidebarLogo from './components/AppSidebarLogo/AppSidebarLogo';
import AppSidebarMenu from './components/AppSidebarMenu/AppSidebarMenu';
import AppSidebarToggle from './components/AppSidebarToggle/AppSidebarToggle';

(function () {
  var styles = ".AppSidebar.ant-layout-sider {\n  background-color: #ffffff;\n  z-index: 2;\n}\n.AppSidebar.ant-layout-sider.hovered {\n  margin-inline-end: -12.0625rem;\n}\n.AppSidebar.ant-layout-sider-has-trigger {\n  padding-bottom: 0;\n}\n.AppSidebar .ant-layout-sider-trigger {\n  height: 0;\n  background-color: transparent;\n}\n.AppSidebar .ant-layout-sider-children {\n  display: flex;\n  flex-direction: column;\n}";
  var fileName = "AppSidebar_AppSidebar";
  var element = document.querySelector("style[data-sass-component='AppSidebar_AppSidebar']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var AppSidebar = function AppSidebar(props) {
  var logo = props.logo,
      menuItems = props.menuItems,
      className = props.className;

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      hovered = _useState2[0],
      setHovered = _useState2[1];

  var _useState3 = useState(true),
      _useState4 = _slicedToArray(_useState3, 2),
      collapsed = _useState4[0],
      setCollapsed = _useState4[1];

  var hoverTimeout = useRef(null);
  var handleMouseEnter = useCallback(function () {
    if (hoverTimeout.current) {
      clearTimeout(hoverTimeout.current);
    }

    if (!hovered) {
      hoverTimeout.current = setTimeout(function () {
        return setHovered(true);
      }, 200);
    }
  }, [hovered]);
  var handleMouseLeave = useCallback(function () {
    if (hoverTimeout.current) {
      clearTimeout(hoverTimeout.current);
    }

    if (hovered) {
      hoverTimeout.current = setTimeout(function () {
        return setHovered(false);
      }, 200);
    }
  }, [hovered]);
  var toggleCollapsed = useCallback(function () {
    setCollapsed(!collapsed);
  }, [collapsed]);
  var SiderClassName = cn('AppSidebar', className, {
    hovered: hovered && collapsed
  });
  return /*#__PURE__*/React.createElement(Layout.Sider, {
    className: SiderClassName,
    collapsible: true,
    width: 269,
    collapsedWidth: 76,
    collapsed: !hovered && collapsed,
    trigger: /*#__PURE__*/React.createElement(AppSidebarToggle, {
      collapsed: collapsed
    }),
    onCollapse: toggleCollapsed,
    onMouseEnter: handleMouseEnter,
    onMouseLeave: handleMouseLeave
  }, /*#__PURE__*/React.createElement(AppSidebarLogo, {
    logo: logo
  }), /*#__PURE__*/React.createElement(AppSidebarMenu, {
    items: menuItems
  }));
};

export default /*#__PURE__*/React.memo(AppSidebar);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9BcHBTaWRlYmFyL0FwcFNpZGViYXIudHN4Il0sIm5hbWVzIjpbIlJlYWN0IiwidXNlU3RhdGUiLCJ1c2VDYWxsYmFjayIsInVzZVJlZiIsIkxheW91dCIsImNuIiwiQXBwU2lkZWJhckxvZ28iLCJBcHBTaWRlYmFyTWVudSIsIkFwcFNpZGViYXJUb2dnbGUiLCJBcHBTaWRlYmFyIiwicHJvcHMiLCJsb2dvIiwibWVudUl0ZW1zIiwiY2xhc3NOYW1lIiwiaG92ZXJlZCIsInNldEhvdmVyZWQiLCJjb2xsYXBzZWQiLCJzZXRDb2xsYXBzZWQiLCJob3ZlclRpbWVvdXQiLCJoYW5kbGVNb3VzZUVudGVyIiwiY3VycmVudCIsImNsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJoYW5kbGVNb3VzZUxlYXZlIiwidG9nZ2xlQ29sbGFwc2VkIiwiU2lkZXJDbGFzc05hbWUiLCJtZW1vIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxPQUFPQSxLQUFQLElBQWdCQyxRQUFoQixFQUEwQkMsV0FBMUIsRUFBdUNDLE1BQXZDLFFBQXFELE9BQXJEO0FBQ0EsU0FBU0MsTUFBVCxRQUF1QixNQUF2QjtBQUNBLE9BQU9DLEVBQVAsTUFBZSxZQUFmO0FBRUEsT0FBT0MsY0FBUCxNQUEyQiw0Q0FBM0I7QUFDQSxPQUFPQyxjQUFQLE1BQTJCLDRDQUEzQjtBQUNBLE9BQU9DLGdCQUFQLE1BQTZCLGdEQUE3Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFZQSxJQUFNQyxVQUE0QixHQUFHLFNBQS9CQSxVQUErQixDQUFDQyxLQUFELEVBQVc7QUFDOUMsTUFBUUMsSUFBUixHQUF1Q0QsS0FBdkMsQ0FBUUMsSUFBUjtBQUFBLE1BQWNDLFNBQWQsR0FBdUNGLEtBQXZDLENBQWNFLFNBQWQ7QUFBQSxNQUF5QkMsU0FBekIsR0FBdUNILEtBQXZDLENBQXlCRyxTQUF6Qjs7QUFDQSxrQkFBOEJaLFFBQVEsQ0FBVSxLQUFWLENBQXRDO0FBQUE7QUFBQSxNQUFPYSxPQUFQO0FBQUEsTUFBZ0JDLFVBQWhCOztBQUNBLG1CQUFrQ2QsUUFBUSxDQUFVLElBQVYsQ0FBMUM7QUFBQTtBQUFBLE1BQU9lLFNBQVA7QUFBQSxNQUFrQkMsWUFBbEI7O0FBQ0EsTUFBTUMsWUFBWSxHQUFHZixNQUFNLENBQXdCLElBQXhCLENBQTNCO0FBRUEsTUFBTWdCLGdCQUFnQixHQUFHakIsV0FBVyxDQUFDLFlBQVk7QUFDL0MsUUFBSWdCLFlBQVksQ0FBQ0UsT0FBakIsRUFBMEI7QUFDeEJDLE1BQUFBLFlBQVksQ0FBQ0gsWUFBWSxDQUFDRSxPQUFkLENBQVo7QUFDRDs7QUFDRCxRQUFJLENBQUNOLE9BQUwsRUFBYztBQUNaSSxNQUFBQSxZQUFZLENBQUNFLE9BQWIsR0FBdUJFLFVBQVUsQ0FBQztBQUFBLGVBQU1QLFVBQVUsQ0FBQyxJQUFELENBQWhCO0FBQUEsT0FBRCxFQUF5QixHQUF6QixDQUFqQztBQUNEO0FBQ0YsR0FQbUMsRUFPakMsQ0FBQ0QsT0FBRCxDQVBpQyxDQUFwQztBQVNBLE1BQU1TLGdCQUFnQixHQUFHckIsV0FBVyxDQUFDLFlBQU07QUFDekMsUUFBSWdCLFlBQVksQ0FBQ0UsT0FBakIsRUFBMEI7QUFDeEJDLE1BQUFBLFlBQVksQ0FBQ0gsWUFBWSxDQUFDRSxPQUFkLENBQVo7QUFDRDs7QUFDRCxRQUFJTixPQUFKLEVBQWE7QUFDWEksTUFBQUEsWUFBWSxDQUFDRSxPQUFiLEdBQXVCRSxVQUFVLENBQUM7QUFBQSxlQUFNUCxVQUFVLENBQUMsS0FBRCxDQUFoQjtBQUFBLE9BQUQsRUFBMEIsR0FBMUIsQ0FBakM7QUFDRDtBQUNGLEdBUG1DLEVBT2pDLENBQUNELE9BQUQsQ0FQaUMsQ0FBcEM7QUFTQSxNQUFNVSxlQUFlLEdBQUd0QixXQUFXLENBQUMsWUFBTTtBQUN4Q2UsSUFBQUEsWUFBWSxDQUFDLENBQUNELFNBQUYsQ0FBWjtBQUNELEdBRmtDLEVBRWhDLENBQUNBLFNBQUQsQ0FGZ0MsQ0FBbkM7QUFJQSxNQUFNUyxjQUFjLEdBQUdwQixFQUFFLENBQUMsWUFBRCxFQUFlUSxTQUFmLEVBQTBCO0FBQ2pEQyxJQUFBQSxPQUFPLEVBQUVBLE9BQU8sSUFBSUU7QUFENkIsR0FBMUIsQ0FBekI7QUFJQSxzQkFDRSxvQkFBQyxNQUFELENBQVEsS0FBUjtBQUNFLElBQUEsU0FBUyxFQUFFUyxjQURiO0FBRUUsSUFBQSxXQUFXLE1BRmI7QUFHRSxJQUFBLEtBQUssRUFBRSxHQUhUO0FBSUUsSUFBQSxjQUFjLEVBQUUsRUFKbEI7QUFLRSxJQUFBLFNBQVMsRUFBRSxDQUFDWCxPQUFELElBQVlFLFNBTHpCO0FBTUUsSUFBQSxPQUFPLGVBQUUsb0JBQUMsZ0JBQUQ7QUFBa0IsTUFBQSxTQUFTLEVBQUVBO0FBQTdCLE1BTlg7QUFPRSxJQUFBLFVBQVUsRUFBRVEsZUFQZDtBQVFFLElBQUEsWUFBWSxFQUFFTCxnQkFSaEI7QUFTRSxJQUFBLFlBQVksRUFBRUk7QUFUaEIsa0JBV0Usb0JBQUMsY0FBRDtBQUFnQixJQUFBLElBQUksRUFBRVo7QUFBdEIsSUFYRixlQVlFLG9CQUFDLGNBQUQ7QUFBZ0IsSUFBQSxLQUFLLEVBQUVDO0FBQXZCLElBWkYsQ0FERjtBQWdCRCxDQWhERDs7QUFrREEsNEJBQWVaLEtBQUssQ0FBQzBCLElBQU4sQ0FBV2pCLFVBQVgsQ0FBZiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlQ2FsbGJhY2ssIHVzZVJlZiB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IHsgTGF5b3V0IH0gZnJvbSAnYW50ZCdcbmltcG9ydCBjbiBmcm9tICdjbGFzc25hbWVzJ1xuXG5pbXBvcnQgQXBwU2lkZWJhckxvZ28gZnJvbSAnLi9jb21wb25lbnRzL0FwcFNpZGViYXJMb2dvL0FwcFNpZGViYXJMb2dvJ1xuaW1wb3J0IEFwcFNpZGViYXJNZW51IGZyb20gJy4vY29tcG9uZW50cy9BcHBTaWRlYmFyTWVudS9BcHBTaWRlYmFyTWVudSdcbmltcG9ydCBBcHBTaWRlYmFyVG9nZ2xlIGZyb20gJy4vY29tcG9uZW50cy9BcHBTaWRlYmFyVG9nZ2xlL0FwcFNpZGViYXJUb2dnbGUnXG5cbmltcG9ydCB0eXBlIHsgSU1lbnVJdGVtIH0gZnJvbSAnLi4vLi4vbW9kZWxzL21lbnUnXG5cbmltcG9ydCAnLi9BcHBTaWRlYmFyLnNjc3MnXG5cbmludGVyZmFjZSBJUHJvcHMge1xuICBsb2dvOiBzdHJpbmdcbiAgbWVudUl0ZW1zOiBJTWVudUl0ZW1bXVxuICBjbGFzc05hbWU/OiBzdHJpbmdcbn1cblxuY29uc3QgQXBwU2lkZWJhcjogUmVhY3QuRkM8SVByb3BzPiA9IChwcm9wcykgPT4ge1xuICBjb25zdCB7IGxvZ28sIG1lbnVJdGVtcywgY2xhc3NOYW1lIH0gPSBwcm9wc1xuICBjb25zdCBbaG92ZXJlZCwgc2V0SG92ZXJlZF0gPSB1c2VTdGF0ZTxib29sZWFuPihmYWxzZSlcbiAgY29uc3QgW2NvbGxhcHNlZCwgc2V0Q29sbGFwc2VkXSA9IHVzZVN0YXRlPGJvb2xlYW4+KHRydWUpXG4gIGNvbnN0IGhvdmVyVGltZW91dCA9IHVzZVJlZjxOb2RlSlMuVGltZW91dCB8IG51bGw+KG51bGwpXG5cbiAgY29uc3QgaGFuZGxlTW91c2VFbnRlciA9IHVzZUNhbGxiYWNrKCgpOiB2b2lkID0+IHtcbiAgICBpZiAoaG92ZXJUaW1lb3V0LmN1cnJlbnQpIHtcbiAgICAgIGNsZWFyVGltZW91dChob3ZlclRpbWVvdXQuY3VycmVudClcbiAgICB9XG4gICAgaWYgKCFob3ZlcmVkKSB7XG4gICAgICBob3ZlclRpbWVvdXQuY3VycmVudCA9IHNldFRpbWVvdXQoKCkgPT4gc2V0SG92ZXJlZCh0cnVlKSwgMjAwKVxuICAgIH1cbiAgfSwgW2hvdmVyZWRdKVxuXG4gIGNvbnN0IGhhbmRsZU1vdXNlTGVhdmUgPSB1c2VDYWxsYmFjaygoKSA9PiB7XG4gICAgaWYgKGhvdmVyVGltZW91dC5jdXJyZW50KSB7XG4gICAgICBjbGVhclRpbWVvdXQoaG92ZXJUaW1lb3V0LmN1cnJlbnQpXG4gICAgfVxuICAgIGlmIChob3ZlcmVkKSB7XG4gICAgICBob3ZlclRpbWVvdXQuY3VycmVudCA9IHNldFRpbWVvdXQoKCkgPT4gc2V0SG92ZXJlZChmYWxzZSksIDIwMClcbiAgICB9XG4gIH0sIFtob3ZlcmVkXSlcblxuICBjb25zdCB0b2dnbGVDb2xsYXBzZWQgPSB1c2VDYWxsYmFjaygoKSA9PiB7XG4gICAgc2V0Q29sbGFwc2VkKCFjb2xsYXBzZWQpXG4gIH0sIFtjb2xsYXBzZWRdKVxuXG4gIGNvbnN0IFNpZGVyQ2xhc3NOYW1lID0gY24oJ0FwcFNpZGViYXInLCBjbGFzc05hbWUsIHtcbiAgICBob3ZlcmVkOiBob3ZlcmVkICYmIGNvbGxhcHNlZCxcbiAgfSlcblxuICByZXR1cm4gKFxuICAgIDxMYXlvdXQuU2lkZXJcbiAgICAgIGNsYXNzTmFtZT17U2lkZXJDbGFzc05hbWV9XG4gICAgICBjb2xsYXBzaWJsZVxuICAgICAgd2lkdGg9ezI2OX1cbiAgICAgIGNvbGxhcHNlZFdpZHRoPXs3Nn1cbiAgICAgIGNvbGxhcHNlZD17IWhvdmVyZWQgJiYgY29sbGFwc2VkfVxuICAgICAgdHJpZ2dlcj17PEFwcFNpZGViYXJUb2dnbGUgY29sbGFwc2VkPXtjb2xsYXBzZWR9IC8+fVxuICAgICAgb25Db2xsYXBzZT17dG9nZ2xlQ29sbGFwc2VkfVxuICAgICAgb25Nb3VzZUVudGVyPXtoYW5kbGVNb3VzZUVudGVyfVxuICAgICAgb25Nb3VzZUxlYXZlPXtoYW5kbGVNb3VzZUxlYXZlfVxuICAgID5cbiAgICAgIDxBcHBTaWRlYmFyTG9nbyBsb2dvPXtsb2dvfSAvPlxuICAgICAgPEFwcFNpZGViYXJNZW51IGl0ZW1zPXttZW51SXRlbXN9IC8+XG4gICAgPC9MYXlvdXQuU2lkZXI+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhBcHBTaWRlYmFyKVxuIl19
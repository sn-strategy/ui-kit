import React from 'react';
import './AppSidebarToggle.scss';
interface IProps {
    collapsed?: boolean;
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

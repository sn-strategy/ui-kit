import React, { memo } from 'react';
import cn from 'classnames';

(function () {
  var styles = ".AppSidebarToggle {\n  position: absolute;\n  top: -2.5rem;\n  right: -0.75rem;\n  width: 1.5rem;\n  height: 1.5rem;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: #412d3e;\n  border: 1px solid #dfd5de;\n  border-radius: 50%;\n  background-color: #ffffff;\n}\n.AppSidebarToggle .ant-menu-submenu-arrow {\n  left: auto;\n  right: auto;\n  transform: rotateY(180deg) translateX(-4px);\n  color: currentColor;\n}\n.AppSidebarToggle .ant-menu-submenu-arrow.collapsed {\n  transform: rotateY(0deg) translateX(-4px);\n}";
  var fileName = "AppSidebarToggle_AppSidebarToggle";
  var element = document.querySelector("style[data-sass-component='AppSidebarToggle_AppSidebarToggle']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var AppSidebarToggle = function AppSidebarToggle(props) {
  var collapsed = props.collapsed;
  var iconClassName = cn('ant-menu-submenu-arrow', {
    collapsed: collapsed
  });
  return /*#__PURE__*/React.createElement("div", {
    className: "AppSidebarToggle"
  }, /*#__PURE__*/React.createElement("i", {
    className: iconClassName
  }));
};

export default /*#__PURE__*/memo(AppSidebarToggle);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9BcHBTaWRlYmFyL2NvbXBvbmVudHMvQXBwU2lkZWJhclRvZ2dsZS9BcHBTaWRlYmFyVG9nZ2xlLnRzeCJdLCJuYW1lcyI6WyJSZWFjdCIsIm1lbW8iLCJjbiIsIkFwcFNpZGViYXJUb2dnbGUiLCJwcm9wcyIsImNvbGxhcHNlZCIsImljb25DbGFzc05hbWUiXSwibWFwcGluZ3MiOiJBQUFBLE9BQU9BLEtBQVAsSUFBb0JDLElBQXBCLFFBQWdDLE9BQWhDO0FBQ0EsT0FBT0MsRUFBUCxNQUFlLFlBQWY7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBUUEsSUFBTUMsZ0JBQTRCLEdBQUcsU0FBL0JBLGdCQUErQixDQUFDQyxLQUFELEVBQVc7QUFDOUMsTUFBUUMsU0FBUixHQUFzQkQsS0FBdEIsQ0FBUUMsU0FBUjtBQUVBLE1BQU1DLGFBQWEsR0FBR0osRUFBRSxDQUFDLHdCQUFELEVBQTJCO0FBQ2pERyxJQUFBQSxTQUFTLEVBQVRBO0FBRGlELEdBQTNCLENBQXhCO0FBSUEsc0JBQ0U7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUcsSUFBQSxTQUFTLEVBQUVDO0FBQWQsSUFERixDQURGO0FBS0QsQ0FaRDs7QUFjQSw0QkFBZUwsSUFBSSxDQUFDRSxnQkFBRCxDQUFuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBGQywgbWVtbyB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IGNuIGZyb20gJ2NsYXNzbmFtZXMnXG5cbmltcG9ydCAnLi9BcHBTaWRlYmFyVG9nZ2xlLnNjc3MnXG5cbmludGVyZmFjZSBJUHJvcHMge1xuICBjb2xsYXBzZWQ/OiBib29sZWFuXG59XG5cbmNvbnN0IEFwcFNpZGViYXJUb2dnbGU6IEZDPElQcm9wcz4gPSAocHJvcHMpID0+IHtcbiAgY29uc3QgeyBjb2xsYXBzZWQgfSA9IHByb3BzXG5cbiAgY29uc3QgaWNvbkNsYXNzTmFtZSA9IGNuKCdhbnQtbWVudS1zdWJtZW51LWFycm93Jywge1xuICAgIGNvbGxhcHNlZCxcbiAgfSlcblxuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPSdBcHBTaWRlYmFyVG9nZ2xlJz5cbiAgICAgIDxpIGNsYXNzTmFtZT17aWNvbkNsYXNzTmFtZX0gLz5cbiAgICA8L2Rpdj5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBtZW1vKEFwcFNpZGViYXJUb2dnbGUpXG4iXX0=
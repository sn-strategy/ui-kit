import React from 'react';
import './AppSidebarLogo.scss';
interface IProps {
    logo: string;
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

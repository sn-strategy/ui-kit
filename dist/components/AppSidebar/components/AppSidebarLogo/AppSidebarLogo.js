import React from 'react';

(function () {
  var styles = ".AppSidebarLogo {\n  height: 4.8125rem;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  margin: 2rem 1.625rem 1rem;\n}\n.AppSidebarLogo__container {\n  height: 4.8125rem;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  padding: 0 1.5rem;\n  border: 1px solid #f2f1f1;\n  border-radius: 0.5rem;\n}\n.AppSidebarLogo__image {\n  max-width: 100%;\n  max-height: 100%;\n  width: 100%;\n}\n\n.ant-layout-sider-collapsed .AppSidebarLogo {\n  margin-inline-start: 0.5rem;\n  margin-inline-end: 0.5rem;\n}\n.ant-layout-sider-collapsed .AppSidebarLogo__container {\n  height: 2.5rem;\n  padding: 0 0.25rem;\n}";
  var fileName = "AppSidebarLogo_AppSidebarLogo";
  var element = document.querySelector("style[data-sass-component='AppSidebarLogo_AppSidebarLogo']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var AppSidebarLogo = function AppSidebarLogo(props) {
  var logo = props.logo;
  return /*#__PURE__*/React.createElement("div", {
    className: "AppSidebarLogo"
  }, /*#__PURE__*/React.createElement("div", {
    className: "AppSidebarLogo__container"
  }, /*#__PURE__*/React.createElement("img", {
    className: "AppSidebarLogo__image",
    src: logo,
    alt: "logo"
  })));
};

export default /*#__PURE__*/React.memo(AppSidebarLogo);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9BcHBTaWRlYmFyL2NvbXBvbmVudHMvQXBwU2lkZWJhckxvZ28vQXBwU2lkZWJhckxvZ28udHN4Il0sIm5hbWVzIjpbIlJlYWN0IiwiQXBwU2lkZWJhckxvZ28iLCJwcm9wcyIsImxvZ28iLCJtZW1vIl0sIm1hcHBpbmdzIjoiQUFBQSxPQUFPQSxLQUFQLE1BQWtCLE9BQWxCOzs7Ozs7Ozs7Ozs7Ozs7OztBQVFBLElBQU1DLGNBQWdDLEdBQUcsU0FBbkNBLGNBQW1DLENBQUNDLEtBQUQsRUFBVztBQUNsRCxNQUFRQyxJQUFSLEdBQWlCRCxLQUFqQixDQUFRQyxJQUFSO0FBRUEsc0JBQ0U7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLElBQUEsU0FBUyxFQUFDLHVCQUFmO0FBQXVDLElBQUEsR0FBRyxFQUFFQSxJQUE1QztBQUFrRCxJQUFBLEdBQUcsRUFBQztBQUF0RCxJQURGLENBREYsQ0FERjtBQU9ELENBVkQ7O0FBWUEsNEJBQWVILEtBQUssQ0FBQ0ksSUFBTixDQUFXSCxjQUFYLENBQWYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5cbmltcG9ydCAnLi9BcHBTaWRlYmFyTG9nby5zY3NzJ1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgbG9nbzogc3RyaW5nXG59XG5cbmNvbnN0IEFwcFNpZGViYXJMb2dvOiBSZWFjdC5GQzxJUHJvcHM+ID0gKHByb3BzKSA9PiB7XG4gIGNvbnN0IHsgbG9nbyB9ID0gcHJvcHNcblxuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPSdBcHBTaWRlYmFyTG9nbyc+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nQXBwU2lkZWJhckxvZ29fX2NvbnRhaW5lcic+XG4gICAgICAgIDxpbWcgY2xhc3NOYW1lPSdBcHBTaWRlYmFyTG9nb19faW1hZ2UnIHNyYz17bG9nb30gYWx0PSdsb2dvJyAvPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhBcHBTaWRlYmFyTG9nbylcbiJdfQ==
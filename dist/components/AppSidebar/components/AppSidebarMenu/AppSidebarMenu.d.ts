import React from 'react';
import type { IMenuItem } from '../../../../models/menu';
import './AppSidebarMenu.scss';
interface IProps {
    items: IMenuItem[];
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

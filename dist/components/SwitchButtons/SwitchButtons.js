function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { memo } from 'react';
import { Radio } from 'antd';

(function () {
  var styles = ".SwitchButtons {\n  width: max-content;\n  display: flex;\n  align-items: center;\n  justify-content: flex-end;\n  padding: 0.25rem;\n  margin: auto 0 auto auto;\n  border-radius: 0.5rem;\n  background-color: #f2f1f1;\n  box-shadow: inset 0px 1px 2px rgba(97, 97, 97, 0.2), inset 0px 2px 4px rgba(97, 97, 97, 0.2);\n}\n.SwitchButtons .ant-radio-button-wrapper {\n  color: #616161;\n  margin: 0;\n  border: none !important;\n  border-radius: 0.25rem;\n  background-color: transparent;\n}\n.SwitchButtons .ant-radio-button-wrapper:before {\n  content: none;\n}\n.SwitchButtons .ant-radio-button-wrapper-checked {\n  color: #ffffff;\n  background-color: #412d3e !important;\n}\n.SwitchButtons .ant-radio-button-wrapper:not(.SwitchButtons .ant-radio-button-wrapper-checked):hover {\n  color: #412d3e;\n}\n.SwitchButtons .ant-radio-button-wrapper-disabled {\n  opacity: 0.5;\n}\n.SwitchButtons.ant-radio-group-small .ant-radio-button-wrapper {\n  height: 1rem;\n  font-size: 0.625rem;\n  line-height: 1rem;\n}";
  var fileName = "SwitchButtons_SwitchButtons";
  var element = document.querySelector("style[data-sass-component='SwitchButtons_SwitchButtons']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var SwitchButtons = function SwitchButtons(props) {
  return /*#__PURE__*/React.createElement(Radio.Group, _extends({
    className: "SwitchButtons",
    optionType: "button",
    buttonStyle: "solid"
  }, props));
};

export default /*#__PURE__*/memo(SwitchButtons);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Td2l0Y2hCdXR0b25zL1N3aXRjaEJ1dHRvbnMudHN4Il0sIm5hbWVzIjpbIlJlYWN0IiwibWVtbyIsIlJhZGlvIiwiU3dpdGNoQnV0dG9ucyIsInByb3BzIl0sIm1hcHBpbmdzIjoiOztBQUFBLE9BQU9BLEtBQVAsSUFBZ0JDLElBQWhCLFFBQTRCLE9BQTVCO0FBQ0EsU0FBU0MsS0FBVCxRQUF1QyxNQUF2Qzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFNQSxJQUFNQyxhQUErQixHQUFHLFNBQWxDQSxhQUFrQyxDQUFDQyxLQUFELEVBQVc7QUFDakQsc0JBQ0Usb0JBQUMsS0FBRCxDQUFPLEtBQVA7QUFDRSxJQUFBLFNBQVMsRUFBQyxlQURaO0FBRUUsSUFBQSxVQUFVLEVBQUMsUUFGYjtBQUdFLElBQUEsV0FBVyxFQUFDO0FBSGQsS0FJTUEsS0FKTixFQURGO0FBUUQsQ0FURDs7QUFXQSw0QkFBZUgsSUFBSSxDQUFDRSxhQUFELENBQW5CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IG1lbW8gfSBmcm9tICdyZWFjdCdcbmltcG9ydCB7IFJhZGlvLCBSYWRpb0dyb3VwUHJvcHMgfSBmcm9tICdhbnRkJ1xuXG5pbXBvcnQgJy4vU3dpdGNoQnV0dG9ucy5zY3NzJ1xuXG50eXBlIElQcm9wcyA9IFJhZGlvR3JvdXBQcm9wc1xuXG5jb25zdCBTd2l0Y2hCdXR0b25zOiBSZWFjdC5GQzxJUHJvcHM+ID0gKHByb3BzKSA9PiB7XG4gIHJldHVybiAoXG4gICAgPFJhZGlvLkdyb3VwXG4gICAgICBjbGFzc05hbWU9J1N3aXRjaEJ1dHRvbnMnXG4gICAgICBvcHRpb25UeXBlPSdidXR0b24nXG4gICAgICBidXR0b25TdHlsZT0nc29saWQnXG4gICAgICB7Li4ucHJvcHN9XG4gICAgLz5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBtZW1vKFN3aXRjaEJ1dHRvbnMpXG4iXX0=
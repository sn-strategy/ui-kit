import React, { ReactNode } from 'react';
import { ModalProps } from 'antd';
import './Modal.scss';
export interface IModalProps extends ModalProps {
    children?: ReactNode | undefined;
}
declare const _default: React.NamedExoticComponent<IModalProps>;
export default _default;

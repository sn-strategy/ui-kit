var _excluded = ["type"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import { Modal as AntModal } from 'antd';
export default function modalNotification(_ref) {
  var type = _ref.type,
      props = _objectWithoutProperties(_ref, _excluded);

  AntModal[type](_objectSpread({
    className: 'Modal Modal_notification'
  }, props));
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Nb2RhbC9tb2RhbE5vdGlmaWNhdGlvbi50cyJdLCJuYW1lcyI6WyJNb2RhbCIsIkFudE1vZGFsIiwibW9kYWxOb3RpZmljYXRpb24iLCJ0eXBlIiwicHJvcHMiLCJjbGFzc05hbWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLFNBQVNBLEtBQUssSUFBSUMsUUFBbEIsUUFBa0MsTUFBbEM7QUFNQSxlQUFlLFNBQVNDLGlCQUFULE9BQThEO0FBQUEsTUFBaENDLElBQWdDLFFBQWhDQSxJQUFnQztBQUFBLE1BQXZCQyxLQUF1Qjs7QUFDM0VILEVBQUFBLFFBQVEsQ0FBQ0UsSUFBRCxDQUFSO0FBQ0VFLElBQUFBLFNBQVMsRUFBRTtBQURiLEtBRUtELEtBRkw7QUFJRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vZGFsIGFzIEFudE1vZGFsIH0gZnJvbSAnYW50ZCdcbmltcG9ydCB7IE1vZGFsRnVuY1Byb3BzIH0gZnJvbSAnYW50ZC9saWIvbW9kYWwvTW9kYWwnXG5cbmludGVyZmFjZSBJUHJvcHMgZXh0ZW5kcyBNb2RhbEZ1bmNQcm9wcyB7XG4gIHR5cGU6ICdpbmZvJyB8ICdzdWNjZXNzJyB8ICdlcnJvcicgfCAnd2FybmluZydcbn1cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIG1vZGFsTm90aWZpY2F0aW9uICh7IHR5cGUsIC4uLnByb3BzIH06IElQcm9wcyk6IHZvaWQge1xuICBBbnRNb2RhbFt0eXBlXSh7XG4gICAgY2xhc3NOYW1lOiAnTW9kYWwgTW9kYWxfbm90aWZpY2F0aW9uJyxcbiAgICAuLi5wcm9wcyxcbiAgfSlcbn1cbiJdfQ==
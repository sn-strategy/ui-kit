import { ModalFuncProps } from 'antd/lib/modal/Modal';
interface IProps extends ModalFuncProps {
    type: 'info' | 'success' | 'error' | 'warning';
}
export default function modalNotification({ type, ...props }: IProps): void;
export {};

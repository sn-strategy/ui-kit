var _excluded = ["children"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { memo } from 'react';
import { Modal as AntModal } from 'antd';

(function () {
  var styles = ".Modal .ant-modal-content {\n  background: #ffffff;\n  border: 1px solid #412d3e;\n  box-sizing: border-box;\n  border-radius: 0.5rem;\n  position: relative;\n}\n.Modal .ant-modal-header {\n  padding: 1rem 1.5rem;\n  background: transparent;\n  border: none;\n  height: 4.5rem;\n  display: flex;\n  align-items: center;\n}\n.Modal .ant-modal-title {\n  font-style: normal;\n  font-weight: 700;\n  font-size: 1.125rem;\n  line-height: 1.25rem;\n  letter-spacing: 0.04em;\n  color: #413045;\n}\n.Modal .ant-modal-body {\n  padding: 0 1.5rem 1.5rem;\n}\n.Modal_notification .ant-modal-body {\n  padding-top: 1.5rem;\n}\n.Modal .ant-modal-close {\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 10;\n  padding: 0;\n  color: rgba(0, 0, 0, 0.45);\n  font-weight: 700;\n  line-height: 1;\n  text-decoration: none;\n  background: transparent;\n  border: 0;\n  outline: 0;\n  cursor: pointer;\n  transition: color 0.3s;\n}\n.Modal .ant-modal-close-x {\n  height: 4.5rem;\n  width: 4.5rem;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  color: #413045;\n}\n.Modal .ant-btn {\n  height: 2.5rem;\n  padding: 0.5rem 1.5rem;\n  color: #412d3e;\n  border-radius: 0.5rem;\n  transition-duration: 0.2s;\n  background: #ffffff;\n  border: 1px solid #412d3e;\n}\n.Modal .ant-btn:hover {\n  border: 1px solid #795a77;\n  color: #795a77;\n}\n.Modal .ant-btn[disabled] {\n  background: #e5e5e5;\n}\n.Modal .ant-btn[disabled]:hover {\n  background: #e5e5e5;\n}\n.Modal .ant-btn-primary {\n  color: #ffffff;\n  background: #412d3e;\n  border: none;\n}\n.Modal .ant-btn-primary:hover {\n  background: #795a77;\n  color: #ffffff;\n}\n\n[dir=rtl] .Modal .ant-modal-close {\n  left: 0;\n  right: auto;\n}";
  var fileName = "Modal_Modal";
  var element = document.querySelector("style[data-sass-component='Modal_Modal']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var Modal = function Modal(_ref) {
  var children = _ref.children,
      other = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement(AntModal, _extends({
    className: "Modal"
  }, other), children);
};

export default /*#__PURE__*/memo(Modal);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Nb2RhbC9Nb2RhbC50c3giXSwibmFtZXMiOlsiUmVhY3QiLCJtZW1vIiwiTW9kYWwiLCJBbnRNb2RhbCIsImNoaWxkcmVuIiwib3RoZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBT0EsS0FBUCxJQUFvQkMsSUFBcEIsUUFBMkMsT0FBM0M7QUFDQSxTQUFTQyxLQUFLLElBQUlDLFFBQWxCLFFBQThDLE1BQTlDOzs7Ozs7Ozs7Ozs7Ozs7OztBQU9BLElBQU1ELEtBQXNCLEdBQUcsU0FBekJBLEtBQXlCLE9BQTRCO0FBQUEsTUFBekJFLFFBQXlCLFFBQXpCQSxRQUF5QjtBQUFBLE1BQVpDLEtBQVk7O0FBQ3pELHNCQUNFLG9CQUFDLFFBQUQ7QUFBVSxJQUFBLFNBQVMsRUFBQztBQUFwQixLQUFnQ0EsS0FBaEMsR0FDR0QsUUFESCxDQURGO0FBS0QsQ0FORDs7QUFRQSw0QkFBZUgsSUFBSSxDQUFDQyxLQUFELENBQW5CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IEZDLCBtZW1vLCBSZWFjdE5vZGUgfSBmcm9tICdyZWFjdCdcbmltcG9ydCB7IE1vZGFsIGFzIEFudE1vZGFsLCBNb2RhbFByb3BzIH0gZnJvbSAnYW50ZCdcbmltcG9ydCAnLi9Nb2RhbC5zY3NzJ1xuXG5leHBvcnQgaW50ZXJmYWNlIElNb2RhbFByb3BzIGV4dGVuZHMgTW9kYWxQcm9wcyB7XG4gIGNoaWxkcmVuPzogUmVhY3ROb2RlIHwgdW5kZWZpbmVkXG59XG5cbmNvbnN0IE1vZGFsOiBGQzxJTW9kYWxQcm9wcz4gPSAoeyBjaGlsZHJlbiwgLi4ub3RoZXIgfSkgPT4ge1xuICByZXR1cm4gKFxuICAgIDxBbnRNb2RhbCBjbGFzc05hbWU9J01vZGFsJyB7Li4ub3RoZXJ9PlxuICAgICAge2NoaWxkcmVufVxuICAgIDwvQW50TW9kYWw+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgbWVtbyhNb2RhbClcbiJdfQ==
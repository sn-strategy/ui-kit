function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/* eslint-disable no-unused-vars */
import React, { memo, useCallback } from 'react';
import { useController } from 'react-hook-form';
import { Radio as AntRadio, Checkbox as AntCheckbox, Tree as AntTree } from 'antd';
import IconChevron from '../../Icons/IconChevron';

(function () {
  var styles = ".Tree .ant-tree-treenode {\n  padding-bottom: 1rem;\n}\n.Tree .ant-tree-treenode:last-child {\n  padding-bottom: 0;\n}\n.Tree .ant-tree-switcher {\n  width: 1.25rem;\n  height: 1.25rem;\n  align-self: center;\n}\n.Tree .ant-tree-switcher_open > *, .Tree .ant-tree-switcher_close > * {\n  width: 1.25rem;\n  height: 1.25rem;\n}\n.Tree .ant-tree-switcher_open > * {\n  transform: rotateZ(360deg);\n}\n.Tree .ant-tree-switcher_close > * {\n  transform: rotateZ(270deg);\n}\n.Tree .ant-tree-indent-unit {\n  width: 1.25rem;\n}\n.Tree .ant-tree-node-content-wrapper {\n  min-height: auto;\n  padding: 0;\n}\n.Tree .ant-tree-checkbox {\n  display: none;\n}";
  var fileName = "Tree_Tree";
  var element = document.querySelector("style[data-sass-component='Tree_Tree']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

export var ETreeNodeType;

(function (ETreeNodeType) {
  ETreeNodeType["RADIO"] = "radio";
  ETreeNodeType["CHECKBOX"] = "checkbox";
})(ETreeNodeType || (ETreeNodeType = {}));

var getCheckedKeys = function getCheckedKeys(data) {
  return Array.isArray(data) ? data : data.checked;
};

var getRadioNodes = function getRadioNodes(data) {
  return data.reduce(function (memo, item) {
    if (item.type === ETreeNodeType.RADIO) {
      memo.push(item);

      if (item.children) {
        memo = [].concat(_toConsumableArray(memo), _toConsumableArray(getRadioNodes(item.children)));
      }
    }

    return memo;
  }, []);
};

var getTreeNodeKeys = function getTreeNodeKeys(data) {
  return data.reduce(function (memo, item) {
    memo.push(item.key);

    if (item.children) {
      memo = [].concat(_toConsumableArray(memo), _toConsumableArray(getTreeNodeKeys(item.children)));
    }

    return memo;
  }, []);
};

var getNewCheckedKeys = function getNewCheckedKeys(treeData, oldCheckedKeys, oldHalfCheckedKeys, newCheckedKeys, newHalfCheckedKeys) {
  var oldKeys = [].concat(_toConsumableArray(oldCheckedKeys), _toConsumableArray(oldHalfCheckedKeys));
  var newKeys = [].concat(_toConsumableArray(newCheckedKeys), _toConsumableArray(newHalfCheckedKeys));
  var radioNodes = getRadioNodes(treeData);
  var deltaCheckedKeys = newKeys.filter(function (k) {
    return !oldKeys.includes(k);
  });
  var oldRadioNodes = radioNodes.filter(function (n) {
    return oldKeys.includes(n.key);
  });
  var deltaRadioNodes = radioNodes.filter(function (n) {
    return deltaCheckedKeys.includes(n.key);
  });
  var removedRadioNodes = oldRadioNodes.filter(function (o) {
    return deltaRadioNodes.some(function (n) {
      return n.groupName === o.groupName && o.key !== n.key;
    });
  });
  var removedKeys = getTreeNodeKeys(removedRadioNodes);
  return [newCheckedKeys.filter(function (k) {
    return !removedKeys.includes(k);
  }), newHalfCheckedKeys.filter(function (k) {
    return !removedKeys.includes(k);
  })];
};

var Tree = function Tree(props) {
  var name = props.name,
      _props$treeData = props.treeData,
      treeData = _props$treeData === void 0 ? [] : _props$treeData,
      control = props.control;

  var _useController = useController({
    name: name,
    control: control,
    defaultValue: {
      checkedKeys: [],
      halfCheckedKeys: []
    }
  }),
      _useController$field = _useController.field,
      onChange = _useController$field.onChange,
      value = _useController$field.value;

  var checkedKeys = value.checkedKeys,
      halfCheckedKeys = value.halfCheckedKeys;
  var handleChangeCheck = useCallback(function (keys, info) {
    var _getNewCheckedKeys = getNewCheckedKeys(treeData, checkedKeys, halfCheckedKeys, getCheckedKeys(keys), info.halfCheckedKeys),
        _getNewCheckedKeys2 = _slicedToArray(_getNewCheckedKeys, 2),
        newCheckedKeys = _getNewCheckedKeys2[0],
        newHalfCheckedKeys = _getNewCheckedKeys2[1];

    onChange({
      checkedKeys: newCheckedKeys,
      halfCheckedKeys: newHalfCheckedKeys
    });
  }, [treeData, checkedKeys, halfCheckedKeys, onChange]);
  var renderTreeNode = useCallback(function (node) {
    var isChecked = checkedKeys.includes(node.key);
    var isHalfChecked = halfCheckedKeys.includes(node.key);
    return node.type === ETreeNodeType.RADIO ? /*#__PURE__*/React.createElement(AntRadio, {
      checked: isChecked || isHalfChecked,
      disabled: node.disabled
    }, node.title) : /*#__PURE__*/React.createElement(AntCheckbox, {
      checked: isChecked,
      indeterminate: isHalfChecked,
      disabled: node.disabled
    }, node.title);
  }, [checkedKeys, halfCheckedKeys]);
  return /*#__PURE__*/React.createElement(AntTree, {
    className: "Tree",
    checkable: true,
    selectable: false,
    treeData: treeData,
    checkedKeys: checkedKeys,
    titleRender: renderTreeNode,
    switcherIcon: /*#__PURE__*/React.createElement(IconChevron, {
      color: "#412D3E"
    }),
    onCheck: handleChangeCheck
  });
};

export default /*#__PURE__*/memo(Tree);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Gb3JtRmllbGRzL1RyZWUvVHJlZS50c3giXSwibmFtZXMiOlsiUmVhY3QiLCJtZW1vIiwidXNlQ2FsbGJhY2siLCJ1c2VDb250cm9sbGVyIiwiUmFkaW8iLCJBbnRSYWRpbyIsIkNoZWNrYm94IiwiQW50Q2hlY2tib3giLCJUcmVlIiwiQW50VHJlZSIsIkljb25DaGV2cm9uIiwiRVRyZWVOb2RlVHlwZSIsImdldENoZWNrZWRLZXlzIiwiZGF0YSIsIkFycmF5IiwiaXNBcnJheSIsImNoZWNrZWQiLCJnZXRSYWRpb05vZGVzIiwicmVkdWNlIiwiaXRlbSIsInR5cGUiLCJSQURJTyIsInB1c2giLCJjaGlsZHJlbiIsImdldFRyZWVOb2RlS2V5cyIsImtleSIsImdldE5ld0NoZWNrZWRLZXlzIiwidHJlZURhdGEiLCJvbGRDaGVja2VkS2V5cyIsIm9sZEhhbGZDaGVja2VkS2V5cyIsIm5ld0NoZWNrZWRLZXlzIiwibmV3SGFsZkNoZWNrZWRLZXlzIiwib2xkS2V5cyIsIm5ld0tleXMiLCJyYWRpb05vZGVzIiwiZGVsdGFDaGVja2VkS2V5cyIsImZpbHRlciIsImsiLCJpbmNsdWRlcyIsIm9sZFJhZGlvTm9kZXMiLCJuIiwiZGVsdGFSYWRpb05vZGVzIiwicmVtb3ZlZFJhZGlvTm9kZXMiLCJvIiwic29tZSIsImdyb3VwTmFtZSIsInJlbW92ZWRLZXlzIiwicHJvcHMiLCJuYW1lIiwiY29udHJvbCIsImRlZmF1bHRWYWx1ZSIsImNoZWNrZWRLZXlzIiwiaGFsZkNoZWNrZWRLZXlzIiwiZmllbGQiLCJvbkNoYW5nZSIsInZhbHVlIiwiaGFuZGxlQ2hhbmdlQ2hlY2siLCJrZXlzIiwiaW5mbyIsInJlbmRlclRyZWVOb2RlIiwibm9kZSIsImlzQ2hlY2tlZCIsImlzSGFsZkNoZWNrZWQiLCJkaXNhYmxlZCIsInRpdGxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0EsT0FBT0EsS0FBUCxJQUFvQkMsSUFBcEIsRUFBMEJDLFdBQTFCLFFBQTZDLE9BQTdDO0FBQ0EsU0FBa0JDLGFBQWxCLFFBQXVDLGlCQUF2QztBQUNBLFNBQVNDLEtBQUssSUFBSUMsUUFBbEIsRUFBNEJDLFFBQVEsSUFBSUMsV0FBeEMsRUFBcURDLElBQUksSUFBSUMsT0FBN0QsUUFBdUYsTUFBdkY7QUFJQSxPQUFPQyxXQUFQLE1BQXdCLHlCQUF4Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJQSxXQUFZQyxhQUFaOztXQUFZQSxhO0FBQUFBLEVBQUFBLGE7QUFBQUEsRUFBQUEsYTtHQUFBQSxhLEtBQUFBLGE7O0FBNEJaLElBQU1DLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQ0MsSUFBRCxFQUFnQztBQUNyRCxTQUFPQyxLQUFLLENBQUNDLE9BQU4sQ0FBY0YsSUFBZCxJQUFzQkEsSUFBdEIsR0FBNkJBLElBQUksQ0FBQ0csT0FBekM7QUFDRCxDQUZEOztBQUlBLElBQU1DLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBQ0osSUFBRCxFQUF5QztBQUM3RCxTQUFPQSxJQUFJLENBQUNLLE1BQUwsQ0FBOEIsVUFBQ2pCLElBQUQsRUFBT2tCLElBQVAsRUFBZ0I7QUFDbkQsUUFBSUEsSUFBSSxDQUFDQyxJQUFMLEtBQWNULGFBQWEsQ0FBQ1UsS0FBaEMsRUFBdUM7QUFDckNwQixNQUFBQSxJQUFJLENBQUNxQixJQUFMLENBQVVILElBQVY7O0FBRUEsVUFBSUEsSUFBSSxDQUFDSSxRQUFULEVBQW1CO0FBQ2pCdEIsUUFBQUEsSUFBSSxnQ0FBT0EsSUFBUCxzQkFBZ0JnQixhQUFhLENBQUNFLElBQUksQ0FBQ0ksUUFBTixDQUE3QixFQUFKO0FBQ0Q7QUFDRjs7QUFDRCxXQUFPdEIsSUFBUDtBQUNELEdBVE0sRUFTSixFQVRJLENBQVA7QUFVRCxDQVhEOztBQWFBLElBQU11QixlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNYLElBQUQsRUFBOEI7QUFDcEQsU0FBT0EsSUFBSSxDQUFDSyxNQUFMLENBQW1CLFVBQUNqQixJQUFELEVBQU9rQixJQUFQLEVBQWdCO0FBQ3hDbEIsSUFBQUEsSUFBSSxDQUFDcUIsSUFBTCxDQUFVSCxJQUFJLENBQUNNLEdBQWY7O0FBQ0EsUUFBSU4sSUFBSSxDQUFDSSxRQUFULEVBQW1CO0FBQ2pCdEIsTUFBQUEsSUFBSSxnQ0FBT0EsSUFBUCxzQkFBZ0J1QixlQUFlLENBQUNMLElBQUksQ0FBQ0ksUUFBTixDQUEvQixFQUFKO0FBQ0Q7O0FBQ0QsV0FBT3RCLElBQVA7QUFDRCxHQU5NLEVBTUosRUFOSSxDQUFQO0FBT0QsQ0FSRDs7QUFVQSxJQUFNeUIsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUN4QkMsUUFEd0IsRUFFeEJDLGNBRndCLEVBR3hCQyxrQkFId0IsRUFJeEJDLGNBSndCLEVBS3hCQyxrQkFMd0IsRUFNTDtBQUNuQixNQUFNQyxPQUFPLGdDQUFPSixjQUFQLHNCQUEwQkMsa0JBQTFCLEVBQWI7QUFDQSxNQUFNSSxPQUFPLGdDQUFPSCxjQUFQLHNCQUEwQkMsa0JBQTFCLEVBQWI7QUFDQSxNQUFNRyxVQUFVLEdBQUdqQixhQUFhLENBQUNVLFFBQUQsQ0FBaEM7QUFDQSxNQUFNUSxnQkFBZ0IsR0FBR0YsT0FBTyxDQUFDRyxNQUFSLENBQWUsVUFBQ0MsQ0FBRDtBQUFBLFdBQU8sQ0FBQ0wsT0FBTyxDQUFDTSxRQUFSLENBQWlCRCxDQUFqQixDQUFSO0FBQUEsR0FBZixDQUF6QjtBQUVBLE1BQU1FLGFBQWEsR0FBR0wsVUFBVSxDQUFDRSxNQUFYLENBQWtCLFVBQUNJLENBQUQ7QUFBQSxXQUFPUixPQUFPLENBQUNNLFFBQVIsQ0FBaUJFLENBQUMsQ0FBQ2YsR0FBbkIsQ0FBUDtBQUFBLEdBQWxCLENBQXRCO0FBQ0EsTUFBTWdCLGVBQWUsR0FBR1AsVUFBVSxDQUFDRSxNQUFYLENBQWtCLFVBQUNJLENBQUQ7QUFBQSxXQUFPTCxnQkFBZ0IsQ0FBQ0csUUFBakIsQ0FBMEJFLENBQUMsQ0FBQ2YsR0FBNUIsQ0FBUDtBQUFBLEdBQWxCLENBQXhCO0FBRUEsTUFBTWlCLGlCQUFpQixHQUFHSCxhQUFhLENBQUNILE1BQWQsQ0FBcUIsVUFBQ08sQ0FBRCxFQUFPO0FBQ3BELFdBQU9GLGVBQWUsQ0FBQ0csSUFBaEIsQ0FBcUIsVUFBQ0osQ0FBRDtBQUFBLGFBQU9BLENBQUMsQ0FBQ0ssU0FBRixLQUFnQkYsQ0FBQyxDQUFDRSxTQUFsQixJQUErQkYsQ0FBQyxDQUFDbEIsR0FBRixLQUFVZSxDQUFDLENBQUNmLEdBQWxEO0FBQUEsS0FBckIsQ0FBUDtBQUNELEdBRnlCLENBQTFCO0FBSUEsTUFBTXFCLFdBQVcsR0FBR3RCLGVBQWUsQ0FBQ2tCLGlCQUFELENBQW5DO0FBQ0EsU0FBTyxDQUNMWixjQUFjLENBQUNNLE1BQWYsQ0FBc0IsVUFBQ0MsQ0FBRDtBQUFBLFdBQU8sQ0FBQ1MsV0FBVyxDQUFDUixRQUFaLENBQXFCRCxDQUFyQixDQUFSO0FBQUEsR0FBdEIsQ0FESyxFQUVMTixrQkFBa0IsQ0FBQ0ssTUFBbkIsQ0FBMEIsVUFBQ0MsQ0FBRDtBQUFBLFdBQU8sQ0FBQ1MsV0FBVyxDQUFDUixRQUFaLENBQXFCRCxDQUFyQixDQUFSO0FBQUEsR0FBMUIsQ0FGSyxDQUFQO0FBSUQsQ0F4QkQ7O0FBMEJBLElBQU03QixJQUFnQixHQUFHLFNBQW5CQSxJQUFtQixDQUFDdUMsS0FBRCxFQUFXO0FBQ2xDLE1BQVFDLElBQVIsR0FBeUNELEtBQXpDLENBQVFDLElBQVI7QUFBQSx3QkFBeUNELEtBQXpDLENBQWNwQixRQUFkO0FBQUEsTUFBY0EsUUFBZCxnQ0FBeUIsRUFBekI7QUFBQSxNQUE2QnNCLE9BQTdCLEdBQXlDRixLQUF6QyxDQUE2QkUsT0FBN0I7O0FBRUEsdUJBS0k5QyxhQUFhLENBQUM7QUFDaEI2QyxJQUFBQSxJQUFJLEVBQUpBLElBRGdCO0FBRWhCQyxJQUFBQSxPQUFPLEVBQVBBLE9BRmdCO0FBR2hCQyxJQUFBQSxZQUFZLEVBQUU7QUFBRUMsTUFBQUEsV0FBVyxFQUFFLEVBQWY7QUFBbUJDLE1BQUFBLGVBQWUsRUFBRTtBQUFwQztBQUhFLEdBQUQsQ0FMakI7QUFBQSw0Q0FDRUMsS0FERjtBQUFBLE1BRUlDLFFBRkosd0JBRUlBLFFBRko7QUFBQSxNQUdJQyxLQUhKLHdCQUdJQSxLQUhKOztBQVdBLE1BQVFKLFdBQVIsR0FBeUNJLEtBQXpDLENBQVFKLFdBQVI7QUFBQSxNQUFxQkMsZUFBckIsR0FBeUNHLEtBQXpDLENBQXFCSCxlQUFyQjtBQUVBLE1BQU1JLGlCQUFpQixHQUFHdEQsV0FBVyxDQUFDLFVBQUN1RCxJQUFELEVBQXNCQyxJQUF0QixFQUFvQztBQUN4RSw2QkFBNkNoQyxpQkFBaUIsQ0FDNURDLFFBRDRELEVBRTVEd0IsV0FGNEQsRUFHNURDLGVBSDRELEVBSTVEeEMsY0FBYyxDQUFDNkMsSUFBRCxDQUo4QyxFQUs1REMsSUFBSSxDQUFDTixlQUx1RCxDQUE5RDtBQUFBO0FBQUEsUUFBT3RCLGNBQVA7QUFBQSxRQUF1QkMsa0JBQXZCOztBQU9BdUIsSUFBQUEsUUFBUSxDQUFDO0FBQUVILE1BQUFBLFdBQVcsRUFBRXJCLGNBQWY7QUFBK0JzQixNQUFBQSxlQUFlLEVBQUVyQjtBQUFoRCxLQUFELENBQVI7QUFDRCxHQVRvQyxFQVNsQyxDQUFDSixRQUFELEVBQVd3QixXQUFYLEVBQXdCQyxlQUF4QixFQUF5Q0UsUUFBekMsQ0FUa0MsQ0FBckM7QUFXQSxNQUFNSyxjQUFjLEdBQUd6RCxXQUFXLENBQUMsVUFBQzBELElBQUQsRUFBcUI7QUFDdEQsUUFBTUMsU0FBUyxHQUFHVixXQUFXLENBQUNiLFFBQVosQ0FBcUJzQixJQUFJLENBQUNuQyxHQUExQixDQUFsQjtBQUNBLFFBQU1xQyxhQUFhLEdBQUdWLGVBQWUsQ0FBQ2QsUUFBaEIsQ0FBeUJzQixJQUFJLENBQUNuQyxHQUE5QixDQUF0QjtBQUVBLFdBQU9tQyxJQUFJLENBQUN4QyxJQUFMLEtBQWNULGFBQWEsQ0FBQ1UsS0FBNUIsZ0JBQ0wsb0JBQUMsUUFBRDtBQUFVLE1BQUEsT0FBTyxFQUFFd0MsU0FBUyxJQUFJQyxhQUFoQztBQUErQyxNQUFBLFFBQVEsRUFBRUYsSUFBSSxDQUFDRztBQUE5RCxPQUNHSCxJQUFJLENBQUNJLEtBRFIsQ0FESyxnQkFLTCxvQkFBQyxXQUFEO0FBQWEsTUFBQSxPQUFPLEVBQUVILFNBQXRCO0FBQWlDLE1BQUEsYUFBYSxFQUFFQyxhQUFoRDtBQUErRCxNQUFBLFFBQVEsRUFBRUYsSUFBSSxDQUFDRztBQUE5RSxPQUNHSCxJQUFJLENBQUNJLEtBRFIsQ0FMRjtBQVNELEdBYmlDLEVBYS9CLENBQUNiLFdBQUQsRUFBY0MsZUFBZCxDQWIrQixDQUFsQztBQWVBLHNCQUNFLG9CQUFDLE9BQUQ7QUFDRSxJQUFBLFNBQVMsRUFBQyxNQURaO0FBRUUsSUFBQSxTQUFTLEVBQUUsSUFGYjtBQUdFLElBQUEsVUFBVSxFQUFFLEtBSGQ7QUFJRSxJQUFBLFFBQVEsRUFBRXpCLFFBSlo7QUFLRSxJQUFBLFdBQVcsRUFBRXdCLFdBTGY7QUFNRSxJQUFBLFdBQVcsRUFBRVEsY0FOZjtBQU9FLElBQUEsWUFBWSxlQUFFLG9CQUFDLFdBQUQ7QUFBYSxNQUFBLEtBQUssRUFBQztBQUFuQixNQVBoQjtBQVFFLElBQUEsT0FBTyxFQUFFSDtBQVJYLElBREY7QUFZRCxDQXRERDs7QUF3REEsNEJBQWV2RCxJQUFJLENBQUNPLElBQUQsQ0FBbkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBlc2xpbnQtZGlzYWJsZSBuby11bnVzZWQtdmFycyAqL1xuaW1wb3J0IFJlYWN0LCB7IEZDLCBtZW1vLCB1c2VDYWxsYmFjayB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IHsgQ29udHJvbCwgdXNlQ29udHJvbGxlciB9IGZyb20gJ3JlYWN0LWhvb2stZm9ybSdcbmltcG9ydCB7IFJhZGlvIGFzIEFudFJhZGlvLCBDaGVja2JveCBhcyBBbnRDaGVja2JveCwgVHJlZSBhcyBBbnRUcmVlLCBUcmVlUHJvcHMgfSBmcm9tICdhbnRkJ1xuaW1wb3J0IHsgRGF0YU5vZGUgfSBmcm9tICdhbnRkL2xpYi90cmVlJ1xuaW1wb3J0IHsgS2V5IH0gZnJvbSAnYW50ZC9saWIvdGFibGUvaW50ZXJmYWNlJ1xuXG5pbXBvcnQgSWNvbkNoZXZyb24gZnJvbSAnLi4vLi4vSWNvbnMvSWNvbkNoZXZyb24nXG5cbmltcG9ydCAnLi9UcmVlLnNjc3MnXG5cbmV4cG9ydCBlbnVtIEVUcmVlTm9kZVR5cGUge1xuICBSQURJTyA9ICdyYWRpbycsXG4gIENIRUNLQk9YID0gJ2NoZWNrYm94Jyxcbn1cblxuaW50ZXJmYWNlIElDaGVja2JveFRyZWVOb2RlIGV4dGVuZHMgRGF0YU5vZGUge1xuICB0eXBlPzogRVRyZWVOb2RlVHlwZS5DSEVDS0JPWFxufVxuXG5pbnRlcmZhY2UgSVJhZGlvVHJlZU5vZGUgZXh0ZW5kcyBEYXRhTm9kZSB7XG4gIHR5cGU6IEVUcmVlTm9kZVR5cGUuUkFESU9cbiAgZ3JvdXBOYW1lOiBzdHJpbmdcbn1cblxuZXhwb3J0IHR5cGUgVFRyZWVOb2RlID0gSUNoZWNrYm94VHJlZU5vZGUgfCBJUmFkaW9UcmVlTm9kZVxuXG5leHBvcnQgaW50ZXJmYWNlIElDaGVja2VkS2V5cyB7XG4gIGNoZWNrZWQ6IEtleVtdXG4gIGhhbGZDaGVja2VkOiBLZXlbXVxufVxuXG50eXBlIFRDaGVja2VkVmFsdWUgPSBJQ2hlY2tlZEtleXMgfCBLZXlbXVxuXG5pbnRlcmZhY2UgSVByb3BzIGV4dGVuZHMgVHJlZVByb3BzIHtcbiAgbmFtZTogc3RyaW5nXG4gIGNvbnRyb2w6IENvbnRyb2xcbn1cblxuY29uc3QgZ2V0Q2hlY2tlZEtleXMgPSAoZGF0YTogVENoZWNrZWRWYWx1ZSk6IEtleVtdID0+IHtcbiAgcmV0dXJuIEFycmF5LmlzQXJyYXkoZGF0YSkgPyBkYXRhIDogZGF0YS5jaGVja2VkXG59XG5cbmNvbnN0IGdldFJhZGlvTm9kZXMgPSAoZGF0YTogVFRyZWVOb2RlW10pOiBJUmFkaW9UcmVlTm9kZVtdID0+IHtcbiAgcmV0dXJuIGRhdGEucmVkdWNlPElSYWRpb1RyZWVOb2RlW10+KChtZW1vLCBpdGVtKSA9PiB7XG4gICAgaWYgKGl0ZW0udHlwZSA9PT0gRVRyZWVOb2RlVHlwZS5SQURJTykge1xuICAgICAgbWVtby5wdXNoKGl0ZW0pXG5cbiAgICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XG4gICAgICAgIG1lbW8gPSBbLi4ubWVtbywgLi4uZ2V0UmFkaW9Ob2RlcyhpdGVtLmNoaWxkcmVuKV1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG1lbW9cbiAgfSwgW10pXG59XG5cbmNvbnN0IGdldFRyZWVOb2RlS2V5cyA9IChkYXRhOiBUVHJlZU5vZGVbXSk6IEtleVtdID0+IHtcbiAgcmV0dXJuIGRhdGEucmVkdWNlPEtleVtdPigobWVtbywgaXRlbSkgPT4ge1xuICAgIG1lbW8ucHVzaChpdGVtLmtleSlcbiAgICBpZiAoaXRlbS5jaGlsZHJlbikge1xuICAgICAgbWVtbyA9IFsuLi5tZW1vLCAuLi5nZXRUcmVlTm9kZUtleXMoaXRlbS5jaGlsZHJlbildXG4gICAgfVxuICAgIHJldHVybiBtZW1vXG4gIH0sIFtdKVxufVxuXG5jb25zdCBnZXROZXdDaGVja2VkS2V5cyA9IChcbiAgdHJlZURhdGE6IFRUcmVlTm9kZVtdLFxuICBvbGRDaGVja2VkS2V5czogS2V5W10sXG4gIG9sZEhhbGZDaGVja2VkS2V5czogS2V5W10sXG4gIG5ld0NoZWNrZWRLZXlzOiBLZXlbXSxcbiAgbmV3SGFsZkNoZWNrZWRLZXlzOiBLZXlbXSxcbik6IFtLZXlbXSwgS2V5W11dID0+IHtcbiAgY29uc3Qgb2xkS2V5cyA9IFsuLi5vbGRDaGVja2VkS2V5cywgLi4ub2xkSGFsZkNoZWNrZWRLZXlzXVxuICBjb25zdCBuZXdLZXlzID0gWy4uLm5ld0NoZWNrZWRLZXlzLCAuLi5uZXdIYWxmQ2hlY2tlZEtleXNdXG4gIGNvbnN0IHJhZGlvTm9kZXMgPSBnZXRSYWRpb05vZGVzKHRyZWVEYXRhKVxuICBjb25zdCBkZWx0YUNoZWNrZWRLZXlzID0gbmV3S2V5cy5maWx0ZXIoKGspID0+ICFvbGRLZXlzLmluY2x1ZGVzKGspKVxuXG4gIGNvbnN0IG9sZFJhZGlvTm9kZXMgPSByYWRpb05vZGVzLmZpbHRlcigobikgPT4gb2xkS2V5cy5pbmNsdWRlcyhuLmtleSkpXG4gIGNvbnN0IGRlbHRhUmFkaW9Ob2RlcyA9IHJhZGlvTm9kZXMuZmlsdGVyKChuKSA9PiBkZWx0YUNoZWNrZWRLZXlzLmluY2x1ZGVzKG4ua2V5KSlcblxuICBjb25zdCByZW1vdmVkUmFkaW9Ob2RlcyA9IG9sZFJhZGlvTm9kZXMuZmlsdGVyKChvKSA9PiB7XG4gICAgcmV0dXJuIGRlbHRhUmFkaW9Ob2Rlcy5zb21lKChuKSA9PiBuLmdyb3VwTmFtZSA9PT0gby5ncm91cE5hbWUgJiYgby5rZXkgIT09IG4ua2V5KVxuICB9KVxuXG4gIGNvbnN0IHJlbW92ZWRLZXlzID0gZ2V0VHJlZU5vZGVLZXlzKHJlbW92ZWRSYWRpb05vZGVzKVxuICByZXR1cm4gW1xuICAgIG5ld0NoZWNrZWRLZXlzLmZpbHRlcigoaykgPT4gIXJlbW92ZWRLZXlzLmluY2x1ZGVzKGspKSxcbiAgICBuZXdIYWxmQ2hlY2tlZEtleXMuZmlsdGVyKChrKSA9PiAhcmVtb3ZlZEtleXMuaW5jbHVkZXMoaykpLFxuICBdXG59XG5cbmNvbnN0IFRyZWU6IEZDPElQcm9wcz4gPSAocHJvcHMpID0+IHtcbiAgY29uc3QgeyBuYW1lLCB0cmVlRGF0YSA9IFtdLCBjb250cm9sIH0gPSBwcm9wc1xuXG4gIGNvbnN0IHtcbiAgICBmaWVsZDoge1xuICAgICAgb25DaGFuZ2UsXG4gICAgICB2YWx1ZSxcbiAgICB9LFxuICB9ID0gdXNlQ29udHJvbGxlcih7XG4gICAgbmFtZSxcbiAgICBjb250cm9sLFxuICAgIGRlZmF1bHRWYWx1ZTogeyBjaGVja2VkS2V5czogW10sIGhhbGZDaGVja2VkS2V5czogW10gfSxcbiAgfSlcblxuICBjb25zdCB7IGNoZWNrZWRLZXlzLCBoYWxmQ2hlY2tlZEtleXMgfSA9IHZhbHVlXG5cbiAgY29uc3QgaGFuZGxlQ2hhbmdlQ2hlY2sgPSB1c2VDYWxsYmFjaygoa2V5czogVENoZWNrZWRWYWx1ZSwgaW5mbzogYW55KSA9PiB7XG4gICAgY29uc3QgW25ld0NoZWNrZWRLZXlzLCBuZXdIYWxmQ2hlY2tlZEtleXNdID0gZ2V0TmV3Q2hlY2tlZEtleXMoXG4gICAgICB0cmVlRGF0YSxcbiAgICAgIGNoZWNrZWRLZXlzLFxuICAgICAgaGFsZkNoZWNrZWRLZXlzLFxuICAgICAgZ2V0Q2hlY2tlZEtleXMoa2V5cyksXG4gICAgICBpbmZvLmhhbGZDaGVja2VkS2V5cyxcbiAgICApXG4gICAgb25DaGFuZ2UoeyBjaGVja2VkS2V5czogbmV3Q2hlY2tlZEtleXMsIGhhbGZDaGVja2VkS2V5czogbmV3SGFsZkNoZWNrZWRLZXlzIH0pXG4gIH0sIFt0cmVlRGF0YSwgY2hlY2tlZEtleXMsIGhhbGZDaGVja2VkS2V5cywgb25DaGFuZ2VdKVxuXG4gIGNvbnN0IHJlbmRlclRyZWVOb2RlID0gdXNlQ2FsbGJhY2soKG5vZGU6IFRUcmVlTm9kZSkgPT4ge1xuICAgIGNvbnN0IGlzQ2hlY2tlZCA9IGNoZWNrZWRLZXlzLmluY2x1ZGVzKG5vZGUua2V5KVxuICAgIGNvbnN0IGlzSGFsZkNoZWNrZWQgPSBoYWxmQ2hlY2tlZEtleXMuaW5jbHVkZXMobm9kZS5rZXkpXG5cbiAgICByZXR1cm4gbm9kZS50eXBlID09PSBFVHJlZU5vZGVUeXBlLlJBRElPID8gKFxuICAgICAgPEFudFJhZGlvIGNoZWNrZWQ9e2lzQ2hlY2tlZCB8fCBpc0hhbGZDaGVja2VkfSBkaXNhYmxlZD17bm9kZS5kaXNhYmxlZH0+XG4gICAgICAgIHtub2RlLnRpdGxlfVxuICAgICAgPC9BbnRSYWRpbz5cbiAgICApIDogKFxuICAgICAgPEFudENoZWNrYm94IGNoZWNrZWQ9e2lzQ2hlY2tlZH0gaW5kZXRlcm1pbmF0ZT17aXNIYWxmQ2hlY2tlZH0gZGlzYWJsZWQ9e25vZGUuZGlzYWJsZWR9PlxuICAgICAgICB7bm9kZS50aXRsZX1cbiAgICAgIDwvQW50Q2hlY2tib3g+XG4gICAgKVxuICB9LCBbY2hlY2tlZEtleXMsIGhhbGZDaGVja2VkS2V5c10pXG5cbiAgcmV0dXJuIChcbiAgICA8QW50VHJlZVxuICAgICAgY2xhc3NOYW1lPSdUcmVlJ1xuICAgICAgY2hlY2thYmxlPXt0cnVlfVxuICAgICAgc2VsZWN0YWJsZT17ZmFsc2V9XG4gICAgICB0cmVlRGF0YT17dHJlZURhdGF9XG4gICAgICBjaGVja2VkS2V5cz17Y2hlY2tlZEtleXN9XG4gICAgICB0aXRsZVJlbmRlcj17cmVuZGVyVHJlZU5vZGV9XG4gICAgICBzd2l0Y2hlckljb249ezxJY29uQ2hldnJvbiBjb2xvcj0nIzQxMkQzRScgLz59XG4gICAgICBvbkNoZWNrPXtoYW5kbGVDaGFuZ2VDaGVja31cbiAgICAvPlxuICApXG59XG5cbmV4cG9ydCBkZWZhdWx0IG1lbW8oVHJlZSlcbiJdfQ==
import React from 'react';
import { Control } from 'react-hook-form';
import { TreeProps } from 'antd';
import { DataNode } from 'antd/lib/tree';
import { Key } from 'antd/lib/table/interface';
import './Tree.scss';
export declare enum ETreeNodeType {
    RADIO = "radio",
    CHECKBOX = "checkbox"
}
interface ICheckboxTreeNode extends DataNode {
    type?: ETreeNodeType.CHECKBOX;
}
interface IRadioTreeNode extends DataNode {
    type: ETreeNodeType.RADIO;
    groupName: string;
}
export declare type TTreeNode = ICheckboxTreeNode | IRadioTreeNode;
export interface ICheckedKeys {
    checked: Key[];
    halfChecked: Key[];
}
interface IProps extends TreeProps {
    name: string;
    control: Control;
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

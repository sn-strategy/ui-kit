import React from 'react';
import { InputProps } from 'antd';
import { Control } from 'react-hook-form';
import './TextInput.scss';
interface IProps {
    name: string;
    label: string;
    inputProps?: InputProps;
    control: Control<any>;
    inputModifier?: (value?: string) => string;
}
declare const _default: React.MemoExoticComponent<(props: IProps) => JSX.Element>;
export default _default;

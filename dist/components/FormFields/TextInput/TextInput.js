function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { useCallback, useMemo } from 'react';
import { Input } from 'antd';
import { useController } from 'react-hook-form';

(function () {
  var styles = ".TextInput {\n  position: relative;\n  margin-bottom: 0.9375rem;\n}\n.TextInput__label {\n  position: relative;\n  width: max-content;\n  font-size: 0.75rem;\n  margin-bottom: -0.4375rem;\n  margin-inline-start: 0.5rem;\n  padding: 0 0.5rem;\n  border-radius: 0.5rem;\n  background-color: #ffffff;\n  z-index: 1;\n}\n.TextInput__error {\n  font-size: 12px;\n  color: #e1343e;\n}\n.TextInput__has-error .TextInput__label, .TextInput__has-error .TextInput__input {\n  color: #e1343e;\n}\n.TextInput__has-error .TextInput__input {\n  border-color: #e1343e;\n}";
  var fileName = "TextInput_TextInput";
  var element = document.querySelector("style[data-sass-component='TextInput_TextInput']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var TextInput = function TextInput(props) {
  var name = props.name,
      label = props.label,
      _props$inputProps = props.inputProps,
      inputProps = _props$inputProps === void 0 ? {} : _props$inputProps,
      control = props.control,
      inputModifier = props.inputModifier;

  var _useController = useController({
    name: name,
    control: control
  }),
      field = _useController.field,
      fieldState = _useController.fieldState;

  var error = useMemo(function () {
    var _fieldState$error;

    return fieldState === null || fieldState === void 0 ? void 0 : (_fieldState$error = fieldState.error) === null || _fieldState$error === void 0 ? void 0 : _fieldState$error.message;
  }, [fieldState === null || fieldState === void 0 ? void 0 : fieldState.error]);
  var handleChange = useCallback(function (e) {
    var value = e.target.value;
    var modifiedValue = inputModifier ? inputModifier(value) : value;
    field.onChange(modifiedValue);
  }, [field, inputModifier]);
  var InputComponent = useMemo(function () {
    var commonProps = _objectSpread(_objectSpread({}, inputProps), {}, {
      name: name,
      id: name,
      value: field.value,
      onChange: handleChange,
      className: 'TextInput__input',
      onBlur: field.onBlur
    });

    switch (inputProps.type) {
      case 'password':
        return /*#__PURE__*/React.createElement(Input.Password, commonProps);
    }

    return /*#__PURE__*/React.createElement(Input, commonProps);
  }, [field.onBlur, field.value, handleChange, inputProps, name]);
  return /*#__PURE__*/React.createElement("div", {
    className: "TextInput ".concat(error ? 'TextInput__has-error' : '')
  }, /*#__PURE__*/React.createElement("div", {
    className: "TextInput__label"
  }, label), InputComponent, error ? /*#__PURE__*/React.createElement("div", {
    className: "TextInput__error"
  }, error) : null);
};

export default /*#__PURE__*/React.memo(TextInput);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Gb3JtRmllbGRzL1RleHRJbnB1dC9UZXh0SW5wdXQudHN4Il0sIm5hbWVzIjpbIlJlYWN0IiwidXNlQ2FsbGJhY2siLCJ1c2VNZW1vIiwiSW5wdXQiLCJ1c2VDb250cm9sbGVyIiwiVGV4dElucHV0IiwicHJvcHMiLCJuYW1lIiwibGFiZWwiLCJpbnB1dFByb3BzIiwiY29udHJvbCIsImlucHV0TW9kaWZpZXIiLCJmaWVsZCIsImZpZWxkU3RhdGUiLCJlcnJvciIsIm1lc3NhZ2UiLCJoYW5kbGVDaGFuZ2UiLCJlIiwidmFsdWUiLCJ0YXJnZXQiLCJtb2RpZmllZFZhbHVlIiwib25DaGFuZ2UiLCJJbnB1dENvbXBvbmVudCIsImNvbW1vblByb3BzIiwiaWQiLCJjbGFzc05hbWUiLCJvbkJsdXIiLCJ0eXBlIiwibWVtbyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBT0EsS0FBUCxJQUFnQkMsV0FBaEIsRUFBNkJDLE9BQTdCLFFBQTRDLE9BQTVDO0FBQ0EsU0FBU0MsS0FBVCxRQUFrQyxNQUFsQztBQUNBLFNBQWtCQyxhQUFsQixRQUF1QyxpQkFBdkM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBWUEsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksQ0FBQ0MsS0FBRCxFQUFtQjtBQUNuQyxNQUNFQyxJQURGLEdBTUlELEtBTkosQ0FDRUMsSUFERjtBQUFBLE1BRUVDLEtBRkYsR0FNSUYsS0FOSixDQUVFRSxLQUZGO0FBQUEsMEJBTUlGLEtBTkosQ0FHRUcsVUFIRjtBQUFBLE1BR0VBLFVBSEYsa0NBR2UsRUFIZjtBQUFBLE1BSUVDLE9BSkYsR0FNSUosS0FOSixDQUlFSSxPQUpGO0FBQUEsTUFLRUMsYUFMRixHQU1JTCxLQU5KLENBS0VLLGFBTEY7O0FBUUEsdUJBQThCUCxhQUFhLENBQUM7QUFBRUcsSUFBQUEsSUFBSSxFQUFKQSxJQUFGO0FBQVFHLElBQUFBLE9BQU8sRUFBUEE7QUFBUixHQUFELENBQTNDO0FBQUEsTUFBUUUsS0FBUixrQkFBUUEsS0FBUjtBQUFBLE1BQWVDLFVBQWYsa0JBQWVBLFVBQWY7O0FBRUEsTUFBTUMsS0FBSyxHQUFHWixPQUFPLENBQUMsWUFBTTtBQUFBOztBQUMxQixXQUFPVyxVQUFQLGFBQU9BLFVBQVAsNENBQU9BLFVBQVUsQ0FBRUMsS0FBbkIsc0RBQU8sa0JBQW1CQyxPQUExQjtBQUNELEdBRm9CLEVBRWxCLENBQUNGLFVBQUQsYUFBQ0EsVUFBRCx1QkFBQ0EsVUFBVSxDQUFFQyxLQUFiLENBRmtCLENBQXJCO0FBSUEsTUFBTUUsWUFBWSxHQUFHZixXQUFXLENBQUMsVUFBQ2dCLENBQUQsRUFBTztBQUN0QyxRQUFNQyxLQUFLLEdBQUdELENBQUMsQ0FBQ0UsTUFBRixDQUFTRCxLQUF2QjtBQUVBLFFBQU1FLGFBQWEsR0FBR1QsYUFBYSxHQUFHQSxhQUFhLENBQUNPLEtBQUQsQ0FBaEIsR0FBMEJBLEtBQTdEO0FBRUFOLElBQUFBLEtBQUssQ0FBQ1MsUUFBTixDQUFlRCxhQUFmO0FBQ0QsR0FOK0IsRUFNN0IsQ0FBQ1IsS0FBRCxFQUFRRCxhQUFSLENBTjZCLENBQWhDO0FBUUEsTUFBTVcsY0FBYyxHQUFHcEIsT0FBTyxDQUFDLFlBQU07QUFDbkMsUUFBTXFCLFdBQXVCLG1DQUN4QmQsVUFEd0I7QUFFM0JGLE1BQUFBLElBQUksRUFBSkEsSUFGMkI7QUFHM0JpQixNQUFBQSxFQUFFLEVBQUVqQixJQUh1QjtBQUkzQlcsTUFBQUEsS0FBSyxFQUFFTixLQUFLLENBQUNNLEtBSmM7QUFLM0JHLE1BQUFBLFFBQVEsRUFBRUwsWUFMaUI7QUFNM0JTLE1BQUFBLFNBQVMsRUFBRSxrQkFOZ0I7QUFPM0JDLE1BQUFBLE1BQU0sRUFBRWQsS0FBSyxDQUFDYztBQVBhLE1BQTdCOztBQVVBLFlBQVFqQixVQUFVLENBQUNrQixJQUFuQjtBQUNFLFdBQUssVUFBTDtBQUNFLDRCQUFPLG9CQUFDLEtBQUQsQ0FBTyxRQUFQLEVBQW9CSixXQUFwQixDQUFQO0FBRko7O0FBS0Esd0JBQU8sb0JBQUMsS0FBRCxFQUFXQSxXQUFYLENBQVA7QUFDRCxHQWpCNkIsRUFpQjNCLENBQUNYLEtBQUssQ0FBQ2MsTUFBUCxFQUFlZCxLQUFLLENBQUNNLEtBQXJCLEVBQTRCRixZQUE1QixFQUEwQ1AsVUFBMUMsRUFBc0RGLElBQXRELENBakIyQixDQUE5QjtBQW1CQSxzQkFDRTtBQUFLLElBQUEsU0FBUyxzQkFBZU8sS0FBSyxHQUFHLHNCQUFILEdBQTRCLEVBQWhEO0FBQWQsa0JBQ0U7QUFBSyxJQUFBLFNBQVMsRUFBQztBQUFmLEtBQW1DTixLQUFuQyxDQURGLEVBRUdjLGNBRkgsRUFHR1IsS0FBSyxnQkFDSjtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FBbUNBLEtBQW5DLENBREksR0FFRixJQUxOLENBREY7QUFTRCxDQW5ERDs7QUFxREEsNEJBQWVkLEtBQUssQ0FBQzRCLElBQU4sQ0FBV3ZCLFNBQVgsQ0FBZiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VDYWxsYmFjaywgdXNlTWVtbyB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IHsgSW5wdXQsIElucHV0UHJvcHMgfSBmcm9tICdhbnRkJ1xuaW1wb3J0IHsgQ29udHJvbCwgdXNlQ29udHJvbGxlciB9IGZyb20gJ3JlYWN0LWhvb2stZm9ybSdcbmltcG9ydCAnLi9UZXh0SW5wdXQuc2NzcydcblxuaW50ZXJmYWNlIElQcm9wcyB7XG4gIG5hbWU6IHN0cmluZ1xuICBsYWJlbDogc3RyaW5nXG4gIGlucHV0UHJvcHM/OiBJbnB1dFByb3BzXG4gIGNvbnRyb2w6IENvbnRyb2w8YW55PlxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW51c2VkLXZhcnNcbiAgaW5wdXRNb2RpZmllcj86ICh2YWx1ZT86IHN0cmluZykgPT4gc3RyaW5nXG59XG5cbmNvbnN0IFRleHRJbnB1dCA9IChwcm9wczogSVByb3BzKSA9PiB7XG4gIGNvbnN0IHtcbiAgICBuYW1lLFxuICAgIGxhYmVsLFxuICAgIGlucHV0UHJvcHMgPSB7fSxcbiAgICBjb250cm9sLFxuICAgIGlucHV0TW9kaWZpZXIsXG4gIH0gPSBwcm9wc1xuXG4gIGNvbnN0IHsgZmllbGQsIGZpZWxkU3RhdGUgfSA9IHVzZUNvbnRyb2xsZXIoeyBuYW1lLCBjb250cm9sIH0pXG5cbiAgY29uc3QgZXJyb3IgPSB1c2VNZW1vKCgpID0+IHtcbiAgICByZXR1cm4gZmllbGRTdGF0ZT8uZXJyb3I/Lm1lc3NhZ2VcbiAgfSwgW2ZpZWxkU3RhdGU/LmVycm9yXSlcblxuICBjb25zdCBoYW5kbGVDaGFuZ2UgPSB1c2VDYWxsYmFjaygoZSkgPT4ge1xuICAgIGNvbnN0IHZhbHVlID0gZS50YXJnZXQudmFsdWVcblxuICAgIGNvbnN0IG1vZGlmaWVkVmFsdWUgPSBpbnB1dE1vZGlmaWVyID8gaW5wdXRNb2RpZmllcih2YWx1ZSkgOiB2YWx1ZVxuXG4gICAgZmllbGQub25DaGFuZ2UobW9kaWZpZWRWYWx1ZSlcbiAgfSwgW2ZpZWxkLCBpbnB1dE1vZGlmaWVyXSlcblxuICBjb25zdCBJbnB1dENvbXBvbmVudCA9IHVzZU1lbW8oKCkgPT4ge1xuICAgIGNvbnN0IGNvbW1vblByb3BzOiBJbnB1dFByb3BzID0ge1xuICAgICAgLi4uaW5wdXRQcm9wcyxcbiAgICAgIG5hbWUsXG4gICAgICBpZDogbmFtZSxcbiAgICAgIHZhbHVlOiBmaWVsZC52YWx1ZSxcbiAgICAgIG9uQ2hhbmdlOiBoYW5kbGVDaGFuZ2UsXG4gICAgICBjbGFzc05hbWU6ICdUZXh0SW5wdXRfX2lucHV0JyxcbiAgICAgIG9uQmx1cjogZmllbGQub25CbHVyLFxuICAgIH1cblxuICAgIHN3aXRjaCAoaW5wdXRQcm9wcy50eXBlKSB7XG4gICAgICBjYXNlICdwYXNzd29yZCc6XG4gICAgICAgIHJldHVybiA8SW5wdXQuUGFzc3dvcmQgey4uLmNvbW1vblByb3BzfSAvPlxuICAgIH1cblxuICAgIHJldHVybiA8SW5wdXQgey4uLmNvbW1vblByb3BzfSAvPlxuICB9LCBbZmllbGQub25CbHVyLCBmaWVsZC52YWx1ZSwgaGFuZGxlQ2hhbmdlLCBpbnB1dFByb3BzLCBuYW1lXSlcblxuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPXtgVGV4dElucHV0ICR7ZXJyb3IgPyAnVGV4dElucHV0X19oYXMtZXJyb3InIDogJyd9YH0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nVGV4dElucHV0X19sYWJlbCc+e2xhYmVsfTwvZGl2PlxuICAgICAge0lucHV0Q29tcG9uZW50fVxuICAgICAge2Vycm9yID8gKFxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nVGV4dElucHV0X19lcnJvcic+e2Vycm9yfTwvZGl2PlxuICAgICAgKSA6IG51bGx9XG4gICAgPC9kaXY+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhUZXh0SW5wdXQpXG4iXX0=
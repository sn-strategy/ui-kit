import React, { ReactNode } from 'react';
import { Control } from 'react-hook-form';
import { SelectProps } from 'antd';
import { IListItem } from '../../../models/common';
import './Select.scss';
export interface ISelectOption<T = any> extends IListItem<T> {
    className?: string;
}
interface IProps {
    name: string;
    label: string;
    options: ISelectOption[];
    control: Control<any>;
    className?: string;
    selectProps?: SelectProps<any>;
    renderOption?: () => ReactNode;
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { memo, useCallback, useMemo } from 'react';
import { useController } from 'react-hook-form';
import { Select as AntSelect } from 'antd';
import cn from 'classnames';
import IconChevron from '../../Icons/IconChevron';

(function () {
  var styles = ".Select {\n  position: relative;\n  margin-bottom: 0.9375rem;\n}\n.Select__label {\n  position: relative;\n  width: max-content;\n  font-size: 0.75rem;\n  margin-bottom: -0.4375rem;\n  margin-inline-start: 0.5rem;\n  padding: 0 0.5rem;\n  border-radius: 0.5rem;\n  background-color: #ffffff;\n  z-index: 1;\n}\n.Select__error {\n  font-size: 12px;\n  color: #e1343e;\n}\n.Select__has-error .Select__label, .Select__has-error .ant-select .ant-select-selector {\n  color: #e1343e;\n}\n.Select__has-error .ant-select .ant-select-selector {\n  border-color: #e1343e;\n}";
  var fileName = "Select_Select";
  var element = document.querySelector("style[data-sass-component='Select_Select']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var Select = function Select(props) {
  var name = props.name,
      label = props.label,
      options = props.options,
      control = props.control,
      className = props.className,
      _props$selectProps = props.selectProps,
      selectProps = _props$selectProps === void 0 ? {} : _props$selectProps,
      _props$renderOption = props.renderOption,
      renderOption = _props$renderOption === void 0 ? function (props) {
    return props.label;
  } : _props$renderOption;

  var _useController = useController({
    name: name,
    control: control
  }),
      field = _useController.field,
      fieldState = _useController.fieldState;

  var error = useMemo(function () {
    var _fieldState$error;

    return fieldState === null || fieldState === void 0 ? void 0 : (_fieldState$error = fieldState.error) === null || _fieldState$error === void 0 ? void 0 : _fieldState$error.message;
  }, [fieldState === null || fieldState === void 0 ? void 0 : fieldState.error]);
  var handleChange = useCallback(function (value) {
    field.onChange(value);
  }, [field]);
  return /*#__PURE__*/React.createElement("div", {
    className: cn('Select', className, error && 'Select__has-error')
  }, /*#__PURE__*/React.createElement("div", {
    className: "Select__label"
  }, label), /*#__PURE__*/React.createElement(AntSelect, _extends({
    value: field.value,
    suffixIcon: /*#__PURE__*/React.createElement(IconChevron, {
      direction: "bottom"
    }),
    onChange: handleChange
  }, selectProps), options.map(function (o) {
    return /*#__PURE__*/React.createElement(AntSelect.Option, {
      key: o.value,
      value: o.value,
      label: o.label,
      disabled: o.disabled,
      className: o.className
    }, renderOption(o));
  })), error ? /*#__PURE__*/React.createElement("div", {
    className: "Select__error"
  }, error) : null);
};

export default /*#__PURE__*/memo(Select);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Gb3JtRmllbGRzL1NlbGVjdC9TZWxlY3QudHN4Il0sIm5hbWVzIjpbIlJlYWN0IiwibWVtbyIsInVzZUNhbGxiYWNrIiwidXNlTWVtbyIsInVzZUNvbnRyb2xsZXIiLCJTZWxlY3QiLCJBbnRTZWxlY3QiLCJjbiIsIkljb25DaGV2cm9uIiwicHJvcHMiLCJuYW1lIiwibGFiZWwiLCJvcHRpb25zIiwiY29udHJvbCIsImNsYXNzTmFtZSIsInNlbGVjdFByb3BzIiwicmVuZGVyT3B0aW9uIiwiZmllbGQiLCJmaWVsZFN0YXRlIiwiZXJyb3IiLCJtZXNzYWdlIiwiaGFuZGxlQ2hhbmdlIiwidmFsdWUiLCJvbkNoYW5nZSIsIm1hcCIsIm8iLCJkaXNhYmxlZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQSxPQUFPQSxLQUFQLElBQWdCQyxJQUFoQixFQUFpQ0MsV0FBakMsRUFBOENDLE9BQTlDLFFBQTZELE9BQTdEO0FBQ0EsU0FBa0JDLGFBQWxCLFFBQXVDLGlCQUF2QztBQUNBLFNBQVNDLE1BQU0sSUFBSUMsU0FBbkIsUUFBaUQsTUFBakQ7QUFDQSxPQUFPQyxFQUFQLE1BQWUsWUFBZjtBQUNBLE9BQU9DLFdBQVAsTUFBd0IseUJBQXhCOzs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxJQUFNSCxNQUF3QixHQUFHLFNBQTNCQSxNQUEyQixDQUFDSSxLQUFELEVBQW1CO0FBQ2xELE1BQ0VDLElBREYsR0FRSUQsS0FSSixDQUNFQyxJQURGO0FBQUEsTUFFRUMsS0FGRixHQVFJRixLQVJKLENBRUVFLEtBRkY7QUFBQSxNQUdFQyxPQUhGLEdBUUlILEtBUkosQ0FHRUcsT0FIRjtBQUFBLE1BSUVDLE9BSkYsR0FRSUosS0FSSixDQUlFSSxPQUpGO0FBQUEsTUFLRUMsU0FMRixHQVFJTCxLQVJKLENBS0VLLFNBTEY7QUFBQSwyQkFRSUwsS0FSSixDQU1FTSxXQU5GO0FBQUEsTUFNRUEsV0FORixtQ0FNZ0IsRUFOaEI7QUFBQSw0QkFRSU4sS0FSSixDQU9FTyxZQVBGO0FBQUEsTUFPRUEsWUFQRixvQ0FPaUIsVUFBQ1AsS0FBRDtBQUFBLFdBQTBCQSxLQUFLLENBQUNFLEtBQWhDO0FBQUEsR0FQakI7O0FBVUEsdUJBQThCUCxhQUFhLENBQUM7QUFBRU0sSUFBQUEsSUFBSSxFQUFKQSxJQUFGO0FBQVFHLElBQUFBLE9BQU8sRUFBUEE7QUFBUixHQUFELENBQTNDO0FBQUEsTUFBUUksS0FBUixrQkFBUUEsS0FBUjtBQUFBLE1BQWVDLFVBQWYsa0JBQWVBLFVBQWY7O0FBRUEsTUFBTUMsS0FBSyxHQUFHaEIsT0FBTyxDQUFDLFlBQU07QUFBQTs7QUFDMUIsV0FBT2UsVUFBUCxhQUFPQSxVQUFQLDRDQUFPQSxVQUFVLENBQUVDLEtBQW5CLHNEQUFPLGtCQUFtQkMsT0FBMUI7QUFDRCxHQUZvQixFQUVsQixDQUFDRixVQUFELGFBQUNBLFVBQUQsdUJBQUNBLFVBQVUsQ0FBRUMsS0FBYixDQUZrQixDQUFyQjtBQUlBLE1BQU1FLFlBQVksR0FBR25CLFdBQVcsQ0FBQyxVQUFDb0IsS0FBRCxFQUFXO0FBQzFDTCxJQUFBQSxLQUFLLENBQUNNLFFBQU4sQ0FBZUQsS0FBZjtBQUNELEdBRitCLEVBRTdCLENBQUNMLEtBQUQsQ0FGNkIsQ0FBaEM7QUFJQSxzQkFDRTtBQUFLLElBQUEsU0FBUyxFQUFFVixFQUFFLENBQUMsUUFBRCxFQUFXTyxTQUFYLEVBQXVCSyxLQUFLLElBQUksbUJBQWhDO0FBQWxCLGtCQUNFO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUFnQ1IsS0FBaEMsQ0FERixlQUVFLG9CQUFDLFNBQUQ7QUFDRSxJQUFBLEtBQUssRUFBRU0sS0FBSyxDQUFDSyxLQURmO0FBRUUsSUFBQSxVQUFVLGVBQUUsb0JBQUMsV0FBRDtBQUFhLE1BQUEsU0FBUyxFQUFDO0FBQXZCLE1BRmQ7QUFHRSxJQUFBLFFBQVEsRUFBRUQ7QUFIWixLQUlNTixXQUpOLEdBTUdILE9BQU8sQ0FBQ1ksR0FBUixDQUFZLFVBQUNDLENBQUQ7QUFBQSx3QkFDWCxvQkFBQyxTQUFELENBQVcsTUFBWDtBQUNFLE1BQUEsR0FBRyxFQUFFQSxDQUFDLENBQUNILEtBRFQ7QUFFRSxNQUFBLEtBQUssRUFBRUcsQ0FBQyxDQUFDSCxLQUZYO0FBR0UsTUFBQSxLQUFLLEVBQUVHLENBQUMsQ0FBQ2QsS0FIWDtBQUlFLE1BQUEsUUFBUSxFQUFFYyxDQUFDLENBQUNDLFFBSmQ7QUFLRSxNQUFBLFNBQVMsRUFBRUQsQ0FBQyxDQUFDWDtBQUxmLE9BT0dFLFlBQVksQ0FBQ1MsQ0FBRCxDQVBmLENBRFc7QUFBQSxHQUFaLENBTkgsQ0FGRixFQW9CR04sS0FBSyxnQkFDSjtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsS0FBZ0NBLEtBQWhDLENBREksR0FFRixJQXRCTixDQURGO0FBMEJELENBL0NEOztBQWlEQSw0QkFBZWxCLElBQUksQ0FBQ0ksTUFBRCxDQUFuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBtZW1vLCBSZWFjdE5vZGUsIHVzZUNhbGxiYWNrLCB1c2VNZW1vIH0gZnJvbSAncmVhY3QnXG5pbXBvcnQgeyBDb250cm9sLCB1c2VDb250cm9sbGVyIH0gZnJvbSAncmVhY3QtaG9vay1mb3JtJ1xuaW1wb3J0IHsgU2VsZWN0IGFzIEFudFNlbGVjdCwgU2VsZWN0UHJvcHMgfSBmcm9tICdhbnRkJ1xuaW1wb3J0IGNuIGZyb20gJ2NsYXNzbmFtZXMnXG5pbXBvcnQgSWNvbkNoZXZyb24gZnJvbSAnLi4vLi4vSWNvbnMvSWNvbkNoZXZyb24nXG5pbXBvcnQgeyBJTGlzdEl0ZW0gfSBmcm9tICcuLi8uLi8uLi9tb2RlbHMvY29tbW9uJ1xuXG5pbXBvcnQgJy4vU2VsZWN0LnNjc3MnXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVNlbGVjdE9wdGlvbjxUID0gYW55PiBleHRlbmRzIElMaXN0SXRlbTxUPiB7XG4gIGNsYXNzTmFtZT86IHN0cmluZ1xufVxuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgbmFtZTogc3RyaW5nXG4gIGxhYmVsOiBzdHJpbmdcbiAgb3B0aW9uczogSVNlbGVjdE9wdGlvbltdXG4gIGNvbnRyb2w6IENvbnRyb2w8YW55PlxuICBjbGFzc05hbWU/OiBzdHJpbmdcbiAgc2VsZWN0UHJvcHM/OiBTZWxlY3RQcm9wczxhbnk+XG4gIHJlbmRlck9wdGlvbj86ICgpID0+IFJlYWN0Tm9kZVxufVxuXG5jb25zdCBTZWxlY3Q6IFJlYWN0LkZDPElQcm9wcz4gPSAocHJvcHM6IElQcm9wcykgPT4ge1xuICBjb25zdCB7XG4gICAgbmFtZSxcbiAgICBsYWJlbCxcbiAgICBvcHRpb25zLFxuICAgIGNvbnRyb2wsXG4gICAgY2xhc3NOYW1lLFxuICAgIHNlbGVjdFByb3BzID0ge30sXG4gICAgcmVuZGVyT3B0aW9uID0gKHByb3BzOiBJU2VsZWN0T3B0aW9uKSA9PiBwcm9wcy5sYWJlbCxcbiAgfSA9IHByb3BzXG5cbiAgY29uc3QgeyBmaWVsZCwgZmllbGRTdGF0ZSB9ID0gdXNlQ29udHJvbGxlcih7IG5hbWUsIGNvbnRyb2wgfSlcblxuICBjb25zdCBlcnJvciA9IHVzZU1lbW8oKCkgPT4ge1xuICAgIHJldHVybiBmaWVsZFN0YXRlPy5lcnJvcj8ubWVzc2FnZVxuICB9LCBbZmllbGRTdGF0ZT8uZXJyb3JdKVxuXG4gIGNvbnN0IGhhbmRsZUNoYW5nZSA9IHVzZUNhbGxiYWNrKCh2YWx1ZSkgPT4ge1xuICAgIGZpZWxkLm9uQ2hhbmdlKHZhbHVlKVxuICB9LCBbZmllbGRdKVxuXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e2NuKCdTZWxlY3QnLCBjbGFzc05hbWUsICBlcnJvciAmJiAnU2VsZWN0X19oYXMtZXJyb3InKX0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nU2VsZWN0X19sYWJlbCc+e2xhYmVsfTwvZGl2PlxuICAgICAgPEFudFNlbGVjdFxuICAgICAgICB2YWx1ZT17ZmllbGQudmFsdWV9XG4gICAgICAgIHN1ZmZpeEljb249ezxJY29uQ2hldnJvbiBkaXJlY3Rpb249J2JvdHRvbScgLz59XG4gICAgICAgIG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9XG4gICAgICAgIHsuLi5zZWxlY3RQcm9wc31cbiAgICAgID5cbiAgICAgICAge29wdGlvbnMubWFwKChvKSA9PiAoXG4gICAgICAgICAgPEFudFNlbGVjdC5PcHRpb25cbiAgICAgICAgICAgIGtleT17by52YWx1ZX1cbiAgICAgICAgICAgIHZhbHVlPXtvLnZhbHVlfVxuICAgICAgICAgICAgbGFiZWw9e28ubGFiZWx9XG4gICAgICAgICAgICBkaXNhYmxlZD17by5kaXNhYmxlZH1cbiAgICAgICAgICAgIGNsYXNzTmFtZT17by5jbGFzc05hbWV9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge3JlbmRlck9wdGlvbihvKX1cbiAgICAgICAgICA8L0FudFNlbGVjdC5PcHRpb24+XG4gICAgICAgICkpfVxuICAgICAgPC9BbnRTZWxlY3Q+XG4gICAgICB7ZXJyb3IgPyAoXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdTZWxlY3RfX2Vycm9yJz57ZXJyb3J9PC9kaXY+XG4gICAgICApIDogbnVsbH1cbiAgICA8L2Rpdj5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBtZW1vKFNlbGVjdClcbiJdfQ==
import React, { memo, useCallback } from 'react';
import { useController } from 'react-hook-form';
import { Checkbox as AntCheckbox } from 'antd';

(function () {
  var styles = ".CheckboxLabel.ant-checkbox-wrapper {\n  display: inline-block;\n  margin: 0.25rem !important;\n}\n.CheckboxLabel .ant-checkbox {\n  display: none !important;\n}\n.CheckboxLabel .ant-checkbox + span {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  min-width: 2.75rem;\n  height: 2rem;\n  color: #412d3e;\n  font-size: 0.875rem;\n  border: 1px solid #412d3e;\n  border-radius: 0.5rem;\n}\n.CheckboxLabel .ant-checkbox-checked + span {\n  color: #ffffff;\n  background-color: #412d3e;\n}";
  var fileName = "Checkbox_Checkbox";
  var element = document.querySelector("style[data-sass-component='Checkbox_Checkbox']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var Checkbox = function Checkbox(props) {
  var name = props.name,
      label = props.label,
      value = props.value,
      mode = props.mode,
      disabled = props.disabled,
      control = props.control;

  var _useController = useController({
    name: name,
    control: control
  }),
      field = _useController.field;

  var handleChange = useCallback(function (value) {
    field.onChange(value.target.checked ? value.target.value : undefined);
  }, [field]);
  var className = mode === 'label' ? 'CheckboxLabel' : 'Checkbox';
  var isChecked = field.value === value;
  return /*#__PURE__*/React.createElement(AntCheckbox, {
    className: className,
    disabled: disabled,
    checked: isChecked,
    value: value,
    onChange: handleChange
  }, label);
};

export default /*#__PURE__*/memo(Checkbox);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Gb3JtRmllbGRzL0NoZWNrYm94L0NoZWNrYm94LnRzeCJdLCJuYW1lcyI6WyJSZWFjdCIsIm1lbW8iLCJ1c2VDYWxsYmFjayIsInVzZUNvbnRyb2xsZXIiLCJDaGVja2JveCIsIkFudENoZWNrYm94IiwicHJvcHMiLCJuYW1lIiwibGFiZWwiLCJ2YWx1ZSIsIm1vZGUiLCJkaXNhYmxlZCIsImNvbnRyb2wiLCJmaWVsZCIsImhhbmRsZUNoYW5nZSIsIm9uQ2hhbmdlIiwidGFyZ2V0IiwiY2hlY2tlZCIsInVuZGVmaW5lZCIsImNsYXNzTmFtZSIsImlzQ2hlY2tlZCJdLCJtYXBwaW5ncyI6IkFBQUEsT0FBT0EsS0FBUCxJQUFnQkMsSUFBaEIsRUFBaUNDLFdBQWpDLFFBQW9ELE9BQXBEO0FBQ0EsU0FBa0JDLGFBQWxCLFFBQXVDLGlCQUF2QztBQUNBLFNBQVNDLFFBQVEsSUFBSUMsV0FBckIsUUFBd0MsTUFBeEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBYUEsSUFBTUQsUUFBMEIsR0FBRyxTQUE3QkEsUUFBNkIsQ0FBQ0UsS0FBRCxFQUFtQjtBQUNwRCxNQUNFQyxJQURGLEdBT0lELEtBUEosQ0FDRUMsSUFERjtBQUFBLE1BRUVDLEtBRkYsR0FPSUYsS0FQSixDQUVFRSxLQUZGO0FBQUEsTUFHRUMsS0FIRixHQU9JSCxLQVBKLENBR0VHLEtBSEY7QUFBQSxNQUlFQyxJQUpGLEdBT0lKLEtBUEosQ0FJRUksSUFKRjtBQUFBLE1BS0VDLFFBTEYsR0FPSUwsS0FQSixDQUtFSyxRQUxGO0FBQUEsTUFNRUMsT0FORixHQU9JTixLQVBKLENBTUVNLE9BTkY7O0FBU0EsdUJBQWtCVCxhQUFhLENBQUM7QUFBRUksSUFBQUEsSUFBSSxFQUFKQSxJQUFGO0FBQVFLLElBQUFBLE9BQU8sRUFBUEE7QUFBUixHQUFELENBQS9CO0FBQUEsTUFBUUMsS0FBUixrQkFBUUEsS0FBUjs7QUFFQSxNQUFNQyxZQUFZLEdBQUdaLFdBQVcsQ0FBQyxVQUFDTyxLQUFELEVBQVc7QUFDMUNJLElBQUFBLEtBQUssQ0FBQ0UsUUFBTixDQUFlTixLQUFLLENBQUNPLE1BQU4sQ0FBYUMsT0FBYixHQUF1QlIsS0FBSyxDQUFDTyxNQUFOLENBQWFQLEtBQXBDLEdBQTRDUyxTQUEzRDtBQUNELEdBRitCLEVBRTdCLENBQUNMLEtBQUQsQ0FGNkIsQ0FBaEM7QUFJQSxNQUFNTSxTQUFTLEdBQUdULElBQUksS0FBSyxPQUFULEdBQW1CLGVBQW5CLEdBQXFDLFVBQXZEO0FBQ0EsTUFBTVUsU0FBUyxHQUFHUCxLQUFLLENBQUNKLEtBQU4sS0FBZ0JBLEtBQWxDO0FBRUEsc0JBQ0Usb0JBQUMsV0FBRDtBQUNFLElBQUEsU0FBUyxFQUFFVSxTQURiO0FBRUUsSUFBQSxRQUFRLEVBQUVSLFFBRlo7QUFHRSxJQUFBLE9BQU8sRUFBRVMsU0FIWDtBQUlFLElBQUEsS0FBSyxFQUFFWCxLQUpUO0FBS0UsSUFBQSxRQUFRLEVBQUVLO0FBTFosS0FPR04sS0FQSCxDQURGO0FBV0QsQ0E5QkQ7O0FBZ0NBLDRCQUFlUCxJQUFJLENBQUNHLFFBQUQsQ0FBbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgbWVtbywgUmVhY3ROb2RlLCB1c2VDYWxsYmFjayB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IHsgQ29udHJvbCwgdXNlQ29udHJvbGxlciB9IGZyb20gJ3JlYWN0LWhvb2stZm9ybSdcbmltcG9ydCB7IENoZWNrYm94IGFzIEFudENoZWNrYm94IH0gZnJvbSAnYW50ZCdcblxuaW1wb3J0ICcuL0NoZWNrYm94LnNjc3MnXG5cbmludGVyZmFjZSBJUHJvcHMge1xuICBuYW1lOiBzdHJpbmdcbiAgbGFiZWw6IHN0cmluZyB8IFJlYWN0Tm9kZVxuICB2YWx1ZTogYW55XG4gIG1vZGU/OiAnbGFiZWwnXG4gIGRpc2FibGVkPzogYm9vbGVhblxuICBjb250cm9sOiBDb250cm9sPGFueT5cbn1cblxuY29uc3QgQ2hlY2tib3g6IFJlYWN0LkZDPElQcm9wcz4gPSAocHJvcHM6IElQcm9wcykgPT4ge1xuICBjb25zdCB7XG4gICAgbmFtZSxcbiAgICBsYWJlbCxcbiAgICB2YWx1ZSxcbiAgICBtb2RlLFxuICAgIGRpc2FibGVkLFxuICAgIGNvbnRyb2wsXG4gIH0gPSBwcm9wc1xuXG4gIGNvbnN0IHsgZmllbGQgfSA9IHVzZUNvbnRyb2xsZXIoeyBuYW1lLCBjb250cm9sIH0pXG5cbiAgY29uc3QgaGFuZGxlQ2hhbmdlID0gdXNlQ2FsbGJhY2soKHZhbHVlKSA9PiB7XG4gICAgZmllbGQub25DaGFuZ2UodmFsdWUudGFyZ2V0LmNoZWNrZWQgPyB2YWx1ZS50YXJnZXQudmFsdWUgOiB1bmRlZmluZWQpXG4gIH0sIFtmaWVsZF0pXG5cbiAgY29uc3QgY2xhc3NOYW1lID0gbW9kZSA9PT0gJ2xhYmVsJyA/ICdDaGVja2JveExhYmVsJyA6ICdDaGVja2JveCdcbiAgY29uc3QgaXNDaGVja2VkID0gZmllbGQudmFsdWUgPT09IHZhbHVlXG5cbiAgcmV0dXJuIChcbiAgICA8QW50Q2hlY2tib3hcbiAgICAgIGNsYXNzTmFtZT17Y2xhc3NOYW1lfVxuICAgICAgZGlzYWJsZWQ9e2Rpc2FibGVkfVxuICAgICAgY2hlY2tlZD17aXNDaGVja2VkfVxuICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX1cbiAgICA+XG4gICAgICB7bGFiZWx9XG4gICAgPC9BbnRDaGVja2JveD5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBtZW1vKENoZWNrYm94KVxuIl19
import React, { ReactNode } from 'react';
import { Control } from 'react-hook-form';
import './Checkbox.scss';
interface IProps {
    name: string;
    label: string | ReactNode;
    value: any;
    mode?: 'label';
    disabled?: boolean;
    control: Control<any>;
}
declare const _default: React.NamedExoticComponent<IProps>;
export default _default;

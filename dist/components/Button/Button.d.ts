import React from 'react';
import { ButtonProps } from 'antd';
import './Button.scss';
export declare type IButtonProps = ButtonProps;
declare const _default: React.NamedExoticComponent<React.PropsWithChildren<Partial<{
    href: string;
    target?: string | undefined;
    onClick?: React.MouseEventHandler<HTMLElement> | undefined;
} & import("antd/lib/button/button").BaseButtonProps & Omit<React.AnchorHTMLAttributes<any>, "onClick" | "type"> & {
    htmlType?: "button" | "submit" | "reset" | undefined;
    onClick?: React.MouseEventHandler<HTMLElement> | undefined;
} & Omit<React.ButtonHTMLAttributes<any>, "onClick" | "type">>>>;
export default _default;

var _excluded = ["children", "type", "className"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { memo } from 'react';
import { Button as AntButton } from 'antd';
import cn from 'classnames';

(function () {
  var styles = ".Button.ant-btn {\n  height: auto;\n  padding: 0.5rem 1.5rem;\n  color: #ffffff;\n  border-radius: 2.8125rem;\n  transition-duration: 0.2s;\n}\n.Button.ant-btn[disabled] {\n  background: #e5e5e5;\n}\n.Button.ant-btn[disabled]:hover {\n  background: #e5e5e5;\n}\n.Button.ant-btn-default {\n  background: #ef3f3d;\n  border: none;\n}\n.Button.ant-btn-default:hover {\n  background: #d11010;\n  color: #ffffff;\n}";
  var fileName = "Button_Button";
  var element = document.querySelector("style[data-sass-component='Button_Button']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var Button = function Button(_ref) {
  var children = _ref.children,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? 'default' : _ref$type,
      className = _ref.className,
      rest = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement(AntButton, _extends({
    className: cn('Button', className),
    type: type
  }, rest), children);
};

export default /*#__PURE__*/memo(Button);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9CdXR0b24vQnV0dG9uLnRzeCJdLCJuYW1lcyI6WyJSZWFjdCIsIm1lbW8iLCJCdXR0b24iLCJBbnRCdXR0b24iLCJjbiIsImNoaWxkcmVuIiwidHlwZSIsImNsYXNzTmFtZSIsInJlc3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsT0FBT0EsS0FBUCxJQUFnQkMsSUFBaEIsUUFBK0MsT0FBL0M7QUFDQSxTQUFTQyxNQUFNLElBQUlDLFNBQW5CLFFBQWlELE1BQWpEO0FBQ0EsT0FBT0MsRUFBUCxNQUFlLFlBQWY7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBTUEsSUFBTUYsTUFBaUQsR0FBRyxTQUFwREEsTUFBb0QsT0FBd0Q7QUFBQSxNQUFyREcsUUFBcUQsUUFBckRBLFFBQXFEO0FBQUEsdUJBQTNDQyxJQUEyQztBQUFBLE1BQTNDQSxJQUEyQywwQkFBcEMsU0FBb0M7QUFBQSxNQUF6QkMsU0FBeUIsUUFBekJBLFNBQXlCO0FBQUEsTUFBWEMsSUFBVzs7QUFDaEgsc0JBQ0Usb0JBQUMsU0FBRDtBQUFXLElBQUEsU0FBUyxFQUFFSixFQUFFLENBQUMsUUFBRCxFQUFXRyxTQUFYLENBQXhCO0FBQStDLElBQUEsSUFBSSxFQUFFRDtBQUFyRCxLQUErREUsSUFBL0QsR0FDR0gsUUFESCxDQURGO0FBS0QsQ0FORDs7QUFRQSw0QkFBZUosSUFBSSxDQUFDQyxNQUFELENBQW5CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IG1lbW8sIFByb3BzV2l0aENoaWxkcmVuIH0gZnJvbSAncmVhY3QnXG5pbXBvcnQgeyBCdXR0b24gYXMgQW50QnV0dG9uLCBCdXR0b25Qcm9wcyB9IGZyb20gJ2FudGQnXG5pbXBvcnQgY24gZnJvbSAnY2xhc3NuYW1lcydcblxuaW1wb3J0ICcuL0J1dHRvbi5zY3NzJ1xuXG5leHBvcnQgdHlwZSBJQnV0dG9uUHJvcHMgPSBCdXR0b25Qcm9wc1xuXG5jb25zdCBCdXR0b246IFJlYWN0LkZDPFByb3BzV2l0aENoaWxkcmVuPElCdXR0b25Qcm9wcz4+ID0gKHsgY2hpbGRyZW4sIHR5cGUgPSAnZGVmYXVsdCcsIGNsYXNzTmFtZSwgLi4ucmVzdCB9KSA9PiB7XG4gIHJldHVybiAoXG4gICAgPEFudEJ1dHRvbiBjbGFzc05hbWU9e2NuKCdCdXR0b24nLCBjbGFzc05hbWUpfSB0eXBlPXt0eXBlfSB7Li4ucmVzdH0+XG4gICAgICB7Y2hpbGRyZW59XG4gICAgPC9BbnRCdXR0b24+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgbWVtbyhCdXR0b24pXG5cbiJdfQ==
import React from 'react';
import type { ILogoItem, IMenuItem } from '../../models/menu';
import './AppHeader.scss';
export interface IAppHeaderProps {
    logos?: ILogoItem[];
    items?: IMenuItem[];
    className?: string;
}
declare const _default: React.NamedExoticComponent<IAppHeaderProps>;
export default _default;

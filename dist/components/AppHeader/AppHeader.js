import React from 'react';
import { Link } from 'react-router-dom';
import { Divider, Layout, Menu } from 'antd';
import cn from 'classnames';
import renderMenuItem from '../../functions/menu/renderMenuItem';

(function () {
  var styles = ".AppHeader .ant-divider-vertical {\n  height: 1rem;\n}\n.AppHeader.ant-layout-header {\n  height: 3rem;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  line-height: initial;\n  padding: 0.5rem 1rem;\n  background-color: #412d3e;\n}\n.AppHeader.ant-layout-header .ant-menu {\n  display: flex;\n  align-items: center;\n  border: none;\n  background-color: transparent;\n}\n.AppHeader.ant-layout-header .ant-menu .ant-menu-submenu .ant-menu-title-content {\n  display: none;\n}\n.AppHeader.ant-layout-header .ant-menu .ant-menu-submenu,\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item {\n  width: 2rem;\n  height: 2rem;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: #ffffff;\n  padding: 0;\n  border-radius: 50%;\n}\n.AppHeader.ant-layout-header .ant-menu .ant-menu-submenu:not(:last-child),\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item:not(:last-child) {\n  margin-inline-end: 0.5rem;\n}\n.AppHeader.ant-layout-header .ant-menu .ant-menu-submenu:hover, .AppHeader.ant-layout-header .ant-menu .ant-menu-submenu-active, .AppHeader.ant-layout-header .ant-menu .ant-menu-submenu-selected,\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item:hover,\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item-active,\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item-selected {\n  color: #ffffff;\n  background-color: #795a77;\n}\n.AppHeader.ant-layout-header .ant-menu .ant-menu-submenu-icon,\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item-icon {\n  width: 1.5rem;\n  height: 1.5rem;\n  flex-shrink: 0;\n}\n.AppHeader.ant-layout-header .ant-menu .ant-menu-submenu-icon + span,\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item-icon + span {\n  margin: 0;\n}\n.AppHeader.ant-layout-header .ant-menu .ant-menu-submenu .ant-typography,\n.AppHeader.ant-layout-header .ant-menu .ant-menu-item .ant-typography {\n  display: inline-block;\n}\n.AppHeader__logo {\n  width: 4.5rem;\n}";
  var fileName = "AppHeader_AppHeader";
  var element = document.querySelector("style[data-sass-component='AppHeader_AppHeader']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var AppHeader = function AppHeader(props) {
  var _props$logos = props.logos,
      logos = _props$logos === void 0 ? [] : _props$logos,
      _props$items = props.items,
      items = _props$items === void 0 ? [] : _props$items,
      className = props.className;
  return /*#__PURE__*/React.createElement(Layout.Header, {
    className: cn('AppHeader', className)
  }, /*#__PURE__*/React.createElement("div", null, logos.reduce(function (acc, logo, id) {
    if (id > 0) {
      acc.push( /*#__PURE__*/React.createElement(Divider, {
        key: "".concat(logo.key, "-divider"),
        type: 'vertical'
      }));
    }

    acc.push( /*#__PURE__*/React.createElement(Link, {
      key: logo.key,
      to: logo.link
    }, /*#__PURE__*/React.createElement("img", {
      className: "AppHeader__logo",
      src: logo.src,
      alt: logo.key
    })));
    return acc;
  }, [])), /*#__PURE__*/React.createElement(Menu, {
    mode: "horizontal",
    disabledOverflow: true
  }, items.map(function (i) {
    return renderMenuItem(i);
  })));
};

export default /*#__PURE__*/React.memo(AppHeader);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9BcHBIZWFkZXIvQXBwSGVhZGVyLnRzeCJdLCJuYW1lcyI6WyJSZWFjdCIsIkxpbmsiLCJEaXZpZGVyIiwiTGF5b3V0IiwiTWVudSIsImNuIiwicmVuZGVyTWVudUl0ZW0iLCJBcHBIZWFkZXIiLCJwcm9wcyIsImxvZ29zIiwiaXRlbXMiLCJjbGFzc05hbWUiLCJyZWR1Y2UiLCJhY2MiLCJsb2dvIiwiaWQiLCJwdXNoIiwia2V5IiwibGluayIsInNyYyIsIm1hcCIsImkiLCJtZW1vIl0sIm1hcHBpbmdzIjoiQUFBQSxPQUFPQSxLQUFQLE1BQW9DLE9BQXBDO0FBQ0EsU0FBU0MsSUFBVCxRQUFxQixrQkFBckI7QUFDQSxTQUFTQyxPQUFULEVBQWtCQyxNQUFsQixFQUEwQkMsSUFBMUIsUUFBc0MsTUFBdEM7QUFDQSxPQUFPQyxFQUFQLE1BQWUsWUFBZjtBQUNBLE9BQU9DLGNBQVAsTUFBMkIscUNBQTNCOzs7Ozs7Ozs7Ozs7Ozs7OztBQVlBLElBQU1DLFNBQW9DLEdBQUcsU0FBdkNBLFNBQXVDLENBQUNDLEtBQUQsRUFBVztBQUN0RCxxQkFBOENBLEtBQTlDLENBQVFDLEtBQVI7QUFBQSxNQUFRQSxLQUFSLDZCQUFnQixFQUFoQjtBQUFBLHFCQUE4Q0QsS0FBOUMsQ0FBb0JFLEtBQXBCO0FBQUEsTUFBb0JBLEtBQXBCLDZCQUE0QixFQUE1QjtBQUFBLE1BQWdDQyxTQUFoQyxHQUE4Q0gsS0FBOUMsQ0FBZ0NHLFNBQWhDO0FBRUEsc0JBQ0Usb0JBQUMsTUFBRCxDQUFRLE1BQVI7QUFBZSxJQUFBLFNBQVMsRUFBRU4sRUFBRSxDQUFDLFdBQUQsRUFBY00sU0FBZDtBQUE1QixrQkFDRSxpQ0FDR0YsS0FBSyxDQUFDRyxNQUFOLENBQTZCLFVBQUNDLEdBQUQsRUFBTUMsSUFBTixFQUFZQyxFQUFaLEVBQW1CO0FBQy9DLFFBQUlBLEVBQUUsR0FBRyxDQUFULEVBQVk7QUFDVkYsTUFBQUEsR0FBRyxDQUFDRyxJQUFKLGVBQVMsb0JBQUMsT0FBRDtBQUFTLFFBQUEsR0FBRyxZQUFLRixJQUFJLENBQUNHLEdBQVYsYUFBWjtBQUFxQyxRQUFBLElBQUksRUFBRTtBQUEzQyxRQUFUO0FBQ0Q7O0FBQ0RKLElBQUFBLEdBQUcsQ0FBQ0csSUFBSixlQUNFLG9CQUFDLElBQUQ7QUFBTSxNQUFBLEdBQUcsRUFBRUYsSUFBSSxDQUFDRyxHQUFoQjtBQUFxQixNQUFBLEVBQUUsRUFBRUgsSUFBSSxDQUFDSTtBQUE5QixvQkFDRTtBQUFLLE1BQUEsU0FBUyxFQUFDLGlCQUFmO0FBQWlDLE1BQUEsR0FBRyxFQUFFSixJQUFJLENBQUNLLEdBQTNDO0FBQWdELE1BQUEsR0FBRyxFQUFFTCxJQUFJLENBQUNHO0FBQTFELE1BREYsQ0FERjtBQUtBLFdBQU9KLEdBQVA7QUFDRCxHQVZBLEVBVUUsRUFWRixDQURILENBREYsZUFjRSxvQkFBQyxJQUFEO0FBQU0sSUFBQSxJQUFJLEVBQUMsWUFBWDtBQUF3QixJQUFBLGdCQUFnQixFQUFFO0FBQTFDLEtBQ0dILEtBQUssQ0FBQ1UsR0FBTixDQUFVLFVBQUNDLENBQUQ7QUFBQSxXQUFPZixjQUFjLENBQUNlLENBQUQsQ0FBckI7QUFBQSxHQUFWLENBREgsQ0FkRixDQURGO0FBb0JELENBdkJEOztBQXlCQSw0QkFBZXJCLEtBQUssQ0FBQ3NCLElBQU4sQ0FBV2YsU0FBWCxDQUFmIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IFJlYWN0RWxlbWVudCB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IHsgTGluayB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nXG5pbXBvcnQgeyBEaXZpZGVyLCBMYXlvdXQsIE1lbnUgfSBmcm9tICdhbnRkJ1xuaW1wb3J0IGNuIGZyb20gJ2NsYXNzbmFtZXMnXG5pbXBvcnQgcmVuZGVyTWVudUl0ZW0gZnJvbSAnLi4vLi4vZnVuY3Rpb25zL21lbnUvcmVuZGVyTWVudUl0ZW0nXG5cbmltcG9ydCB0eXBlIHsgSUxvZ29JdGVtLCBJTWVudUl0ZW0gfSBmcm9tICcuLi8uLi9tb2RlbHMvbWVudSdcblxuaW1wb3J0ICcuL0FwcEhlYWRlci5zY3NzJ1xuXG5leHBvcnQgaW50ZXJmYWNlIElBcHBIZWFkZXJQcm9wcyB7XG4gICAgbG9nb3M/OiBJTG9nb0l0ZW1bXVxuICAgIGl0ZW1zPzogSU1lbnVJdGVtW11cbiAgICBjbGFzc05hbWU/OiBzdHJpbmdcbn1cblxuY29uc3QgQXBwSGVhZGVyOiBSZWFjdC5GQzxJQXBwSGVhZGVyUHJvcHM+ID0gKHByb3BzKSA9PiB7XG4gIGNvbnN0IHsgbG9nb3MgPSBbXSwgaXRlbXMgPSBbXSwgY2xhc3NOYW1lIH0gPSBwcm9wc1xuXG4gIHJldHVybiAoXG4gICAgPExheW91dC5IZWFkZXIgY2xhc3NOYW1lPXtjbignQXBwSGVhZGVyJywgY2xhc3NOYW1lKX0+XG4gICAgICA8ZGl2PlxuICAgICAgICB7bG9nb3MucmVkdWNlPFJlYWN0RWxlbWVudFtdPigoYWNjLCBsb2dvLCBpZCkgPT4ge1xuICAgICAgICAgIGlmIChpZCA+IDApIHtcbiAgICAgICAgICAgIGFjYy5wdXNoKDxEaXZpZGVyIGtleT17YCR7bG9nby5rZXl9LWRpdmlkZXJgfSB0eXBlPXsndmVydGljYWwnfS8+KVxuICAgICAgICAgIH1cbiAgICAgICAgICBhY2MucHVzaChcbiAgICAgICAgICAgIDxMaW5rIGtleT17bG9nby5rZXl9IHRvPXtsb2dvLmxpbmt9PlxuICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT0nQXBwSGVhZGVyX19sb2dvJyBzcmM9e2xvZ28uc3JjfSBhbHQ9e2xvZ28ua2V5fS8+XG4gICAgICAgICAgICA8L0xpbms+LFxuICAgICAgICAgIClcbiAgICAgICAgICByZXR1cm4gYWNjXG4gICAgICAgIH0sIFtdKX1cbiAgICAgIDwvZGl2PlxuICAgICAgPE1lbnUgbW9kZT0naG9yaXpvbnRhbCcgZGlzYWJsZWRPdmVyZmxvdz17dHJ1ZX0+XG4gICAgICAgIHtpdGVtcy5tYXAoKGkpID0+IHJlbmRlck1lbnVJdGVtKGkpKX1cbiAgICAgIDwvTWVudT5cbiAgICA8L0xheW91dC5IZWFkZXI+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhBcHBIZWFkZXIpXG4iXX0=
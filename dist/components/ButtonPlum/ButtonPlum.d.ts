import React from 'react';
import { ButtonProps } from 'antd';
import './ButtonPlum.scss';
export interface IButtonPlumProps extends ButtonProps {
    primary?: boolean;
}
declare const _default: React.NamedExoticComponent<React.PropsWithChildren<IButtonPlumProps>>;
export default _default;

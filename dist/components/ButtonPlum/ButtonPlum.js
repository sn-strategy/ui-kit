var _excluded = ["children", "primary", "type", "className"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { memo } from 'react';
import { Button as AntButton } from 'antd';
import cn from 'classnames';

(function () {
  var styles = ".ButtonPlum.ant-btn {\n  height: 2.5rem;\n  padding: 0.5rem 1.5rem;\n  color: #412d3e;\n  border-radius: 0.5rem;\n  transition-duration: 0.2s;\n}\n.ButtonPlum.ant-btn[disabled] {\n  background: #e5e5e5;\n}\n.ButtonPlum.ant-btn[disabled]:hover {\n  background: #e5e5e5;\n}\n.ButtonPlum.ant-btn-default {\n  background: #ffffff;\n  border: 1px solid #412d3e;\n}\n.ButtonPlum.ant-btn-default:hover {\n  border: 1px solid #795a77;\n  color: #795a77;\n}\n.ButtonPlum.primary.ant-btn {\n  color: #ffffff;\n}\n.ButtonPlum.primary.ant-btn-default {\n  background: #412d3e;\n  border: none;\n}\n.ButtonPlum.primary.ant-btn-default:hover {\n  background: #795a77;\n  color: #ffffff;\n}";
  var fileName = "ButtonPlum_ButtonPlum";
  var element = document.querySelector("style[data-sass-component='ButtonPlum_ButtonPlum']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var ButtonPlum = function ButtonPlum(_ref) {
  var children = _ref.children,
      primary = _ref.primary,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? 'default' : _ref$type,
      className = _ref.className,
      rest = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement(AntButton, _extends({
    className: cn('ButtonPlum', className, {
      primary: primary
    }),
    type: type
  }, rest), children);
};

export default /*#__PURE__*/memo(ButtonPlum);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9CdXR0b25QbHVtL0J1dHRvblBsdW0udHN4Il0sIm5hbWVzIjpbIlJlYWN0IiwibWVtbyIsIkJ1dHRvbiIsIkFudEJ1dHRvbiIsImNuIiwiQnV0dG9uUGx1bSIsImNoaWxkcmVuIiwicHJpbWFyeSIsInR5cGUiLCJjbGFzc05hbWUiLCJyZXN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU9BLEtBQVAsSUFBZ0JDLElBQWhCLFFBQStDLE9BQS9DO0FBQ0EsU0FBU0MsTUFBTSxJQUFJQyxTQUFuQixRQUFpRCxNQUFqRDtBQUNBLE9BQU9DLEVBQVAsTUFBZSxZQUFmOzs7Ozs7Ozs7Ozs7Ozs7OztBQVFBLElBQU1DLFVBQXlELEdBQUcsU0FBNURBLFVBQTRELE9BTTVEO0FBQUEsTUFMSkMsUUFLSSxRQUxKQSxRQUtJO0FBQUEsTUFKSkMsT0FJSSxRQUpKQSxPQUlJO0FBQUEsdUJBSEpDLElBR0k7QUFBQSxNQUhKQSxJQUdJLDBCQUhHLFNBR0g7QUFBQSxNQUZKQyxTQUVJLFFBRkpBLFNBRUk7QUFBQSxNQUREQyxJQUNDOztBQUNKLHNCQUNFLG9CQUFDLFNBQUQ7QUFBVyxJQUFBLFNBQVMsRUFBRU4sRUFBRSxDQUFDLFlBQUQsRUFBZUssU0FBZixFQUEwQjtBQUFFRixNQUFBQSxPQUFPLEVBQVBBO0FBQUYsS0FBMUIsQ0FBeEI7QUFBZ0UsSUFBQSxJQUFJLEVBQUVDO0FBQXRFLEtBQWdGRSxJQUFoRixHQUNHSixRQURILENBREY7QUFLRCxDQVpEOztBQWNBLDRCQUFlTCxJQUFJLENBQUNJLFVBQUQsQ0FBbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgbWVtbywgUHJvcHNXaXRoQ2hpbGRyZW4gfSBmcm9tICdyZWFjdCdcbmltcG9ydCB7IEJ1dHRvbiBhcyBBbnRCdXR0b24sIEJ1dHRvblByb3BzIH0gZnJvbSAnYW50ZCdcbmltcG9ydCBjbiBmcm9tICdjbGFzc25hbWVzJ1xuXG5pbXBvcnQgJy4vQnV0dG9uUGx1bS5zY3NzJ1xuXG5leHBvcnQgaW50ZXJmYWNlIElCdXR0b25QbHVtUHJvcHMgZXh0ZW5kcyBCdXR0b25Qcm9wcyB7XG4gIHByaW1hcnk/OiBib29sZWFuXG59XG5cbmNvbnN0IEJ1dHRvblBsdW06IFJlYWN0LkZDPFByb3BzV2l0aENoaWxkcmVuPElCdXR0b25QbHVtUHJvcHM+PiA9ICh7XG4gIGNoaWxkcmVuLFxuICBwcmltYXJ5LFxuICB0eXBlID0gJ2RlZmF1bHQnLFxuICBjbGFzc05hbWUsXG4gIC4uLnJlc3Rcbn0pID0+IHtcbiAgcmV0dXJuIChcbiAgICA8QW50QnV0dG9uIGNsYXNzTmFtZT17Y24oJ0J1dHRvblBsdW0nLCBjbGFzc05hbWUsIHsgcHJpbWFyeSB9KX0gdHlwZT17dHlwZX0gey4uLnJlc3R9PlxuICAgICAge2NoaWxkcmVufVxuICAgIDwvQW50QnV0dG9uPlxuICApXG59XG5cbmV4cG9ydCBkZWZhdWx0IG1lbW8oQnV0dG9uUGx1bSlcblxuIl19
import React, { ReactNode } from 'react';
import './ManagementContainer.scss';
interface IProps {
    list?: ReactNode;
    className?: string;
}
declare const _default: React.NamedExoticComponent<React.PropsWithChildren<IProps>>;
export default _default;

import React from 'react';
import cn from 'classnames';

(function () {
  var styles = ".ManagementContainer {\n  height: 100%;\n  display: grid;\n  grid-template-columns: 18.4375rem 1fr;\n  gap: 1.5rem;\n}\n.ManagementContainer__content {\n  height: 100%;\n}";
  var fileName = "ManagementContainer_ManagementContainer";
  var element = document.querySelector("style[data-sass-component='ManagementContainer_ManagementContainer']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var ManagementContainer = function ManagementContainer(props) {
  var children = props.children,
      list = props.list,
      className = props.className;
  return /*#__PURE__*/React.createElement("div", {
    className: cn('ManagementContainer', className)
  }, list ? /*#__PURE__*/React.createElement("section", {
    className: "ManagementContainer__list"
  }, list) : null, /*#__PURE__*/React.createElement("section", {
    className: "ManagementContainer__content"
  }, children));
};

export default /*#__PURE__*/React.memo(ManagementContainer);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9NYW5hZ2VtZW50L01hbmFnZW1lbnRDb250YWluZXIvTWFuYWdlbWVudENvbnRhaW5lci50c3giXSwibmFtZXMiOlsiUmVhY3QiLCJjbiIsIk1hbmFnZW1lbnRDb250YWluZXIiLCJwcm9wcyIsImNoaWxkcmVuIiwibGlzdCIsImNsYXNzTmFtZSIsIm1lbW8iXSwibWFwcGluZ3MiOiJBQUFBLE9BQU9BLEtBQVAsTUFBb0QsT0FBcEQ7QUFDQSxPQUFPQyxFQUFQLE1BQWUsWUFBZjs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFTQSxJQUFNQyxtQkFBd0QsR0FBRyxTQUEzREEsbUJBQTJELENBQUNDLEtBQUQsRUFBVztBQUMxRSxNQUFRQyxRQUFSLEdBQXNDRCxLQUF0QyxDQUFRQyxRQUFSO0FBQUEsTUFBa0JDLElBQWxCLEdBQXNDRixLQUF0QyxDQUFrQkUsSUFBbEI7QUFBQSxNQUF3QkMsU0FBeEIsR0FBc0NILEtBQXRDLENBQXdCRyxTQUF4QjtBQUVBLHNCQUNFO0FBQUssSUFBQSxTQUFTLEVBQUVMLEVBQUUsQ0FBQyxxQkFBRCxFQUF3QkssU0FBeEI7QUFBbEIsS0FDR0QsSUFBSSxnQkFDSDtBQUFTLElBQUEsU0FBUyxFQUFDO0FBQW5CLEtBQ0dBLElBREgsQ0FERyxHQUlELElBTE4sZUFNRTtBQUFTLElBQUEsU0FBUyxFQUFDO0FBQW5CLEtBQ0dELFFBREgsQ0FORixDQURGO0FBWUQsQ0FmRDs7QUFpQkEsNEJBQWVKLEtBQUssQ0FBQ08sSUFBTixDQUFXTCxtQkFBWCxDQUFmIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IFJlYWN0Tm9kZSwgUHJvcHNXaXRoQ2hpbGRyZW4gfSBmcm9tICdyZWFjdCdcbmltcG9ydCBjbiBmcm9tICdjbGFzc25hbWVzJ1xuXG5pbXBvcnQgJy4vTWFuYWdlbWVudENvbnRhaW5lci5zY3NzJ1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgbGlzdD86IFJlYWN0Tm9kZVxuICBjbGFzc05hbWU/OiBzdHJpbmdcbn1cblxuY29uc3QgTWFuYWdlbWVudENvbnRhaW5lcjogUmVhY3QuRkM8UHJvcHNXaXRoQ2hpbGRyZW48SVByb3BzPj4gPSAocHJvcHMpID0+IHtcbiAgY29uc3QgeyBjaGlsZHJlbiwgbGlzdCwgY2xhc3NOYW1lIH0gPSBwcm9wc1xuXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9e2NuKCdNYW5hZ2VtZW50Q29udGFpbmVyJywgY2xhc3NOYW1lKX0+XG4gICAgICB7bGlzdCA/IChcbiAgICAgICAgPHNlY3Rpb24gY2xhc3NOYW1lPSdNYW5hZ2VtZW50Q29udGFpbmVyX19saXN0Jz5cbiAgICAgICAgICB7bGlzdH1cbiAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgKSA6IG51bGx9XG4gICAgICA8c2VjdGlvbiBjbGFzc05hbWU9J01hbmFnZW1lbnRDb250YWluZXJfX2NvbnRlbnQnPlxuICAgICAgICB7Y2hpbGRyZW59XG4gICAgICA8L3NlY3Rpb24+XG4gICAgPC9kaXY+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhNYW5hZ2VtZW50Q29udGFpbmVyKVxuIl19
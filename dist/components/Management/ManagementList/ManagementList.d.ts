import { ReactNode } from 'react';
import './ManagementList.scss';
export interface IManagementListAction {
    key: string;
    image: string;
    title?: ReactNode;
    content: ReactNode;
}
export interface IManagementListProps<T> {
    items: T[];
    title: string;
    subtitle?: string;
    searchPlaceholder: string;
    createButtonLabel?: string;
    onCreate?: () => void;
    onSearch: (value: string) => void;
    onRenderItem: (item: T) => ReactNode;
    actions?: IManagementListAction[];
    className?: string;
}
declare const _default: <T>(props: IManagementListProps<T>) => JSX.Element;
export default _default;

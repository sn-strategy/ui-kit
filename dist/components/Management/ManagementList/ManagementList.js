/* eslint-disable no-unused-vars */
import React, { useCallback } from 'react';
import { Input, Popover } from 'antd';
import cn from 'classnames';
import ButtonPlum from '../../ButtonPlum/ButtonPlum';

(function () {
  var styles = ".ManagementList {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  border-radius: 0.5rem;\n  background-color: #ffffff;\n}\n.ManagementList__header {\n  height: 4.5rem;\n  border-bottom: 1px solid #f2f1f1;\n  display: grid;\n  grid-template-columns: max-content 1fr;\n  grid-template-areas: \"title buttons\" \"subtitle subtitle\";\n  padding: 1rem 1rem 0.625rem;\n  color: #3e3e40;\n}\n.ManagementList__action-container {\n  grid-area: buttons;\n  display: flex;\n  gap: 0.9375rem;\n  justify-self: end;\n  align-self: center;\n}\n.ManagementList__action {\n  width: 1.5rem;\n  height: 1.5rem;\n  padding: 0;\n  margin: 0;\n  border: none;\n  background-color: transparent;\n  cursor: pointer;\n  background-position: center;\n}\n.ManagementList__action:hover, .ManagementList__action:focus {\n  opacity: 0.7;\n}\n.ManagementList__title {\n  margin: 0;\n  grid-area: title;\n  font-style: normal;\n  font-weight: 700;\n  font-size: 1.125rem;\n  line-height: 1.25rem;\n  display: flex;\n  align-items: center;\n  text-align: right;\n  letter-spacing: 0.04em;\n}\n.ManagementList__subtitle {\n  grid-area: subtitle;\n  font-weight: 400;\n  font-size: 0.875rem;\n  line-height: 1.25rem;\n  display: flex;\n  align-items: center;\n  text-align: right;\n  letter-spacing: 0.04em;\n}\n.ManagementList__search {\n  display: flex;\n  align-items: center;\n  height: 4.5rem;\n  border-bottom: 1px solid #f2f1f1;\n  padding: 0 1rem;\n}\n.ManagementList__search .ant-input-search {\n  border: 1px solid #dfd5de;\n  border-radius: 0.5rem;\n}\n.ManagementList__search .ant-input-search:focus, .ManagementList__search .ant-input-search:hover, .ManagementList__search .ant-input-search:active, .ManagementList__search .ant-input-search-focused {\n  border-color: #412d3e;\n  outline: none;\n  box-shadow: none;\n}\n.ManagementList__search .ant-input-search-button {\n  border: none;\n  box-shadow: none;\n  background: none;\n}\n.ManagementList__search .ant-input-search .ant-input-group-addon {\n  background: none;\n}\n.ManagementList__search .ant-input-search .ant-input-affix-wrapper {\n  border: none;\n  background: none;\n}\n.ManagementList__content {\n  flex: 1;\n  overflow: auto;\n}\n.ManagementList__button {\n  margin: auto 1rem 1rem;\n}";
  var fileName = "ManagementList_ManagementList";
  var element = document.querySelector("style[data-sass-component='ManagementList_ManagementList']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var ManagementList = function ManagementList(props) {
  var items = props.items,
      title = props.title,
      subtitle = props.subtitle,
      searchPlaceholder = props.searchPlaceholder,
      createButtonLabel = props.createButtonLabel,
      onCreate = props.onCreate,
      onSearch = props.onSearch,
      onRenderItem = props.onRenderItem,
      actions = props.actions,
      className = props.className;
  var handleSearch = useCallback(function (e) {
    if (typeof e === 'string') {
      onSearch(e);
      return;
    }

    onSearch(e.target.value);
  }, [onSearch]);
  return /*#__PURE__*/React.createElement("div", {
    className: cn('ManagementList', className)
  }, /*#__PURE__*/React.createElement("div", {
    className: "ManagementList__header"
  }, actions && /*#__PURE__*/React.createElement("div", {
    className: "ManagementList__action-container"
  }, actions.map(function (action) {
    return /*#__PURE__*/React.createElement(Popover, {
      key: action.key,
      placement: "bottomRight",
      title: action.title,
      content: action.content,
      trigger: "click"
    }, /*#__PURE__*/React.createElement("button", {
      className: "ManagementList__action",
      style: {
        backgroundImage: "url(\"".concat(action.image, "\")")
      }
    }));
  })), /*#__PURE__*/React.createElement("h3", {
    className: "ManagementList__title"
  }, title), subtitle && /*#__PURE__*/React.createElement("strong", {
    className: "ManagementList__subtitle"
  }, subtitle)), /*#__PURE__*/React.createElement("div", {
    className: "ManagementList__search"
  }, /*#__PURE__*/React.createElement(Input.Search, {
    placeholder: searchPlaceholder,
    allowClear: true,
    onChange: handleSearch,
    onSearch: handleSearch
  })), /*#__PURE__*/React.createElement("div", {
    className: "ManagementList__content"
  }, items.map(function (u) {
    return onRenderItem(u);
  })), onCreate ? /*#__PURE__*/React.createElement(ButtonPlum, {
    className: "ManagementList__button",
    primary: true,
    onClick: onCreate
  }, createButtonLabel) : null);
};

export default /*#__PURE__*/React.memo(ManagementList);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9NYW5hZ2VtZW50L01hbmFnZW1lbnRMaXN0L01hbmFnZW1lbnRMaXN0LnRzeCJdLCJuYW1lcyI6WyJSZWFjdCIsInVzZUNhbGxiYWNrIiwiSW5wdXQiLCJQb3BvdmVyIiwiY24iLCJCdXR0b25QbHVtIiwiTWFuYWdlbWVudExpc3QiLCJwcm9wcyIsIml0ZW1zIiwidGl0bGUiLCJzdWJ0aXRsZSIsInNlYXJjaFBsYWNlaG9sZGVyIiwiY3JlYXRlQnV0dG9uTGFiZWwiLCJvbkNyZWF0ZSIsIm9uU2VhcmNoIiwib25SZW5kZXJJdGVtIiwiYWN0aW9ucyIsImNsYXNzTmFtZSIsImhhbmRsZVNlYXJjaCIsImUiLCJ0YXJnZXQiLCJ2YWx1ZSIsIm1hcCIsImFjdGlvbiIsImtleSIsImNvbnRlbnQiLCJiYWNrZ3JvdW5kSW1hZ2UiLCJpbWFnZSIsInUiLCJtZW1vIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBLE9BQU9BLEtBQVAsSUFBMkJDLFdBQTNCLFFBQTJELE9BQTNEO0FBQ0EsU0FBU0MsS0FBVCxFQUFnQkMsT0FBaEIsUUFBK0IsTUFBL0I7QUFDQSxPQUFPQyxFQUFQLE1BQWUsWUFBZjtBQUNBLE9BQU9DLFVBQVAsTUFBdUIsNkJBQXZCOzs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQSxJQUFNQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQU1DLEtBQU4sRUFBc0Q7QUFDM0UsTUFDRUMsS0FERixHQVdJRCxLQVhKLENBQ0VDLEtBREY7QUFBQSxNQUVFQyxLQUZGLEdBV0lGLEtBWEosQ0FFRUUsS0FGRjtBQUFBLE1BR0VDLFFBSEYsR0FXSUgsS0FYSixDQUdFRyxRQUhGO0FBQUEsTUFJRUMsaUJBSkYsR0FXSUosS0FYSixDQUlFSSxpQkFKRjtBQUFBLE1BS0VDLGlCQUxGLEdBV0lMLEtBWEosQ0FLRUssaUJBTEY7QUFBQSxNQU1FQyxRQU5GLEdBV0lOLEtBWEosQ0FNRU0sUUFORjtBQUFBLE1BT0VDLFFBUEYsR0FXSVAsS0FYSixDQU9FTyxRQVBGO0FBQUEsTUFRRUMsWUFSRixHQVdJUixLQVhKLENBUUVRLFlBUkY7QUFBQSxNQVNFQyxPQVRGLEdBV0lULEtBWEosQ0FTRVMsT0FURjtBQUFBLE1BVUVDLFNBVkYsR0FXSVYsS0FYSixDQVVFVSxTQVZGO0FBYUEsTUFBTUMsWUFBWSxHQUFHakIsV0FBVyxDQUFDLFVBQUNrQixDQUFELEVBQStDO0FBQzlFLFFBQUksT0FBT0EsQ0FBUCxLQUFhLFFBQWpCLEVBQTJCO0FBQ3pCTCxNQUFBQSxRQUFRLENBQUNLLENBQUQsQ0FBUjtBQUNBO0FBQ0Q7O0FBQ0RMLElBQUFBLFFBQVEsQ0FBQ0ssQ0FBQyxDQUFDQyxNQUFGLENBQVNDLEtBQVYsQ0FBUjtBQUNELEdBTitCLEVBTTdCLENBQUNQLFFBQUQsQ0FONkIsQ0FBaEM7QUFRQSxzQkFDRTtBQUFLLElBQUEsU0FBUyxFQUFFVixFQUFFLENBQUMsZ0JBQUQsRUFBbUJhLFNBQW5CO0FBQWxCLGtCQUNFO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNHRCxPQUFPLGlCQUFJO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNUQSxPQUFPLENBQUNNLEdBQVIsQ0FBWSxVQUFDQyxNQUFEO0FBQUEsd0JBQ1gsb0JBQUMsT0FBRDtBQUNFLE1BQUEsR0FBRyxFQUFFQSxNQUFNLENBQUNDLEdBRGQ7QUFFRSxNQUFBLFNBQVMsRUFBQyxhQUZaO0FBR0UsTUFBQSxLQUFLLEVBQUVELE1BQU0sQ0FBQ2QsS0FIaEI7QUFJRSxNQUFBLE9BQU8sRUFBRWMsTUFBTSxDQUFDRSxPQUpsQjtBQUtFLE1BQUEsT0FBTyxFQUFDO0FBTFYsb0JBT0U7QUFBUSxNQUFBLFNBQVMsRUFBQyx3QkFBbEI7QUFDRSxNQUFBLEtBQUssRUFBRTtBQUFFQyxRQUFBQSxlQUFlLGtCQUFVSCxNQUFNLENBQUNJLEtBQWpCO0FBQWpCO0FBRFQsTUFQRixDQURXO0FBQUEsR0FBWixDQURTLENBRGQsZUFnQkU7QUFBSSxJQUFBLFNBQVMsRUFBQztBQUFkLEtBQXVDbEIsS0FBdkMsQ0FoQkYsRUFpQkdDLFFBQVEsaUJBQUk7QUFBUSxJQUFBLFNBQVMsRUFBQztBQUFsQixLQUE4Q0EsUUFBOUMsQ0FqQmYsQ0FERixlQW9CRTtBQUFLLElBQUEsU0FBUyxFQUFDO0FBQWYsa0JBQ0Usb0JBQUMsS0FBRCxDQUFPLE1BQVA7QUFBYyxJQUFBLFdBQVcsRUFBRUMsaUJBQTNCO0FBQThDLElBQUEsVUFBVSxNQUF4RDtBQUF5RCxJQUFBLFFBQVEsRUFBRU8sWUFBbkU7QUFDRSxJQUFBLFFBQVEsRUFBRUE7QUFEWixJQURGLENBcEJGLGVBd0JFO0FBQUssSUFBQSxTQUFTLEVBQUM7QUFBZixLQUNHVixLQUFLLENBQUNjLEdBQU4sQ0FBVSxVQUFDTSxDQUFEO0FBQUEsV0FBT2IsWUFBWSxDQUFDYSxDQUFELENBQW5CO0FBQUEsR0FBVixDQURILENBeEJGLEVBMkJHZixRQUFRLGdCQUNQLG9CQUFDLFVBQUQ7QUFBWSxJQUFBLFNBQVMsRUFBQyx3QkFBdEI7QUFBK0MsSUFBQSxPQUFPLE1BQXREO0FBQ0UsSUFBQSxPQUFPLEVBQUVBO0FBRFgsS0FDc0JELGlCQUR0QixDQURPLEdBR0wsSUE5Qk4sQ0FERjtBQWtDRCxDQXhERDs7QUEwREEsNEJBQWVaLEtBQUssQ0FBQzZCLElBQU4sQ0FBV3ZCLGNBQVgsQ0FBZiIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIG5vLXVudXNlZC12YXJzICovXG5pbXBvcnQgUmVhY3QsIHsgUmVhY3ROb2RlLCB1c2VDYWxsYmFjaywgQ2hhbmdlRXZlbnQgfSBmcm9tICdyZWFjdCdcbmltcG9ydCB7IElucHV0LCBQb3BvdmVyIH0gZnJvbSAnYW50ZCdcbmltcG9ydCBjbiBmcm9tICdjbGFzc25hbWVzJ1xuaW1wb3J0IEJ1dHRvblBsdW0gZnJvbSAnLi4vLi4vQnV0dG9uUGx1bS9CdXR0b25QbHVtJ1xuaW1wb3J0ICcuL01hbmFnZW1lbnRMaXN0LnNjc3MnXG5cblxuZXhwb3J0IGludGVyZmFjZSBJTWFuYWdlbWVudExpc3RBY3Rpb24ge1xuICAgIGtleTogc3RyaW5nXG4gICAgaW1hZ2U6IHN0cmluZ1xuICAgIHRpdGxlPzogUmVhY3ROb2RlXG4gICAgY29udGVudDogUmVhY3ROb2RlXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSU1hbmFnZW1lbnRMaXN0UHJvcHM8VD4ge1xuICAgIGl0ZW1zOiBUW11cbiAgICB0aXRsZTogc3RyaW5nXG4gICAgc3VidGl0bGU/OiBzdHJpbmdcbiAgICBzZWFyY2hQbGFjZWhvbGRlcjogc3RyaW5nXG4gICAgY3JlYXRlQnV0dG9uTGFiZWw/OiBzdHJpbmdcbiAgICBvbkNyZWF0ZT86ICgpID0+IHZvaWRcbiAgICBvblNlYXJjaDogKHZhbHVlOiBzdHJpbmcpID0+IHZvaWRcbiAgICBvblJlbmRlckl0ZW06IChpdGVtOiBUKSA9PiBSZWFjdE5vZGVcbiAgICBhY3Rpb25zPzogSU1hbmFnZW1lbnRMaXN0QWN0aW9uW11cbiAgICBjbGFzc05hbWU/OiBzdHJpbmdcbn1cblxuY29uc3QgTWFuYWdlbWVudExpc3QgPSA8VCwgPihwcm9wczogSU1hbmFnZW1lbnRMaXN0UHJvcHM8VD4pOiBKU1guRWxlbWVudCA9PiB7XG4gIGNvbnN0IHtcbiAgICBpdGVtcyxcbiAgICB0aXRsZSxcbiAgICBzdWJ0aXRsZSxcbiAgICBzZWFyY2hQbGFjZWhvbGRlcixcbiAgICBjcmVhdGVCdXR0b25MYWJlbCxcbiAgICBvbkNyZWF0ZSxcbiAgICBvblNlYXJjaCxcbiAgICBvblJlbmRlckl0ZW0sXG4gICAgYWN0aW9ucyxcbiAgICBjbGFzc05hbWUsXG4gIH0gPSBwcm9wc1xuXG4gIGNvbnN0IGhhbmRsZVNlYXJjaCA9IHVzZUNhbGxiYWNrKChlOiBzdHJpbmcgfCBDaGFuZ2VFdmVudDxIVE1MSW5wdXRFbGVtZW50PikgPT4ge1xuICAgIGlmICh0eXBlb2YgZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgIG9uU2VhcmNoKGUpXG4gICAgICByZXR1cm5cbiAgICB9XG4gICAgb25TZWFyY2goZS50YXJnZXQudmFsdWUpXG4gIH0sIFtvblNlYXJjaF0pXG5cbiAgcmV0dXJuIChcbiAgICA8ZGl2IGNsYXNzTmFtZT17Y24oJ01hbmFnZW1lbnRMaXN0JywgY2xhc3NOYW1lKX0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nTWFuYWdlbWVudExpc3RfX2hlYWRlcic+XG4gICAgICAgIHthY3Rpb25zICYmIDxkaXYgY2xhc3NOYW1lPSdNYW5hZ2VtZW50TGlzdF9fYWN0aW9uLWNvbnRhaW5lcic+XG4gICAgICAgICAge2FjdGlvbnMubWFwKChhY3Rpb24pID0+IChcbiAgICAgICAgICAgIDxQb3BvdmVyXG4gICAgICAgICAgICAgIGtleT17YWN0aW9uLmtleX1cbiAgICAgICAgICAgICAgcGxhY2VtZW50PSdib3R0b21SaWdodCdcbiAgICAgICAgICAgICAgdGl0bGU9e2FjdGlvbi50aXRsZX1cbiAgICAgICAgICAgICAgY29udGVudD17YWN0aW9uLmNvbnRlbnR9XG4gICAgICAgICAgICAgIHRyaWdnZXI9J2NsaWNrJ1xuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT0nTWFuYWdlbWVudExpc3RfX2FjdGlvbidcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBiYWNrZ3JvdW5kSW1hZ2U6IGB1cmwoXCIke2FjdGlvbi5pbWFnZX1cIilgIH19Lz5cbiAgICAgICAgICAgIDwvUG9wb3Zlcj5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9kaXY+fVxuXG4gICAgICAgIDxoMyBjbGFzc05hbWU9J01hbmFnZW1lbnRMaXN0X190aXRsZSc+e3RpdGxlfTwvaDM+XG4gICAgICAgIHtzdWJ0aXRsZSAmJiA8c3Ryb25nIGNsYXNzTmFtZT0nTWFuYWdlbWVudExpc3RfX3N1YnRpdGxlJz57c3VidGl0bGV9PC9zdHJvbmc+fVxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nTWFuYWdlbWVudExpc3RfX3NlYXJjaCc+XG4gICAgICAgIDxJbnB1dC5TZWFyY2ggcGxhY2Vob2xkZXI9e3NlYXJjaFBsYWNlaG9sZGVyfSBhbGxvd0NsZWFyIG9uQ2hhbmdlPXtoYW5kbGVTZWFyY2h9XG4gICAgICAgICAgb25TZWFyY2g9e2hhbmRsZVNlYXJjaH0vPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nTWFuYWdlbWVudExpc3RfX2NvbnRlbnQnPlxuICAgICAgICB7aXRlbXMubWFwKCh1KSA9PiBvblJlbmRlckl0ZW0odSkpfVxuICAgICAgPC9kaXY+XG4gICAgICB7b25DcmVhdGUgPyAoXG4gICAgICAgIDxCdXR0b25QbHVtIGNsYXNzTmFtZT0nTWFuYWdlbWVudExpc3RfX2J1dHRvbicgcHJpbWFyeVxuICAgICAgICAgIG9uQ2xpY2s9e29uQ3JlYXRlfT57Y3JlYXRlQnV0dG9uTGFiZWx9PC9CdXR0b25QbHVtPlxuICAgICAgKSA6IG51bGx9XG4gICAgPC9kaXY+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhNYW5hZ2VtZW50TGlzdCkgYXMgdHlwZW9mIE1hbmFnZW1lbnRMaXN0XG4iXX0=
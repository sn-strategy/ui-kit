import React from 'react';
import { Alert, Skeleton } from 'antd';

(function () {
  var styles = "";
  var fileName = "ContentWrapper_ContentWrapper";
  var element = document.querySelector("style[data-sass-component='ContentWrapper_ContentWrapper']");

  if (!element) {
    var styleBlock = document.createElement("style");
    styleBlock.innerHTML = styles;
    styleBlock.setAttribute("data-sass-component", fileName);
    document.head.appendChild(styleBlock);
  } else {
    element.innerHTML = styles;
  }
})();

var ContentWrapper = function ContentWrapper(props) {
  var isLoading = props.isLoading,
      error = props.error,
      children = props.children;

  if (isLoading) {
    return /*#__PURE__*/React.createElement(Skeleton, {
      active: true
    });
  }

  if (error) {
    return /*#__PURE__*/React.createElement(Alert, {
      message: "Error",
      description: error,
      type: "error",
      showIcon: true
    });
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, children);
};

export default /*#__PURE__*/React.memo(ContentWrapper);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY29tcG9uZW50cy9Db250ZW50V3JhcHBlci9Db250ZW50V3JhcHBlci50c3giXSwibmFtZXMiOlsiUmVhY3QiLCJBbGVydCIsIlNrZWxldG9uIiwiQ29udGVudFdyYXBwZXIiLCJwcm9wcyIsImlzTG9hZGluZyIsImVycm9yIiwiY2hpbGRyZW4iLCJtZW1vIl0sIm1hcHBpbmdzIjoiQUFBQSxPQUFPQSxLQUFQLE1BQXlDLE9BQXpDO0FBQ0EsU0FBU0MsS0FBVCxFQUFnQkMsUUFBaEIsUUFBZ0MsTUFBaEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBU0EsSUFBTUMsY0FBbUQsR0FBRyxTQUF0REEsY0FBc0QsQ0FBQ0MsS0FBRCxFQUFXO0FBQ3JFLE1BQVFDLFNBQVIsR0FBdUNELEtBQXZDLENBQVFDLFNBQVI7QUFBQSxNQUFtQkMsS0FBbkIsR0FBdUNGLEtBQXZDLENBQW1CRSxLQUFuQjtBQUFBLE1BQTBCQyxRQUExQixHQUF1Q0gsS0FBdkMsQ0FBMEJHLFFBQTFCOztBQUVBLE1BQUlGLFNBQUosRUFBZTtBQUNiLHdCQUFPLG9CQUFDLFFBQUQ7QUFBVSxNQUFBLE1BQU07QUFBaEIsTUFBUDtBQUNEOztBQUVELE1BQUlDLEtBQUosRUFBVztBQUNULHdCQUNFLG9CQUFDLEtBQUQ7QUFDRSxNQUFBLE9BQU8sRUFBQyxPQURWO0FBRUUsTUFBQSxXQUFXLEVBQUVBLEtBRmY7QUFHRSxNQUFBLElBQUksRUFBQyxPQUhQO0FBSUUsTUFBQSxRQUFRO0FBSlYsTUFERjtBQVFEOztBQUVELHNCQUNFLDBDQUFHQyxRQUFILENBREY7QUFHRCxDQXJCRDs7QUF1QkEsNEJBQWVQLEtBQUssQ0FBQ1EsSUFBTixDQUFXTCxjQUFYLENBQWYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgUHJvcHNXaXRoQ2hpbGRyZW4gfSBmcm9tICdyZWFjdCdcbmltcG9ydCB7IEFsZXJ0LCBTa2VsZXRvbiB9IGZyb20gJ2FudGQnXG5cbmltcG9ydCAnLi9Db250ZW50V3JhcHBlci5zY3NzJ1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgaXNMb2FkaW5nPzogYm9vbGVhblxuICBlcnJvcj86IHN0cmluZ1xufVxuXG5jb25zdCBDb250ZW50V3JhcHBlcjogUmVhY3QuRkM8UHJvcHNXaXRoQ2hpbGRyZW48SVByb3BzPj4gPSAocHJvcHMpID0+IHtcbiAgY29uc3QgeyBpc0xvYWRpbmcsIGVycm9yLCBjaGlsZHJlbiB9ID0gcHJvcHNcblxuICBpZiAoaXNMb2FkaW5nKSB7XG4gICAgcmV0dXJuIDxTa2VsZXRvbiBhY3RpdmUgLz5cbiAgfVxuXG4gIGlmIChlcnJvcikge1xuICAgIHJldHVybiAoXG4gICAgICA8QWxlcnRcbiAgICAgICAgbWVzc2FnZT0nRXJyb3InXG4gICAgICAgIGRlc2NyaXB0aW9uPXtlcnJvcn1cbiAgICAgICAgdHlwZT0nZXJyb3InXG4gICAgICAgIHNob3dJY29uXG4gICAgICAvPlxuICAgIClcbiAgfVxuXG4gIHJldHVybiAoXG4gICAgPD57Y2hpbGRyZW59PC8+XG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhDb250ZW50V3JhcHBlcilcbiJdfQ==
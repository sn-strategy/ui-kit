import React from 'react';
import './ContentWrapper.scss';
interface IProps {
    isLoading?: boolean;
    error?: string;
}
declare const _default: React.NamedExoticComponent<React.PropsWithChildren<IProps>>;
export default _default;

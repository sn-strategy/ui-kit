export interface IIterableItem {
    key: string;
}
export interface IListItem<T = any> {
    label: string;
    value: T;
    disabled?: boolean;
}

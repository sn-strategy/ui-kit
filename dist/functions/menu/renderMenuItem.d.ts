/// <reference types="react" />
import type { IMenuItem } from '../../models/menu';
declare type TRenderMenuItem = (i: IMenuItem, renderIcon?: (src: IMenuItem['icon']) => IMenuItem['icon']) => JSX.Element;
declare const renderMenuItem: TRenderMenuItem;
export default renderMenuItem;

import React from 'react';
import {addDecorator} from '@storybook/react';
import RouterWrapper from './helpers/RouterWrapper';
import BaseStyleProvider from "./helpers/BaseStyleProvider";

addDecorator(storyFn => <RouterWrapper><BaseStyleProvider>{storyFn()}</BaseStyleProvider></RouterWrapper>);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

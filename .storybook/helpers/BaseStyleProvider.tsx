import React, {FC} from 'react'
import '../../src/App.scss'

const BaseStyleProvider: FC = ({children}) => {
    return (
        <div>{children}</div>
    )
}

export default BaseStyleProvider

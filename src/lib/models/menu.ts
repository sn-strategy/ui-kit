import type { LinkProps } from 'react-router-dom'
import type { MenuItemProps } from 'antd'

import type { IIterableItem } from './common'

export interface ILogoItem extends IIterableItem {
  src: string
  link: LinkProps['to']
}

export interface IMenuItem extends IIterableItem {
  icon?: string | MenuItemProps['icon']
  iconClassName?: string
  link?: LinkProps['to']
  label?: string
  badge?: number
  groupTitle?: string
  hasTopDivider?: boolean
  hasBottomDivider?: boolean
  children?: IMenuItem[]
  onClick?: MenuItemProps['onClick']
}

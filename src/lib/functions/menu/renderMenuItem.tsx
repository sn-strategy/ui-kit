import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import { Menu, Badge, Typography } from 'antd'

import type { IMenuItem } from '../../models/menu'

// eslint-disable-next-line no-unused-vars
type TRenderMenuItem = (i: IMenuItem, renderIcon?: (src: IMenuItem['icon']) => IMenuItem['icon']) => JSX.Element

const renderMenuItem: TRenderMenuItem = (i, renderIcon) => {
  const isGroup = !!i.groupTitle
  const isSubmenu = !!i.children

  const iconElement = renderIcon ? (
    renderIcon(i.icon)
  ) : typeof i.icon === 'string' ? (
    <img className={i.iconClassName} src={i.icon} alt={i.key} />
  ) : i.icon

  const content = isGroup ? (
    <Menu.ItemGroup key={i.key} title={i.groupTitle}>
      {i.children?.map((c) => renderMenuItem(c, renderIcon))}
    </Menu.ItemGroup>
  ) : isSubmenu ? (
    <Menu.SubMenu key={i.key} icon={iconElement} title={i.label}>
      {i.children?.map((c) => renderMenuItem(c, renderIcon))}
    </Menu.SubMenu>
  ) : (
    <Menu.Item key={i.key} icon={iconElement} title={i.label} onClick={i.onClick}>
      <Badge status='error' size='small' count={i.badge}>
        <Typography.Text>
          {i.link ? (
            <Link to={i.link}>
              {i.label}
            </Link>
          ) : (
            i.label
          )}
        </Typography.Text>
      </Badge>
    </Menu.Item>
  )

  return (
    <Fragment key={i.key}>
      {i.hasTopDivider ? <Menu.Divider /> : null}
      {content}
      {i.hasBottomDivider ? <Menu.Divider /> : null}
    </Fragment>
  )
}

export default renderMenuItem

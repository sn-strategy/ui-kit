import React, { useState, useCallback, useRef } from 'react'
import { Layout } from 'antd'
import cn from 'classnames'

import AppSidebarLogo from './components/AppSidebarLogo/AppSidebarLogo'
import AppSidebarMenu from './components/AppSidebarMenu/AppSidebarMenu'
import AppSidebarToggle from './components/AppSidebarToggle/AppSidebarToggle'

import type { IMenuItem } from '../../models/menu'

import './AppSidebar.scss'

interface IProps {
  logo: string
  menuItems: IMenuItem[]
  className?: string
}

const AppSidebar: React.FC<IProps> = (props) => {
  const { logo, menuItems, className } = props
  const [hovered, setHovered] = useState<boolean>(false)
  const [collapsed, setCollapsed] = useState<boolean>(true)
  const hoverTimeout = useRef<NodeJS.Timeout | null>(null)

  const handleMouseEnter = useCallback((): void => {
    if (hoverTimeout.current) {
      clearTimeout(hoverTimeout.current)
    }
    if (!hovered) {
      hoverTimeout.current = setTimeout(() => setHovered(true), 200)
    }
  }, [hovered])

  const handleMouseLeave = useCallback(() => {
    if (hoverTimeout.current) {
      clearTimeout(hoverTimeout.current)
    }
    if (hovered) {
      hoverTimeout.current = setTimeout(() => setHovered(false), 200)
    }
  }, [hovered])

  const toggleCollapsed = useCallback(() => {
    setCollapsed(!collapsed)
  }, [collapsed])

  const SiderClassName = cn('AppSidebar', className, {
    hovered: hovered && collapsed,
  })

  return (
    <Layout.Sider
      className={SiderClassName}
      collapsible
      width={269}
      collapsedWidth={76}
      collapsed={!hovered && collapsed}
      trigger={<AppSidebarToggle collapsed={collapsed} />}
      onCollapse={toggleCollapsed}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <AppSidebarLogo logo={logo} />
      <AppSidebarMenu items={menuItems} />
    </Layout.Sider>
  )
}

export default React.memo(AppSidebar)

import React, { useState, useCallback } from 'react'
import { Menu } from 'antd'
import { default as Icon } from './AppSidebarMenuIcon'
import renderMenuItem from '../../../../functions/menu/renderMenuItem'

import type { IMenuItem } from '../../../../models/menu'

import './AppSidebarMenu.scss'

interface IProps {
  items: IMenuItem[]
}

const renderMenuIcon = (src: IMenuItem['icon']): IMenuItem['icon'] => {
  if (typeof src === 'string') {
    return <Icon src={src} />
  }
  return src
}

const AppSidebarMenu: React.FC<IProps> = (props) => {
  const { items } = props
  const [openSubmenu, setOpenSubmenu] = useState<string[]>([])

  const handleOpenChange = useCallback((keys: string[]) => {
    const latestOpenKey = keys.find((key) => !openSubmenu.includes(key))
    setOpenSubmenu(latestOpenKey ? [latestOpenKey] : [])
  }, [openSubmenu])

  return (
    <div className='AppSidebarMenu'>
      <Menu mode='vertical' inlineIndent={18} openKeys={openSubmenu} onOpenChange={handleOpenChange}>
        {items.map((i) => renderMenuItem(i, renderMenuIcon))}
      </Menu>
    </div>
  )
}

export default React.memo(AppSidebarMenu)

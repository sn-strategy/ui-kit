import React from 'react'

interface IProps {
  src: string
}

const AppSidebarMenuIcon: React.FC<IProps> = (props) => {
  const { src } = props
 
  return (
    <div className='AppSidebarMenu__icon'>
      <img className='AppSidebarMenu__icon--image' src={src} alt='icon' />
    </div>
  )
}

export default React.memo(AppSidebarMenuIcon)

import React from 'react'

import './AppSidebarLogo.scss'

interface IProps {
  logo: string
}

const AppSidebarLogo: React.FC<IProps> = (props) => {
  const { logo } = props

  return (
    <div className='AppSidebarLogo'>
      <div className='AppSidebarLogo__container'>
        <img className='AppSidebarLogo__image' src={logo} alt='logo' />
      </div>
    </div>
  )
}

export default React.memo(AppSidebarLogo)

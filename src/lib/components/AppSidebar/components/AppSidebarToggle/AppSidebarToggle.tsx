import React, { FC, memo } from 'react'
import cn from 'classnames'

import './AppSidebarToggle.scss'

interface IProps {
  collapsed?: boolean
}

const AppSidebarToggle: FC<IProps> = (props) => {
  const { collapsed } = props

  const iconClassName = cn('ant-menu-submenu-arrow', {
    collapsed,
  })

  return (
    <div className='AppSidebarToggle'>
      <i className={iconClassName} />
    </div>
  )
}

export default memo(AppSidebarToggle)

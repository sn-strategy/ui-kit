import React, { ReactNode, PropsWithChildren } from 'react'
import cn from 'classnames'

import './ManagementContainer.scss'

interface IProps {
  list?: ReactNode
  className?: string
}

const ManagementContainer: React.FC<PropsWithChildren<IProps>> = (props) => {
  const { children, list, className } = props

  return (
    <div className={cn('ManagementContainer', className)}>
      {list ? (
        <section className='ManagementContainer__list'>
          {list}
        </section>
      ) : null}
      <section className='ManagementContainer__content'>
        {children}
      </section>
    </div>
  )
}

export default React.memo(ManagementContainer)

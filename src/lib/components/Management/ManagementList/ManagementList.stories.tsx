import React from 'react'
import ManagementList from './ManagementList'
import { ComponentStory } from '@storybook/react'

export default {
  title: 'Example/ManagementList',
  component: ManagementList,
}

const Template: ComponentStory<typeof ManagementList> = (args) => (<ManagementList {...args}/>)

export const Default = Template.bind({})
Default.args = {
  items: [],
  title: 'title',
  subtitle: 'subtitle',
  searchPlaceholder: 'Search',
}


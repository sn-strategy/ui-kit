/* eslint-disable no-unused-vars */
import React, { ReactNode, useCallback, ChangeEvent } from 'react'
import { Input, Popover } from 'antd'
import cn from 'classnames'
import ButtonPlum from '../../ButtonPlum/ButtonPlum'
import './ManagementList.scss'


export interface IManagementListAction {
    key: string
    image: string
    title?: ReactNode
    content: ReactNode
}

export interface IManagementListProps<T> {
    items: T[]
    title: string
    subtitle?: string
    searchPlaceholder: string
    createButtonLabel?: string
    onCreate?: () => void
    onSearch: (value: string) => void
    onRenderItem: (item: T) => ReactNode
    actions?: IManagementListAction[]
    className?: string
}

const ManagementList = <T, >(props: IManagementListProps<T>): JSX.Element => {
  const {
    items,
    title,
    subtitle,
    searchPlaceholder,
    createButtonLabel,
    onCreate,
    onSearch,
    onRenderItem,
    actions,
    className,
  } = props

  const handleSearch = useCallback((e: string | ChangeEvent<HTMLInputElement>) => {
    if (typeof e === 'string') {
      onSearch(e)
      return
    }
    onSearch(e.target.value)
  }, [onSearch])

  return (
    <div className={cn('ManagementList', className)}>
      <div className='ManagementList__header'>
        {actions && <div className='ManagementList__action-container'>
          {actions.map((action) => (
            <Popover
              key={action.key}
              placement='bottomRight'
              title={action.title}
              content={action.content}
              trigger='click'
            >
              <button className='ManagementList__action'
                style={{ backgroundImage: `url("${action.image}")` }}/>
            </Popover>
          ))}
        </div>}

        <h3 className='ManagementList__title'>{title}</h3>
        {subtitle && <strong className='ManagementList__subtitle'>{subtitle}</strong>}
      </div>
      <div className='ManagementList__search'>
        <Input.Search placeholder={searchPlaceholder} allowClear onChange={handleSearch}
          onSearch={handleSearch}/>
      </div>
      <div className='ManagementList__content'>
        {items.map((u) => onRenderItem(u))}
      </div>
      {onCreate ? (
        <ButtonPlum className='ManagementList__button' primary
          onClick={onCreate}>{createButtonLabel}</ButtonPlum>
      ) : null}
    </div>
  )
}

export default React.memo(ManagementList) as typeof ManagementList

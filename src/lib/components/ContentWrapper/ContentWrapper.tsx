import React, { PropsWithChildren } from 'react'
import { Alert, Skeleton } from 'antd'

import './ContentWrapper.scss'

interface IProps {
  isLoading?: boolean
  error?: string
}

const ContentWrapper: React.FC<PropsWithChildren<IProps>> = (props) => {
  const { isLoading, error, children } = props

  if (isLoading) {
    return <Skeleton active />
  }

  if (error) {
    return (
      <Alert
        message='Error'
        description={error}
        type='error'
        showIcon
      />
    )
  }

  return (
    <>{children}</>
  )
}

export default React.memo(ContentWrapper)

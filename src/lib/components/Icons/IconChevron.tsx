import React, { FC } from 'react'
import cn from 'classnames'

import './Icons.scss'

interface IProps {
  color?: string
  direction?: 'top' | 'left' | 'right' | 'bottom'
}

const IconChevron: FC<IProps> = (props) => {
  const { color = 'currentColor', direction = 'left' } = props

  const className = cn('Icon', {
    [`direction-${direction}`]: direction,
  })

  return (
    <svg className={className} width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M16 9.50009C15.744 9.50009 15.488 9.59809 15.293 9.79309L11.988 13.0981L8.69502 9.91809C8.29702 9.53409 7.66502 9.54409 7.28102 9.94309C6.89702 10.3401 6.90802 10.9741 7.30502 11.3571L11.305 15.2191C11.698 15.5981 12.322 15.5931 12.707 15.2071L16.707 11.2071C17.098 10.8161 17.098 10.1841 16.707 9.79309C16.512 9.59809 16.256 9.50009 16 9.50009' fill={color}/>
      <mask id='mask0_922_40709' style={{ maskType: 'alpha' }} maskUnits='userSpaceOnUse' x='7' y='9' width='11' height='7'>
        <path fillRule='evenodd' clipRule='evenodd' d='M16 9.50009C15.744 9.50009 15.488 9.59809 15.293 9.79309L11.988 13.0981L8.69502 9.91809C8.29702 9.53409 7.66502 9.54409 7.28102 9.94309C6.89702 10.3401 6.90802 10.9741 7.30502 11.3571L11.305 15.2191C11.698 15.5981 12.322 15.5931 12.707 15.2071L16.707 11.2071C17.098 10.8161 17.098 10.1841 16.707 9.79309C16.512 9.59809 16.256 9.50009 16 9.50009' fill='white'/>
      </mask>
      <g mask='url(#mask0_922_40709)'>
      </g>
    </svg>

  )
}

export default React.memo(IconChevron)

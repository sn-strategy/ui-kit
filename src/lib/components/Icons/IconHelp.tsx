import React, { FC } from 'react'

interface IProps {
  color?: string
}

const IconHelp: FC<IProps> = (props) => {
  const { color = 'black' } = props

  return (
    <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M13.0124 2.55507L13.0134 2.55606L21.4394 10.935C22.005 11.5416 22.0049 12.4588 21.439 13.0652L13.0595 21.4447C12.5011 22.0031 11.5458 22.0031 10.9873 21.4447L10.9863 21.4437L2.55507 13.0595C2.55493 13.0594 2.55478 13.0592 2.55463 13.0591C1.99662 12.5006 1.99676 11.5456 2.55507 10.9873L2.55606 10.9863L10.9402 2.55507C10.9403 2.55493 10.9405 2.55478 10.9406 2.55463C11.4992 1.99662 12.4541 1.99676 13.0124 2.55507Z' stroke={color}/>
      <path d='M10 10C10 9.33333 10.4 8 12 8C13.6 8 14 9.33333 14 10C14 10.3333 13.8 11.1 13 11.5C12 12 12 12.5 12 13C12 13.4 12 13.3333 12 13.5M12 16V15.5' stroke={color} strokeLinecap='round'/>
    </svg>
  )
}

export default React.memo(IconHelp)

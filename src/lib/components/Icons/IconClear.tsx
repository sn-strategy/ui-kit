import React, { FC } from 'react'

interface IProps {
  color?: string
}

const IconClear: FC<IProps> = (props) => {
  const { color = 'black' } = props

  return (
    <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M16 14H21M16.3846 17H21M17.5385 20H21' stroke={color} strokeLinecap='round'/>
      <path d='M5.00014 17C4.65782 18.7886 5.59157 22 7.00001 22' stroke={color} strokeLinecap='round'/>
      <path d='M8.65686 17.2114C8.50006 18.5 9.59155 22 11 22' stroke={color} strokeLinecap='round'/>
      <path d='M17.1733 2L10.6944 9.88732M10.6944 9.88732C6.93858 10.169 0.0465552 12.9859 2.52543 22C5.0043 22 12.1968 22 15.4832 22C9.84936 17.7746 13.5113 16.0845 10.6944 9.88732Z' stroke={color} strokeLinecap='round'/>
      <path d='M6.1875 11.2957C7.03257 12.4224 8.44102 14.3942 11.8213 13.8309' stroke={color} strokeLinecap='round'/>
    </svg>
  )
}

export default React.memo(IconClear)

import React, { FC } from 'react'

interface IProps {
  color?: string
}

const IconFilter: FC<IProps> = (props) => {
  const { color = 'currentColor' } = props

  return (
    <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M21.8601 2.41406H2.14049C1.48424 2.41406 1.07442 3.12924 1.40389 3.69978L7.63425 14.5587V20.7355C7.63425 21.2096 8.0146 21.5926 8.48603 21.5926H15.5146C15.986 21.5926 16.3664 21.2096 16.3664 20.7355V14.5587L22.5994 3.69978C22.9262 3.12924 22.5164 2.41406 21.8601 2.41406ZM14.4485 19.6641H9.5521V15.6998H14.4512V19.6641H14.4485ZM14.7057 13.5944L14.4807 13.9855H9.51728L9.29228 13.5944L7.24317 10.0212H16.7575L14.7057 13.5944ZM17.7405 8.30692H6.26014L3.98335 4.34263H20.0173L17.7405 8.30692Z' fill={color} fillOpacity='0.85'/>
    </svg>
  )
}

export default React.memo(IconFilter)

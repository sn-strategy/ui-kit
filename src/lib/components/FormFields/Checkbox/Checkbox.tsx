import React, { memo, ReactNode, useCallback } from 'react'
import { Control, useController } from 'react-hook-form'
import { Checkbox as AntCheckbox } from 'antd'

import './Checkbox.scss'

interface IProps {
  name: string
  label: string | ReactNode
  value: any
  mode?: 'label'
  disabled?: boolean
  control: Control<any>
}

const Checkbox: React.FC<IProps> = (props: IProps) => {
  const {
    name,
    label,
    value,
    mode,
    disabled,
    control,
  } = props

  const { field } = useController({ name, control })

  const handleChange = useCallback((value) => {
    field.onChange(value.target.checked ? value.target.value : undefined)
  }, [field])

  const className = mode === 'label' ? 'CheckboxLabel' : 'Checkbox'
  const isChecked = field.value === value

  return (
    <AntCheckbox
      className={className}
      disabled={disabled}
      checked={isChecked}
      value={value}
      onChange={handleChange}
    >
      {label}
    </AntCheckbox>
  )
}

export default memo(Checkbox)

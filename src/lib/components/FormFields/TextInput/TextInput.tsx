import React, { useCallback, useMemo } from 'react'
import { Input, InputProps } from 'antd'
import { Control, useController } from 'react-hook-form'
import './TextInput.scss'

interface IProps {
  name: string
  label: string
  inputProps?: InputProps
  control: Control<any>
  // eslint-disable-next-line no-unused-vars
  inputModifier?: (value?: string) => string
}

const TextInput = (props: IProps) => {
  const {
    name,
    label,
    inputProps = {},
    control,
    inputModifier,
  } = props

  const { field, fieldState } = useController({ name, control })

  const error = useMemo(() => {
    return fieldState?.error?.message
  }, [fieldState?.error])

  const handleChange = useCallback((e) => {
    const value = e.target.value

    const modifiedValue = inputModifier ? inputModifier(value) : value

    field.onChange(modifiedValue)
  }, [field, inputModifier])

  const InputComponent = useMemo(() => {
    const commonProps: InputProps = {
      ...inputProps,
      name,
      id: name,
      value: field.value,
      onChange: handleChange,
      className: 'TextInput__input',
      onBlur: field.onBlur,
    }

    switch (inputProps.type) {
      case 'password':
        return <Input.Password {...commonProps} />
    }

    return <Input {...commonProps} />
  }, [field.onBlur, field.value, handleChange, inputProps, name])

  return (
    <div className={`TextInput ${error ? 'TextInput__has-error' : ''}`}>
      <div className='TextInput__label'>{label}</div>
      {InputComponent}
      {error ? (
        <div className='TextInput__error'>{error}</div>
      ) : null}
    </div>
  )
}

export default React.memo(TextInput)

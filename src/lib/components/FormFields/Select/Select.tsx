import React, { memo, ReactNode, useCallback, useMemo } from 'react'
import { Control, useController } from 'react-hook-form'
import { Select as AntSelect, SelectProps } from 'antd'
import cn from 'classnames'
import IconChevron from '../../Icons/IconChevron'
import { IListItem } from '../../../models/common'

import './Select.scss'

export interface ISelectOption<T = any> extends IListItem<T> {
  className?: string
}

interface IProps {
  name: string
  label: string
  options: ISelectOption[]
  control: Control<any>
  className?: string
  selectProps?: SelectProps<any>
  renderOption?: () => ReactNode
}

const Select: React.FC<IProps> = (props: IProps) => {
  const {
    name,
    label,
    options,
    control,
    className,
    selectProps = {},
    renderOption = (props: ISelectOption) => props.label,
  } = props

  const { field, fieldState } = useController({ name, control })

  const error = useMemo(() => {
    return fieldState?.error?.message
  }, [fieldState?.error])

  const handleChange = useCallback((value) => {
    field.onChange(value)
  }, [field])

  return (
    <div className={cn('Select', className,  error && 'Select__has-error')}>
      <div className='Select__label'>{label}</div>
      <AntSelect
        value={field.value}
        suffixIcon={<IconChevron direction='bottom' />}
        onChange={handleChange}
        {...selectProps}
      >
        {options.map((o) => (
          <AntSelect.Option
            key={o.value}
            value={o.value}
            label={o.label}
            disabled={o.disabled}
            className={o.className}
          >
            {renderOption(o)}
          </AntSelect.Option>
        ))}
      </AntSelect>
      {error ? (
        <div className='Select__error'>{error}</div>
      ) : null}
    </div>
  )
}

export default memo(Select)

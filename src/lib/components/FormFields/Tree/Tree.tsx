/* eslint-disable no-unused-vars */
import React, { FC, memo, useCallback } from 'react'
import { Control, useController } from 'react-hook-form'
import { Radio as AntRadio, Checkbox as AntCheckbox, Tree as AntTree, TreeProps } from 'antd'
import { DataNode } from 'antd/lib/tree'
import { Key } from 'antd/lib/table/interface'

import IconChevron from '../../Icons/IconChevron'

import './Tree.scss'

export enum ETreeNodeType {
  RADIO = 'radio',
  CHECKBOX = 'checkbox',
}

interface ICheckboxTreeNode extends DataNode {
  type?: ETreeNodeType.CHECKBOX
}

interface IRadioTreeNode extends DataNode {
  type: ETreeNodeType.RADIO
  groupName: string
}

export type TTreeNode = ICheckboxTreeNode | IRadioTreeNode

export interface ICheckedKeys {
  checked: Key[]
  halfChecked: Key[]
}

type TCheckedValue = ICheckedKeys | Key[]

interface IProps extends TreeProps {
  name: string
  control: Control
}

const getCheckedKeys = (data: TCheckedValue): Key[] => {
  return Array.isArray(data) ? data : data.checked
}

const getRadioNodes = (data: TTreeNode[]): IRadioTreeNode[] => {
  return data.reduce<IRadioTreeNode[]>((memo, item) => {
    if (item.type === ETreeNodeType.RADIO) {
      memo.push(item)

      if (item.children) {
        memo = [...memo, ...getRadioNodes(item.children)]
      }
    }
    return memo
  }, [])
}

const getTreeNodeKeys = (data: TTreeNode[]): Key[] => {
  return data.reduce<Key[]>((memo, item) => {
    memo.push(item.key)
    if (item.children) {
      memo = [...memo, ...getTreeNodeKeys(item.children)]
    }
    return memo
  }, [])
}

const getNewCheckedKeys = (
  treeData: TTreeNode[],
  oldCheckedKeys: Key[],
  oldHalfCheckedKeys: Key[],
  newCheckedKeys: Key[],
  newHalfCheckedKeys: Key[],
): [Key[], Key[]] => {
  const oldKeys = [...oldCheckedKeys, ...oldHalfCheckedKeys]
  const newKeys = [...newCheckedKeys, ...newHalfCheckedKeys]
  const radioNodes = getRadioNodes(treeData)
  const deltaCheckedKeys = newKeys.filter((k) => !oldKeys.includes(k))

  const oldRadioNodes = radioNodes.filter((n) => oldKeys.includes(n.key))
  const deltaRadioNodes = radioNodes.filter((n) => deltaCheckedKeys.includes(n.key))

  const removedRadioNodes = oldRadioNodes.filter((o) => {
    return deltaRadioNodes.some((n) => n.groupName === o.groupName && o.key !== n.key)
  })

  const removedKeys = getTreeNodeKeys(removedRadioNodes)
  return [
    newCheckedKeys.filter((k) => !removedKeys.includes(k)),
    newHalfCheckedKeys.filter((k) => !removedKeys.includes(k)),
  ]
}

const Tree: FC<IProps> = (props) => {
  const { name, treeData = [], control } = props

  const {
    field: {
      onChange,
      value,
    },
  } = useController({
    name,
    control,
    defaultValue: { checkedKeys: [], halfCheckedKeys: [] },
  })

  const { checkedKeys, halfCheckedKeys } = value

  const handleChangeCheck = useCallback((keys: TCheckedValue, info: any) => {
    const [newCheckedKeys, newHalfCheckedKeys] = getNewCheckedKeys(
      treeData,
      checkedKeys,
      halfCheckedKeys,
      getCheckedKeys(keys),
      info.halfCheckedKeys,
    )
    onChange({ checkedKeys: newCheckedKeys, halfCheckedKeys: newHalfCheckedKeys })
  }, [treeData, checkedKeys, halfCheckedKeys, onChange])

  const renderTreeNode = useCallback((node: TTreeNode) => {
    const isChecked = checkedKeys.includes(node.key)
    const isHalfChecked = halfCheckedKeys.includes(node.key)

    return node.type === ETreeNodeType.RADIO ? (
      <AntRadio checked={isChecked || isHalfChecked} disabled={node.disabled}>
        {node.title}
      </AntRadio>
    ) : (
      <AntCheckbox checked={isChecked} indeterminate={isHalfChecked} disabled={node.disabled}>
        {node.title}
      </AntCheckbox>
    )
  }, [checkedKeys, halfCheckedKeys])

  return (
    <AntTree
      className='Tree'
      checkable={true}
      selectable={false}
      treeData={treeData}
      checkedKeys={checkedKeys}
      titleRender={renderTreeNode}
      switcherIcon={<IconChevron color='#412D3E' />}
      onCheck={handleChangeCheck}
    />
  )
}

export default memo(Tree)

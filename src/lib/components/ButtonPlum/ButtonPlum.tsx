import React, { memo, PropsWithChildren } from 'react'
import { Button as AntButton, ButtonProps } from 'antd'
import cn from 'classnames'

import './ButtonPlum.scss'

export interface IButtonPlumProps extends ButtonProps {
  primary?: boolean
}

const ButtonPlum: React.FC<PropsWithChildren<IButtonPlumProps>> = ({
  children,
  primary,
  type = 'default',
  className,
  ...rest
}) => {
  return (
    <AntButton className={cn('ButtonPlum', className, { primary })} type={type} {...rest}>
      {children}
    </AntButton>
  )
}

export default memo(ButtonPlum)


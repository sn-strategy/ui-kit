import React from 'react'
import ButtonPlum from './ButtonPlum'
import { ComponentStory } from '@storybook/react'

export default {
  title: 'Example/ButtonPlum',
  component: ButtonPlum,
}

const Template: ComponentStory<typeof ButtonPlum> = (args) => (<ButtonPlum {...args}/>)

export const Default = Template.bind({})
Default.args = {
  primary: true,
  children: 'Press me',
}


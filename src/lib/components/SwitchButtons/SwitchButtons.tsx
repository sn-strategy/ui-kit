import React, { memo } from 'react'
import { Radio, RadioGroupProps } from 'antd'

import './SwitchButtons.scss'

type IProps = RadioGroupProps

const SwitchButtons: React.FC<IProps> = (props) => {
  return (
    <Radio.Group
      className='SwitchButtons'
      optionType='button'
      buttonStyle='solid'
      {...props}
    />
  )
}

export default memo(SwitchButtons)

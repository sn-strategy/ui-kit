import React, { memo, PropsWithChildren } from 'react'
import { Button as AntButton, ButtonProps } from 'antd'
import cn from 'classnames'

import './Button.scss'

export type IButtonProps = ButtonProps

const Button: React.FC<PropsWithChildren<IButtonProps>> = ({ children, type = 'default', className, ...rest }) => {
  return (
    <AntButton className={cn('Button', className)} type={type} {...rest}>
      {children}
    </AntButton>
  )
}

export default memo(Button)


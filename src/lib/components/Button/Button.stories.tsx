import React from 'react'
import Button from './Button'
import { ComponentStory } from '@storybook/react'

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    type: {
      options: ['default', 'primary', 'ghost', 'dashed', 'link', 'text'],
      control: { type: 'select' },
    },
  },
}

const Template: ComponentStory<typeof Button> = (args) => (<Button {...args}/>)

export const Default = Template.bind({})
Default.args = {
  type: 'default',
  children: 'Press me',
}


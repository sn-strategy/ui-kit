import React, { ReactElement } from 'react'
import { Link } from 'react-router-dom'
import { Divider, Layout, Menu } from 'antd'
import cn from 'classnames'
import renderMenuItem from '../../functions/menu/renderMenuItem'

import type { ILogoItem, IMenuItem } from '../../models/menu'

import './AppHeader.scss'

export interface IAppHeaderProps {
    logos?: ILogoItem[]
    items?: IMenuItem[]
    className?: string
}

const AppHeader: React.FC<IAppHeaderProps> = (props) => {
  const { logos = [], items = [], className } = props

  return (
    <Layout.Header className={cn('AppHeader', className)}>
      <div>
        {logos.reduce<ReactElement[]>((acc, logo, id) => {
          if (id > 0) {
            acc.push(<Divider key={`${logo.key}-divider`} type={'vertical'}/>)
          }
          acc.push(
            <Link key={logo.key} to={logo.link}>
              <img className='AppHeader__logo' src={logo.src} alt={logo.key}/>
            </Link>,
          )
          return acc
        }, [])}
      </div>
      <Menu mode='horizontal' disabledOverflow={true}>
        {items.map((i) => renderMenuItem(i))}
      </Menu>
    </Layout.Header>
  )
}

export default React.memo(AppHeader)

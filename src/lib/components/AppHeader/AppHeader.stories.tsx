import React from 'react'
import AppHeader from './AppHeader'
import { ComponentStory } from '@storybook/react'
import { HEADER_LOGOS, HEADER_MENU_ITEMS } from '../../../fixtures/menu'

export default {
  title: 'Example/AppHeader',
  component: AppHeader,
}

const Template: ComponentStory<typeof AppHeader> = (args) => (<AppHeader {...args}/>)

export const Default = Template.bind({})
Default.args = {
  logos: HEADER_LOGOS,
  items: HEADER_MENU_ITEMS,
}


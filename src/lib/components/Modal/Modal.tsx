import React, { FC, memo, ReactNode } from 'react'
import { Modal as AntModal, ModalProps } from 'antd'
import './Modal.scss'

export interface IModalProps extends ModalProps {
  children?: ReactNode | undefined
}

const Modal: FC<IModalProps> = ({ children, ...other }) => {
  return (
    <AntModal className='Modal' {...other}>
      {children}
    </AntModal>
  )
}

export default memo(Modal)

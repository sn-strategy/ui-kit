import React from 'react'
import Modal from './Modal'
import { ComponentStory } from '@storybook/react'

export default {
  title: 'Example/Modal',
  component: Modal,
  argTypes: {
    visible: {
      control: { type: 'boolean' },
    },
  },
}

const Template: ComponentStory<typeof Modal> = ({ children, ...args }) => (
  <Modal {...args}>
    <div>{children}</div>
  </Modal>
)

export const Default = Template.bind({})
Default.args = {
  visible: false,
  children: 'Some message',
  title: 'Some title',
  footer: null,
  width: 1001,
}


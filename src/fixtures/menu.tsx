import React from 'react'
import { Avatar } from 'antd'
import { UserOutlined } from '@ant-design/icons'

export const HEADER_LOGOS = [
  {
    key: '1',
    src: 'assets/logo.svg',
    link: '/',
  },
  {
    key: '2',
    src: 'assets/logo.svg',
    link: '/',
  },
]

export const HEADER_MENU_ITEMS = [
  {
    key: '1',
    icon: 'assets/messages.svg',
    link: '/',
    // label: 'label',
    badge: 1,
  },
  {
    key: '2',
    icon: 'assets/messages.svg',
    link: '/',
    children: [
      {
        key: '3',
        link: '/',
        label: 'label',
      },
    ],
  },
  {
    key: '4',
    icon: <Avatar size={24} icon={<UserOutlined />} />,
    children: [
      {
        key: '5',
        label: 'label',
        onClick: () => null,
      },
    ],
  },
]

export const SIDEBAR_MENU = [
  {
    key: 'Dashboard',
    label: 'Dashboard',
    icon: 'assets/rocket.svg',
  },
  {
    key: 'module',
    groupTitle: 'Module',
    hasTopDivider: true,
    hasBottomDivider: true,
    children: [
      {
        key: 'currentModule',
        label: 'Current module',
        icon: 'assets/rocket.svg',
        children: [
          {
            key: 'Assortement',
            label: 'Assortement',
          },
        ],
      },
    ],
  },
  {
    key: 'Scoreboard',
    label: 'Scoreboard',
    icon: 'assets/rocket.svg',
    badge: 10,
  },
  {
    key: 'Category Analysis',
    label: 'Category Analysis',
    icon: 'assets/rocket.svg',
  },
  {
    key: 'Reports',
    label: 'Reports',
    icon: 'assets/rocket.svg',
    children: [
      {
        key: 'Report 2',
        label: 'Report 2',
        icon: 'assets/rocket.svg',
        badge: 10,
      },
      {
        key: 'Dynamic report',
        label: 'Dynamic report',
        link: '/d',
        children: [
          {
            key: 'currentModule1',
            label: 'Current module1',
            children: [
              {
                key: 'Assortement1',
                label: 'Assortement1',
              },
            ],
          },
        ],
      },
    ],
  },
]

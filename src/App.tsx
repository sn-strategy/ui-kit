import React, { useCallback } from 'react'
import { HashRouter } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { Layout } from 'antd'
import {
  AppHeader,
  AppSidebar,
  ManagementList,
  ManagementContainer,
  ContentWrapper,
  SwitchButtons,
  Button,
  Select,
  TextInput,
  Tree,
} from './lib'
import { HEADER_LOGOS, HEADER_MENU_ITEMS, SIDEBAR_MENU } from './fixtures/menu'

import './App.scss'

const App: React.FC = () => {
  const { control } = useForm()

  const cb = useCallback(() => null, [])
  return (
    <HashRouter>
      <Layout>
        <AppHeader logos={HEADER_LOGOS} items={HEADER_MENU_ITEMS} />
        <Layout>
          <AppSidebar logo={HEADER_LOGOS[0].src} menuItems={SIDEBAR_MENU} />
          <div style={{ padding: 24, width:'100%' }}>
            <ManagementContainer
              list={(
                <ManagementList
                  items={[]}
                  title={'Title'}
                  subtitle={'Subtitle'}
                  actions={[
                    {
                      key: '1',
                      image: 'assets/filter.svg',
                      content: <div>some content 1</div>,
                    },
                    {
                      key: '2',
                      image: 'assets/excel.svg',
                      content: <div>some content 2</div>,
                    },
                  ]}
                  searchPlaceholder={'Search'}
                  onSearch={cb}
                  onRenderItem={cb}
                  onCreate={cb}
                  createButtonLabel='createButtonLabel'
                />
              )}
            >
              <ContentWrapper isLoading={true}>
                content
              </ContentWrapper>
              <ContentWrapper isLoading={false} error='error text'>
                content
              </ContentWrapper>
              <ContentWrapper isLoading={false}>
                <SwitchButtons options={[{ label: '1', value: 1 }, { label: '2', value: 2 }]} />
                <Button>BUTTON</Button>
                <TextInput name={'catalog'} label={'text'} control={control} />
                <Select
                  name={'suppliers'}
                  label={'select'}
                  control={control}
                  options={[{ label: '1', value: 1 }, { label: '2', value: 2 }]}
                  selectProps={{
                    mode: 'multiple',
                  }}
                />
                <Tree name='measurementView' control={control} treeData={[{ key: 1, title: 1 }, { key: 2, title: 2 }]} />
              </ContentWrapper>
            </ManagementContainer>
          </div>
        </Layout>
      </Layout>
    </HashRouter>
  )
}

export default App
